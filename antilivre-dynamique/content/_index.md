---
title: Praxis critique ontologique
---

<div class="texte" data-id="0.">

<span>0. &mdash; </span>Les philosophes ont seulement interprété différemment l'*être*, il importe de le transformer.

</div>

<div class="texte" data-id="1.">

<span>1. &mdash; </span>La *praxis* a la puissance de produire un renversement de l'*être*.

</div>

<div class="texte" data-id="1.1">

<span>1.1 &mdash; </span>La *praxis* s'entend comme une action qui se discerne elle-même et qui perçoit la force transformatrice de son *agir* dans l'*être*.

</div>

<div class="texte" data-id="1.11">

<span>1.11 &mdash; </span>L'ontologie ne peut résider qu'au niveau de l'action. L'*être* n'est pas *abstraitement*.

</div>

<div class="texte" data-id="1.111">

<span>1.111 &mdash; </span>L'*être* n'est pas l'*être* en tant qu'*être*, mais en tant que devenir.

</div>

<div class="texte" data-id="1.1111">

<span>1.1111 &mdash; </span>L'*être* est sujet de la seule physique, même si elle aime à se cacher.

</div>

<div class="texte" data-id="1.1112">

<span>1.1112 &mdash; </span>L'*être* se situe dans la matière et dans les mystères énergiques de la matière.

</div>

<div class="texte" data-id="1.1113">

<span>1.1113 &mdash; </span>Le *mode d'être* est la manière d'*être* dans l'*être*, et le *mode d'être* de l'*être* est la dialectique *en soi*.

</div>

<div class="texte" data-id="1.11131">

<span>1.11131 &mdash; </span>La dialectique dans l'*être* est un agir de l'*être* dans l'*être*.

</div>

<div class="texte" data-id="1.11132">

<span>1.11132 &mdash; </span>La dialectique dans l'*être* est productrice de l'espace de l'*être*.

</div>

<div class="texte" data-id="1.1114">

<span>1.1114 &mdash; </span>Tout *être* en tant qu'*étant* reproduit la dialectique motrice de l'*être* pour devenir à son tour dans le *devenir* lui-même.

</div>

<div class="texte" data-id="1.1115">

<span>1.1115 &mdash; </span>Le *devenir* est une intrication de devenirs, pris dans leurs dialectiques réciproques.

</div>

<div class="texte" data-id="1.112">

<span>1.112 &mdash; </span>Il n'y a pas d'*être* en tant qu'*étant* qui ne soit acteur en tant qu'agissant.

</div>

<div class="texte" data-id="1.113">

<span>1.113 &mdash; </span>Le *non-être* est, parce que le *non-être* est une contingence du *devenir*.

</div>

<div class="texte" data-id="1.1131">

<span>1.1131 &mdash; </span>Le *non-être* peut *être*, donc il *est*.

</div>

<div class="texte" data-id="1.1132">

<span>1.1132 &mdash; </span>Le *non-être* participe à l'*être*. Le *non-être* est un *mode d'être* dans l'*être*.

</div>

<div class="texte" data-id="1.12">

<span>1.12 &mdash; </span>L'*être* vit, devient et déploie son action dans le réel qu'il compose.

</div>

<div class="texte" data-id="1.121">

<span>1.121 &mdash; </span>Il n'existe pas d'une part l'*être* du sujet, l'*être* de l'objet, et l'*être* de l'*être*. Il n'existe qu'une spatialité de l'*être* où la fluidité des *étants* s'entremêle dans une même production de leur spatialité à venir.

</div>

<div class="texte" data-id="1.1211">

<span>1.1211 &mdash; </span>Le réel est cet espace fluide du *devenir*.

</div>

<div class="texte" data-id="1.1212">

<span>1.1212 &mdash; </span>Le réel a toujours la forme de ce qu'il n'est pas encore devenu, puisque le réel est une puissance de lui-même.

</div>

<div class="texte" data-id="1.1213">

<span>1.1213 &mdash; </span>Le réel n'est pas la totalité de l'*être*, mais il est la totalité de sa matérialité baryonique.

</div>

<div class="texte" data-id="1.1214">

<span>1.1214 &mdash; </span>Au-delà du réel, il existe encore l'espace de l'*être* qui continue de se situer dans la physique.

</div>

<div class="texte" data-id="1.12141">

<span>1.12141 &mdash; </span>La physique est un espace qui croît.

</div>

<div class="texte" data-id="1.12142">

<span>1.12142 &mdash; </span>Le temps est une dimension subjective figée de l'espace en tant que connaissance de celui-ci, et l'espace est l'*être* en extension où la dynamique de la physique *est* la physique, c'est-à-dire que la matière est une dynamique de ce qui *est* en tant que ce qui *devient*.

</div>

<div class="texte" data-id="1.12143">

<span>1.12143 &mdash; </span>Rien n'existe au-delà de la physique.

</div>

<div class="texte" data-id="1.12144">

<span>1.12144 &mdash; </span>La métaphysique n'existe pas en tant qu'espace. La métaphysique existe en tant que temps du savoir. La métaphysique est une étape de la dialectique d'une raison qui s'éveille à ce qu'*est* la physique. La métaphysique ne doit plus être un discours de la transcendance de la physique, mais doit devenir un discours de la réflexivité de la physique.

</div>

<div class="texte" data-id="1.122">

<span>1.122 &mdash; </span>L'humain s'inscrit dans le vivant, sans pouvoir se contenter de son *mode d'être*.

</div>

<div class="texte" data-id="1.1221">

<span>1.1221 &mdash; </span>Le vivant se définit par une autonomie interactive au sein de la production spatiale de l'*être*.

</div>

<div class="texte" data-id="1.1222">

<span>1.1222 &mdash; </span>Le vivant n'est qu'une tournure matérielle de l'*être*.

</div>

<div class="texte" data-id="1.123">

<span>1.123 &mdash; </span>L'humain doit décentrer son *mode d'être* et le confondre à ce qui se présente à lui, à ce qui ne se présente pas à lui et à ce qui peut à la fois se présenter à lui et ne jamais se présenter à lui.

</div>

<div class="texte" data-id="1.1231">

<span>1.1231 &mdash; </span>L'humain est une puissance de la dialectique avec son environnement.

</div>

<div class="texte" data-id="1.124">

<span>1.124 &mdash; </span>L'humain doit enrichir sa *praxis* de tout ce qui se mêle à son *devenir*.

</div>

<div class="texte" data-id="1.13">

<span>1.13 &mdash; </span>Toute action *est*, indépendamment de son acteur.

</div>

<div class="texte" data-id="1.131">

<span>1.131 &mdash; </span>Tout *est* action, indépendamment de ce qui la détermine.

</div>

<div class="texte" data-id="1.2">

<span>1.2 &mdash; </span>La *praxis* regroupe potentiellement la totalité de l'activité humaine.

</div>

<div class="texte" data-id="1.21">

<span>1.21 &mdash; </span>La *praxis* va de la théorie à la création, en passant par la plus simple action.

</div>

<div class="texte" data-id="1.211">

<span>1.211 &mdash; </span>La *praxis* ne se place pas en opposition à la théorie ou à la création.

</div>

<div class="texte" data-id="1.2111">

<span>1.2111 &mdash; </span>Ni action, ni création, ni théorie, entendues dans leur singularité propre, la *praxis* est tout à la fois.

</div>

<div class="texte" data-id="1.21111">

<span>1.21111 &mdash; </span>La *praxis* est au monde, puisqu'elle n'est qu'une dialectique transformatrice du monde.

</div>

<div class="texte" data-id="1.2112">

<span>1.2112 &mdash; </span>La confusion de la nature de l'action, de la création et de la théorie dans la *praxis* ouvre une compréhension affirmant que leur *mode d'être*, indépendamment de leur élément causal, fabrique une dialectique interne à l'*être* lui-même.

</div>

<div class="texte" data-id="1.2113">

<span>1.2113 &mdash; </span>La négation de la distinction entre action, création et théorie correspond à l'affirmation de leur contingence commune, propre à modifier l'espace ontologique de leur avènement.

</div>

<div class="texte" data-id="1.2114">

<span>1.2114 &mdash; </span>Toute *praxis* est une action qui place l'humain face à la responsabilité de son *devenir*.

</div>

<div class="texte" data-id="1.212">

<span>1.212 &mdash; </span>La théorie ou la création est tout autant une action que l'action elle-même.

</div>

<div class="texte" data-id="1.213">

<span>1.213 &mdash; </span>Le fait de penser ou de créer est qualifié par le langage courant d'acte de penser ou d'acte de créer.

</div>

<div class="texte" data-id="1.214">

<span>1.214 &mdash; </span>Penser est une action du corps, créer l'est tout autant, comme l'est un licenciement, une révolution ou le fait de s'alimenter.

</div>

<div class="texte" data-id="1.2141">

<span>1.2141 &mdash; </span>La pensée est une *praxis*, par conséquent, une pensée qui se voudrait non pratique serait une pensée qui se voudrait non-pensée.

</div>

<div class="texte" data-id="1.22">

<span>1.22 &mdash; </span>Il ne peut pas être envisagé une action, qu'elle se place à un niveau industriel, intellectuel, voire physiologique, sans que cette action ne se réfère à la dynamique même du vivant.

</div>

<div class="texte" data-id="1.3">

<span>1.3 &mdash; </span>La théorie est l'action qui se projette au-devant d'elle-même dans la contemplation de la contingence de son *devenir*.

</div>

<div class="texte" data-id="1.31">

<span>1.31 &mdash; </span>La théorie est l'action de contempler l'action, elle est le mouvement qui s'en retourne à lui-même, et par conséquent, la théorie est l'action *en* elle-même.

</div>

<div class="texte" data-id="1.311">

<span>1.311 &mdash; </span>La raison qui tente ici de se faire théorie est avant tout une *praxis* qui tente de se défaire de ce que cette raison définit comme théorie.

</div>

<div class="texte" data-id="1.3111">

<span>1.3111 &mdash; </span>La raison ne triomphera *pratiquement* d'elle-même que lorsqu'elle placera action, création et théorie sur un même plan, au-delà de toute langue, contingente et éphémère, dans le seul langage de la *praxis*.

</div>

<div class="texte" data-id="1.3112">

<span>1.3112 &mdash; </span>La raison se construit comme le vecteur d'une circulation qui lie le sujet au réel.

</div>

<div class="texte" data-id="1.3113">

<span>1.3113 &mdash; </span>Limiter la raison à la langue limite le champ de perception du réel.

</div>

<div class="texte" data-id="1.4">

<span>1.4 &mdash; </span>La création est l'action qui va au réarrangement de la spatialité de son *devenir*.

</div>

<div class="texte" data-id="1.41">

<span>1.41 &mdash; </span>La création se porte à son environnement comme une potentialité transformatrice de celui-ci.

</div>

<div class="texte" data-id="1.411">

<span>1.411 &mdash; </span>La création est une action transmuant l'environnement de son avènement.

</div>

<div class="texte" data-id="1.412">

<span>1.412 &mdash; </span>La création peut renverser complètement la forme qui la détermine.

</div>

<div class="texte" data-id="1.413">

<span>1.413 &mdash; </span>La création, qui renverse la forme qui la détermine, revient sur les conditions de son avènement, et ainsi sur sa dynamique ontologique.

</div>

<div class="texte" data-id="1.414">

<span>1.414 &mdash; </span>La création, qui renverse la forme qui la détermine, est une action régénératrice de l'*être*.

</div>

<div class="texte" data-id="1.4141">

<span>1.4141 &mdash; </span>La création humaine se donne comme une action transformatrice de l'humain lui-même, en ce qu'elle transforme son action.

</div>

<div class="texte" data-id="1.42">

<span>1.42 &mdash; </span>La création s'inscrit, tout autant que l'action ou la théorie, dans une indistinction avec l'action et la théorie.

</div>

<div class="texte" data-id="1.5">

<span>1.5 &mdash; </span>Il ne s'agit plus simplement de faire évoluer l'action, mais de faire en sorte que l'action puisse faire évoluer l'espace de son propre devenir.

</div>

<div class="texte" data-id="1.51">

<span>1.51 &mdash; </span>L'action peuple l'espace où elle se déploie, et l'espace acquiert la forme de l'action.

</div>

<div class="texte" data-id="1.511">

<span>1.511 &mdash; </span>Peupler l'espace revient non seulement à *être* dans l'espace, mais à *être* l'espace.

</div>

<div class="texte" data-id="1.6">

<span>1.6 &mdash; </span>La *praxis*, consciente de ses capacités de transformer son cadre ontologique, mène à ce que nous qualifions de *poïèsis révolutionnaire*.

</div>

<div class="texte" data-id="1.61">

<span>1.61 &mdash; </span>La *poïèsis révolutionnaire*, riche d'une indistinction entre théorie et création, enclenche les capacités transformatrices intrinsèques à l'espace de son *devenir* dans le *devenir* lui-même, c'est-à-dire dans l'*être*.

</div>

<div class="texte" data-id="1.62">

<span>1.62 &mdash; </span>En transformant les conditions ontologiques de son déploiement, la *poïèsis révolutionnaire* est une force de renversement des structures qui cloisonnent la connaissance qu'une époque peut avoir d'elle-même.

</div>

<div class="texte" data-id="1.63">

<span>1.63 &mdash; </span>Toute activité humaine se subsume à la catégorie actionnelle de la *praxis*.

</div>

<div class="texte" data-id="1.631">

<span>1.631 &mdash; </span>C'est du substrat de la *praxis* qu'il faut se saisir politiquement pour chercher à renverser l'idéologie qui enserre le *mode d'être* de l'action humaine, et conséquemment le *mode d'être* de l'humain lui-même.

</div>

<div class="texte" data-id="1.632">

<span>1.632 &mdash; </span>Toute activité humaine, consciente de ses capacités créatrices, peut faire acte de révolution des conditions d'avènement de toute activité humaine.

</div>

<div class="texte" data-id="1.633">

<span>1.633 &mdash; </span>Toute activité humaine a la potentialité de ne plus pouvoir *être* ce qu'elle *est*, mais de pouvoir *être* totalement autre.

</div>

<div class="texte" data-id="2.">

<span>2. &mdash; </span>L'idéologie est un système d'idées se servant du langage pour restreindre le réel à une vérité imposée.

</div>

<div class="texte" data-id="2.1">

<span>2.1 &mdash; </span>La vérité du langage est l'idéologie qui la meut.

</div>

<div class="texte" data-id="2.11">

<span>2.11 &mdash; </span>L'idéologie est un langage au cœur du langage qui enclot le savoir sur lui-même.

</div>

<div class="texte" data-id="2.111">

<span>2.111 &mdash; </span>La langue n'est qu'une des manifestations idéologiques du langage.

</div>

<div class="texte" data-id="2.1111">

<span>2.1111 &mdash; </span>Le langage est une production systémique d'images structurant les rapports sociaux dans l'*être*.

</div>

<div class="texte" data-id="2.1112">

<span>2.1112 &mdash; </span>La production d'images intermédiantes par le langage sert à transcrire les perceptions de l'*être* dans l'*être* lui-même.

</div>

<div class="texte" data-id="2.112">

<span>2.112 &mdash; </span>L'image du corps est l'élément langagier le plus agissant sur la restriction du réel.

</div>

<div class="texte" data-id="2.113">

<span>2.113 &mdash; </span>L'image offre à l'idéologie une plasticité de la restriction.

</div>

<div class="texte" data-id="2.1131">

<span>2.1131 &mdash; </span>La langue est l'image en tant que représentation du réel accessible directement par la raison.

</div>

<div class="texte" data-id="2.1132">

<span>2.1132 &mdash; </span>Manipuler la langue revient à manipuler l'image du réel qui s'offre à la raison.

</div>

<div class="texte" data-id="2.1133">

<span>2.1133 &mdash; </span>La langue montre, elle est une direction, tandis que le langage infuse, il est une atmosphère.

</div>

<div class="texte" data-id="2.114">

<span>2.114 &mdash; </span>Chaque humain s'adapte au langage idéologique pour restreindre inconsciemment ses capacités à *être* avec autonomie.

</div>

<div class="texte" data-id="2.1141">

<span>2.1141 &mdash; </span>L'idéologie se sert du langage du fait de sa flexibilité adaptable à chaque *champ imaginaire*.

</div>

<div class="texte" data-id="2.1142">

<span>2.1142 &mdash; </span>Le *champ imaginaire*, c'est-à-dire la capacité de l'humain à produire des images comme cadre de son *devenir* propre, est la cible de toute idéologie.

</div>

<div class="texte" data-id="2.1143">

<span>2.1143 &mdash; </span>Le *champ imaginaire* est toujours une production mouvante, qui se situe entre l'humain pris dans sa granularité et les capacités du groupe à produire et partager collectivement des images.

</div>

<div class="texte" data-id="2.1144">

<span>2.1144 &mdash; </span>Modeler le *champ imaginaire* de l'humain revient à modeler sa capacité même à se projeter dans le réel, au-delà de ce qui est dans le cadre idéologique dominant.

</div>

<div class="texte" data-id="2.1145">

<span>2.1145 &mdash; </span>En modelant le *champ imaginaire* de l'humain, l'idéologie l'oriente vers un *devenir* unique et imposé, servant l'intérêt de ceux qui en usent.

</div>

<div class="texte" data-id="2.1146">

<span>2.1146 &mdash; </span>La restriction du *champ imaginaire* de l'humain est une restriction indirecte des contingences de l'*être*.

</div>

<div class="texte" data-id="2.12">

<span>2.12 &mdash; </span>L'emploi du terme *idéologie* relève de l'idéologie elle-même, s'il ne cherche pas sa déconstruction.

</div>

<div class="texte" data-id="2.121">

<span>2.121 &mdash; </span>La langue qui use du terme *idéologie* est à inscrire le plus souvent dans l'appareil langagier de l'ennemi politique, comme un usage qui tente de restreindre les capacités de libération du *champ imaginaire*.

</div>

<div class="texte" data-id="2.122">

<span>2.122 &mdash; </span>Tout emploi du terme *idéologie* nécessite la déconstruction du motif de son emploi, afin d'en séparer le masque sémantique de sa puissance révélatrice des rouages qui meuvent la configuration dominante et restrictive du savoir.

</div>

<div class="texte" data-id="2.123">

<span>2.123 &mdash; </span>Aucun emploi du terme *idéologie* ne doit être accepté sans la compréhension de ce qui incite à son usage, y compris l'emploi que nous en faisons ici.

</div>

<div class="texte" data-id="2.1231">

<span>2.1231 &mdash; </span>Tout emploi du terme *idéologie* doit être analysé à partir du discours depuis lequel il est émis, et il s'avère nécessaire de connaître la dominance de ce discours dans la structuration de la société.

</div>

<div class="texte" data-id="2.13">

<span>2.13 &mdash; </span>L'idéologie est l'instrument d'imposition de la vérité, puisque l'idéologie est un instrument de domination par le langage.

</div>

<div class="texte" data-id="2.131">

<span>2.131 &mdash; </span>La vérité est l'imposition d'*une* vérité.

</div>

<div class="texte" data-id="2.1311">

<span>2.1311 &mdash; </span>Une vérité n'est qu'une perspective dialectique sur le réel. Nous qualifions son instant de réalité.

</div>

<div class="texte" data-id="2.1312">

<span>2.1312 &mdash; </span>La vérité est toujours un *régime de savoir*.

</div>

<div class="texte" data-id="2.1313">

<span>2.1313 &mdash; </span>C'est dans l'espace qui se situe entre le réel et la vérité que doit venir s'insinuer la *praxis*, comme instrument de déconstruction de la vérité.

</div>

<div class="texte" data-id="2.132">

<span>2.132 &mdash; </span>Le langage doit chercher sa vérité comme une vérité, s'il souhaite entendre l'idéologie qui l'anime.

</div>

<div class="texte" data-id="2.1321">

<span>2.1321 &mdash; </span>La multiplication des perspectives sur le réel décloisonne la vérité.

</div>

<div class="texte" data-id="2.1322">

<span>2.1322 &mdash; </span>La vérité en tant que régime absolutiste du savoir doit être combattue par la révélation du relativisme de son emploi. Celui-ci s'avère exclusivement politique et ne sert que les intérêts variables des catégories sociales dominantes ayant les capacités d'établir un savoir comme *régime de savoir*.

</div>

<div class="texte" data-id="2.1323">

<span>2.1323 &mdash; </span>Un *régime de savoir* s'entend par la fixité qu'il s'impose à lui-même. L'autorité de cette fixité est un indicateur de la nature politique du pouvoir dominant.

</div>

<div class="texte" data-id="2.1324">

<span>2.1324 &mdash; </span>Un savoir ne peut être contraint à aucune fixité. Il est un *devenir* de la raison cherchant à comprendre la mécanique de son propre devenir.

</div>

<div class="texte" data-id="2.2">

<span>2.2 &mdash; </span>L'idéologie est l'œillère qui sert les velléités des dominants.

</div>

<div class="texte" data-id="2.21">

<span>2.21 &mdash; </span>La restriction du réel à une vérité imposée ne peut être que le fait de ceux qui possèdent le pouvoir de l'imposer, au sein d'une société, au sein d'une classe ou, à plus petite échelle, au sein d'un groupe social.

</div>

<div class="texte" data-id="2.22">

<span>2.22 &mdash; </span>Les dominants, par l'emploi de l'idéologie, font la promotion de leur seul *devenir*, qui doit s'entendre comme *devenir* seul, servant leur épanouissement unique dans l'*être*.

</div>

<div class="texte" data-id="2.221">

<span>2.221 &mdash; </span>Les dominants ne visent que leur propre épanouissement dans l'*être* par la seule revendication d'une hiérarchie sociale, quelle que soit la nature structurelle de leur groupement.

</div>

<div class="texte" data-id="2.222">

<span>2.222 &mdash; </span>L'épanouissement dans l'*être* des dominés est un empêchement de l'action de domination des dominants, et cet empêchement doit être à son tour empêché si le groupe dominant souhaite conserver à terme son hégémonie.

</div>

<div class="texte" data-id="2.3">

<span>2.3 &mdash; </span>L'idéologie est un idéalisme du langage qui cherche à dominer.

</div>

<div class="texte" data-id="2.31">

<span>2.31 &mdash; </span>Tout idéalisme obstrue le réel par une volonté d'y adjoindre une restriction de son entendement limité à l'esprit.

</div>

<div class="texte" data-id="2.311">

<span>2.311 &mdash; </span>Se limiter à l'esprit revient toujours à se limiter à l'esprit tel
qu'il est énoncé par l'idéalisme, sans tenir compte de sa contingence à
*être* autrement.

</div>

<div class="texte" data-id="2.32">

<span>2.32 &mdash; </span>La réalité de l'idéalisme limite le réel à la seule capacité de se le représenter.

</div>

<div class="texte" data-id="2.321">

<span>2.321 &mdash; </span>L'idéalisme n'établit qu'un unique accès à l'*être* par l'intermédiaire des capacités de représentation de l'esprit.

</div>

<div class="texte" data-id="2.322">

<span>2.322 &mdash; </span>L'idéalisme se représente lui-même comme une illusion qui tente de masquer l'angoisse de la finitude humaine face aux incompréhensions d'une *nature* au *devenir* insaisissable et indépendant de l'action même.

</div>

<div class="texte" data-id="2.323">

<span>2.323 &mdash; </span>L'idéologie se sert de ce rapport unique entre l'abstraction de l'*être* et celle de la pensée pour affirmer l'imposition de son *régime de savoir*.

</div>

<div class="texte" data-id="2.324">

<span>2.324 &mdash; </span>Il est nécessaire de penser le non-représentable comme évasion de l'ensemble indépassable que tente de consolider l'idéologie. Cette pensée doit demeurer elle-même non-représentation.

</div>

<div class="texte" data-id="2.33">

<span>2.33 &mdash; </span>La théorie devient de la sorte un instrument de l'idéologie qui essaie d'imposer une déformation *vraie* du réel dans le but de contenir l'action des dominés à un espace restreint, utile aux seuls intérêts de l'action propre des classes dominantes.

</div>

<div class="texte" data-id="2.331">

<span>2.331 &mdash; </span>Une théorie, qui se contenterait de n'être que théorique, de n'évoluer que dans les sphères éthérées que goûteraient quelques privilégiés repus d'abstractions, resterait invariablement un outil idéologique déconnecté de sa nature actionnelle.

</div>

<div class="texte" data-id="2.332">

<span>2.332 &mdash; </span>La seule théorie acceptable est celle qui ne se distingue pas de la *praxis*, mais qui s'y incorpore inextricablement et qui œuvre à l'augmentation de la potentialité agissante du vivant.

</div>

<div class="texte" data-id="2.3321">

<span>2.3321 &mdash; </span>La *praxis* théorique ne peut se représenter que comme une théorie issue démocratiquement de la multitude agissante, dont le savoir substantiel demeure à la disposition de la totalité de cette multitude agissante dans le but d'accroître possiblement sa puissance d'action.

</div>

<div class="texte" data-id="2.3322">

<span>2.3322 &mdash; </span>La théorie, entendue comme *praxis*, se fait action transformatrice de l'action. Elle est une intelligence de la transmutation de l'action, de son adaptation à ce qui se présente à elle, à sa façon de pousser à *être* un *être* agissant et devenant.

</div>

<div class="texte" data-id="2.34">

<span>2.34 &mdash; </span>L'idéalisme est une défaite de l'esprit face à la question de sa propre matérialité, et de la place de celle-ci dans le réel. L'idéalisme simplifie sa réalité à un rapport de l'*être* à la représentation de celui-ci par l'esprit, pour écarter les complexités du réel et de la physique qui le sous-tend.

</div>

<div class="texte" data-id="2.341">

<span>2.341 &mdash; </span>Le refus du tout physique de la matière, jusque dans les phénomènes sociaux et historiques, est le cœur idéologique de tout idéalisme.

</div>

<div class="texte" data-id="2.342">

<span>2.342 &mdash; </span>Le refus du tout mouvant de la matière, pris dans son constant devenir, jusque dans les phénomènes sociaux et historiques, est le cœur idéaliste de toute idéologie.

</div>

<div class="texte" data-id="2.4">

<span>2.4 &mdash; </span>La déconstruction de l'idéologie passe par l'affirmation du matérialisme historique afin d'établir les éléments qui conditionnent l'idéologie motrice du social.

</div>

<div class="texte" data-id="2.41">

<span>2.41 &mdash; </span>Le matérialisme historique offre une vision de la réalité sociale non à partir d'un quelconque idéalisme de l'appréhension du réel, mais depuis l'entendement des conséquences de l'enchevêtrement des rapports sociaux.

</div>

<div class="texte" data-id="2.411">

<span>2.411 &mdash; </span>Les rapports sociaux de la modernité ne sont animés que par un productivisme ne servant que l'idée d'un individu libre et possesseur de son environnement, c'est-à-dire que les rapports sociaux modernes ne sont animés que par les moyens de production eux-mêmes.

</div>

<div class="texte" data-id="2.4111">

<span>2.4111 &mdash; </span>Les moyens de production ne doivent pas être appréhendés comme des choses abstraites, mais comme des phénomènes concrets produits par les humains, à partir de leurs besoins, et au service de leur asservissement à l'idéologie dominante.

</div>

<div class="texte" data-id="2.412">

<span>2.412 &mdash; </span>Les humains ne sont pas les produits des circonstances, ils ne sont pas même de quelconques produits, les humains sont des producteurs d'humains.

</div>

<div class="texte" data-id="2.413">

<span>2.413 &mdash; </span>L'imposition de l'idéologie à la multitude a lieu uniquement grâce à la malléabilité de son consentement.

</div>

<div class="texte" data-id="2.4131">

<span>2.4131 &mdash; </span>Ce n'est pas simplement la multitude qui est contrainte par le joug d'une idéologie, mais c'est la multitude qui nourrit l'idéologie par son inconscience ontologique.

</div>

<div class="texte" data-id="2.4132">

<span>2.4132 &mdash; </span>La passivité de la multitude est un refus de sa puissance à *être*, et ainsi de sa puissance à transformer l'*être* lui-même.

</div>

<div class="texte" data-id="2.4133">

<span>2.4133 &mdash; </span>La *praxis* est le seul moyen de l'émancipation collective de la multitude.

</div>

<div class="texte" data-id="2.414">

<span>2.414 &mdash; </span>Il faut renverser la causalité établie dans le discours dominant sur la manière dont s'engendreraient les structures sociales pour révéler le cadre idéologique qui empêche l'autonomie de la multitude.

</div>

<div class="texte" data-id="2.4141">

<span>2.4141 &mdash; </span>Les circonstances de l'histoire de la multitude sont déterminées systémiquement à partir d'une dépendance à sa volonté de soumission.

</div>

<div class="texte" data-id="2.4142">

<span>2.4142 &mdash; </span>La multitude veut ne pas vouloir sa révolution.

</div>

<div class="texte" data-id="2.4143">

<span>2.4143 &mdash; </span>La perception du renversement de la causalité qui anime le discours dominant est une brèche dans la passivité d'une multitude disjointe, séparée de la conscience d'elle-même, conscience qui pourrait conduire pourtant à l'inversion de sa volonté de soumission.

</div>

<div class="texte" data-id="2.4144">

<span>2.4144 &mdash; </span>Une multitude volitive, consciente d'elle-même, par sa *praxis*, peut renverser son *mode d'être* et transformer l'espace de son *devenir*.

</div>

<div class="texte" data-id="2.4145">

<span>2.4145 &mdash; </span>L'insoumission est l'étincelle de la *praxis*.

</div>

<div class="texte" data-id="2.42">

<span>2.42 &mdash; </span>L'idéologie permet de dissimuler la minorité dirigeante qui détermine les conditions du système producteur des circonstances historiques.

</div>

<div class="texte" data-id="2.421">

<span>2.421 &mdash; </span>L'idéologie se fait art des artifices en établissant un système qui masque les étapes intermédiaires de sa causalité, afin de suggérer une indépendance de la production systémique des conditions sociales de la vie humaine.

</div>

<div class="texte" data-id="2.43">

<span>2.43 &mdash; </span>Par la conscience du processus idéologique, l'humain peut ne plus *être transformé par* l'humain, mais l'humain peut *transformer* son humanité et commencer à composer une multitude autonome.

</div>

<div class="texte" data-id="2.431">

<span>2.431 &mdash; </span>Les perspectives sur le réel doivent être multipliées si l'on souhaite échapper à l'imposition de la vérité, et révéler la construction idéologique des situations sociales.

</div>

<div class="texte" data-id="2.432">

<span>2.432 &mdash; </span>L'humain est en possession d'une maîtrise de son environnement, qui présente une potentialité de déconstruction des hiérarchies l'enfermant en une passivité dépendante de l'idéologie dominante.

</div>

<div class="texte" data-id="2.4321">

<span>2.4321 &mdash; </span>Une situation sociale ne se subit pas parce qu'elle *est*, une situation sociale se subit parce qu'elle est imposée.

</div>

<div class="texte" data-id="2.4322">

<span>2.4322 &mdash; </span>Il importe à ceux qui imposent les situations sociales, et en tirent un profit personnel, de masquer la construction dirigée de ces mêmes situations.

</div>

<div class="texte" data-id="2.433">

<span>2.433 &mdash; </span>Doit être envisagé un dépassement de l'idée d'individu, cette granularité sociale en tant qu'élément paradigmatique qui structure l'idéologie moderne, pour dépasser l'enclos de la modernité.

</div>

<div class="texte" data-id="2.4331">

<span>2.4331 &mdash; </span>La modernité est un *régime de savoir* où tout se concentre autour d'un individu à la volonté dite libre.

</div>

<div class="texte" data-id="2.4332">

<span>2.4332 &mdash; </span>La volonté individuelle libre de la modernité se contente d'une simple passivité de l'*avoir*, c'est-à-dire d'une puissance d'accaparement qui fait simulacre de l'*être*.

</div>

<div class="texte" data-id="2.4333">

<span>2.4333 &mdash; </span>Toute transformation causée par l'individu ne peut entraîner, dans le cadre du *régime de savoir* de la modernité, qu'un accroissement de la capacité possessive exclusive de son environnement.

</div>

<div class="texte" data-id="2.4334">

<span>2.4334 &mdash; </span>La négation du primat groupal, par la considération de l'individu comme granularité, se propage dans la conception libérale de la citoyenneté et ameute les individus-citoyens en une société bourgeoise.

</div>

<div class="texte" data-id="2.43341">

<span>2.43341 &mdash; </span>La négation du primat groupal a pour substance la discrimination : discrimination externe qu'opère la citoyenneté envers ce qui n'est pas citoyen, mais aussi discrimination interne qui fait distinction entre les individus, ces entités granulaires.

</div>

<div class="texte" data-id="2.43342">

<span>2.43342 &mdash; </span>Pour l'individu-citoyen, sa classe est l'espace fini de sa citoyenneté, son monde devant être l'espace fini de ce qui doit *être*.

</div>

<div class="texte" data-id="2.43343">

<span>2.43343 &mdash; </span>Le compartimentage du social en classes sociales est fonction de la manière dont les pratiques existentielles individuelles protègent et favorisent la hiérarchie motrice de la société bourgeoise, qui renferme chacun en sa subjectivité et oppose chacun à la subjectivité de l'autre.

</div>

<div class="texte" data-id="2.43344">

<span>2.43344 &mdash; </span>Seule une conscience de l'illusion idéologique que compose une finitude des classes sociales permet de briser tous les déterminismes de cette illusion.

</div>

<div class="texte" data-id="2.434">

<span>2.434 &mdash; </span>Le possible dépassement de l'individu ne s'examine qu'à partir d'un matérialisme historique qui fait primer le groupe social sur l'idéal d'une quelconque abstraction, et met de telle façon en exergue la prévalence des phénomènes sociaux dans la compréhension de notre réalité.

</div>

<div class="texte" data-id="3.">

<span>3. &mdash; </span>Le *sujet réticulaire* compose l'avenir d'une multitude consciente d'elle-même et agissante avec autonomie dans son espace ontologique.

</div>

<div class="texte" data-id="3.1">

<span>3.1 &mdash; </span>Le sujet moderne, humain et individuel, nécessite un décentrement en une forme groupale tant humaine que non humaine. Ce décentrement vers un sujet pluriel et mouvant, nous le nommons *sujet réticulaire*.

</div>

<div class="texte" data-id="3.11">

<span>3.11 &mdash; </span>La mise en exergue du fait que toute pensée individuelle s'organise à partir d'une dialectique matérialiste démontre que toute pensée est avant tout un phénomène pluriel et dynamique.

</div>

<div class="texte" data-id="3.111">

<span>3.111 &mdash; </span>Le *sujet réticulaire* se façonne à partir d'un réseau de pensées interindividuelles, en tant que pensées réticulaires et mouvantes, dynamisant les phénomènes sociaux entre eux.

</div>

<div class="texte" data-id="3.112">

<span>3.112 &mdash; </span>Les phénomènes sociaux se traversent les uns les autres en laissant la trace de leur passage dans l'inconscient collectif que l'ensemble des phénomènes sociaux construit.

</div>

<div class="texte" data-id="3.113">

<span>3.113 &mdash; </span>L'organisation d'une pensée réticulaire révèle l'illusion de la constitution d'une pensée moderne basée sur l'autonomie individuelle.

</div>

<div class="texte" data-id="3.114">

<span>3.114 &mdash; </span>L'autonomie ne peut être qu'une caractéristique de la pluralité, elle ne peut s'envisager dans l'incohérence d'un détachement des interactions sociales et biologiques.

</div>

<div class="texte" data-id="3.1141">

<span>3.1141 &mdash; </span>N'importe quel élément du réel s'inscrit nécessairement dans un réseau de causalités physiques.

</div>

<div class="texte" data-id="3.1142">

<span>3.1142 &mdash; </span>N'importe quel élément de la réalité sociale s'inscrit nécessairement dans un réseau de causalités sociales.

</div>

<div class="texte" data-id="3.1143">

<span>3.1143 &mdash; </span>Un réseau d'interactions causales consiste en un ensemble de circulations où les causes et les effets se confondent du fait de leur constante dynamique.

</div>

<div class="texte" data-id="3.12">

<span>3.12 &mdash; </span>Le nœud dialectique de la modernité se conçoit dans l'opposition entre
la volonté de domination individuelle et l'agrégat d'individus qui subit
cette subjectivité dominatrice.

</div>

<div class="texte" data-id="3.121">

<span>3.121 &mdash; </span>La modernité correspond à un amoncellement de groupes humains, ne voulant pas se saisir de leur subjectivité groupale et ainsi de leur puissance d'action plurielle.

</div>

<div class="texte" data-id="3.1211">

<span>3.1211 &mdash; </span>Les groupes humains modernes sont composés d'individus mus par une volonté égoïste de domination sur ce qui se trouve extérieur à leur subjectivité individuelle.

</div>

<div class="texte" data-id="3.122">

<span>3.122 &mdash; </span>La réalité de la modernité est une limitation du réel à la subjectivité individuelle.

</div>

<div class="texte" data-id="3.1221">

<span>3.1221 &mdash; </span>La modernité s'est construite sur une vision tronquée du réel qui le clôt en l'action individuelle. La modernité est ainsi devenue une pratique solipsiste, inconsciente même des dimensions ontologiques de son renfermement.

</div>

<div class="texte" data-id="3.1222">

<span>3.1222 &mdash; </span>La modernité doit être vue comme une réaction limitant la transformation du sujet aux seuls contentements de l'individu.

</div>

<div class="texte" data-id="3.123">

<span>3.123 &mdash; </span>Il faut non seulement élever l'observation à un niveau groupal, mais y réintroduire la question de la subjectivité.

</div>

<div class="texte" data-id="3.1231">

<span>3.1231 &mdash; </span>Laisser un groupe sans capacité subjective propre, si ce n'est celle des subjectivités individuelles, perpétue la figure du sujet individuel, terré dans son angoisse existentielle, par le fait même de son égocentrisme opaque qui l'empêche de percevoir une quelconque traversée de son horizon ontologique.

</div>

<div class="texte" data-id="3.1232">

<span>3.1232 &mdash; </span>Attaquer toute forme d'idéalisme revient à attaquer indirectement cette question de la granularité sociale en la plaçant au cœur d'un réseau d'interactions indépassable par le seul individu.

</div>

<div class="texte" data-id="3.124">

<span>3.124 &mdash; </span>Le matérialisme doit être compris dans une globalité subjective, puisque la seule vision objective finirait inévitablement en une dénaturation d'un sujet social et pluriel et correspondrait à une trahison interne à la multitude.

</div>

<div class="texte" data-id="3.125">

<span>3.125 &mdash; </span>Ne pas étendre la subjectivité à la multitude revient à la condamner à une *impossible insoumission*, c'est-à-dire à n'accorder qu'aux forces modernes et réactionnaires, celles qui meuvent les intérêts individuels dominants, la maîtrise des potentialités du sujet.

</div>

<div class="texte" data-id="3.13">

<span>3.13 &mdash; </span>Il s'agit de transformer le sujet pour le joindre à l'objet, dans une interpénétration fluide de leur identité, de ne plus dire *je*, mais *nous*, d'affirmer l'*agimus* au détriment de toute forme du *cogito*, et de confondre ainsi subjectivité et objectivité au sein même de la *praxis*, cette pratique qui se veut critique et ontologique, discernement de l'*être* en tant que force agissante, transformatrice d'elle-même et de l'espace de son *devenir*.

</div>

<div class="texte" data-id="3.2">

<span>3.2 &mdash; </span>La *praxis* du *sujet réticulaire* met en évidence une réalité de la physique : la permanence de sa dynamique autorégulatrice.

</div>

<div class="texte" data-id="3.21">

<span>3.21 &mdash; </span>La physique est *en soi* un phénomène non individuel, et la *praxis* du *sujet réticulaire* s'y adjoint, tout en se dissociant du cloisonnement des mécaniques individuantes du vouloir.

</div>

<div class="texte" data-id="3.22">

<span>3.22 &mdash; </span>Quel que soit le cadre social ou historique, isoler la *praxis* s'avère une impossibilité.

</div>

<div class="texte" data-id="3.221">

<span>3.221 &mdash; </span>La recherche de l'isolement de la *praxis*, à notre époque de transition où un *sujet réticulaire* est en train de faire naître une *praxis* de la multitude s'unifiant dans une conscience balbutiante d'elle-même, se rapporte inévitablement à une volonté contre-révolutionnaire, mue par une propension à réagir aux mouvements de l'histoire.

</div>

<div class="texte" data-id="3.3">

<span>3.3 &mdash; </span>La *praxis* de la transformation de la multitude par elle-même correspond à une action créatrice d'un renversement ontologique et politique des conditions d'existence : c'est une *poïèsis révolutionnaire*.

</div>

<div class="texte" data-id="3.31">

<span>3.31 &mdash; </span>Une *poïèsis révolutionnaire*, catégorie de la *praxis*, ne s'envisage, après une objectivation du sujet individuel par l'observation des mouvements sociaux et historiques déterminant les groupes humains, qu'avec la réintroduction d'une subjectivité plurielle, mais commune à la totalité du groupe social.

</div>

<div class="texte" data-id="3.311">

<span>3.311 &mdash; </span>La conscience d'une subjectivité plurielle permet à la multitude de *pouvoir* s'emparer de son *devenir* ontologique et d'y concevoir ses transformations.

</div>

<div class="texte" data-id="3.32">

<span>3.32 &mdash; </span>La *poïèsis révolutionnaire* de la multitude est une activité créatrice de l'autonomie du sujet et dès lors de la libération des conditions politiques et sociales de son existence.

</div>

<div class="texte" data-id="3.321">

<span>3.321 &mdash; </span>La transformation des circonstances qui enserre l'humain en une certaine causalité ne se contente pas d'être une simple *praxis révolutionnaire*, mais se spécifie de telle manière en une *poïèsis révolutionnaire*.

</div>

<div class="texte" data-id="3.33">

<span>3.33 &mdash; </span>L'activité politique ne se contente plus d'être une simple *action*, mais se fait pleinement *création* qui influe directement sur l'environnement de celle-ci par une transformation de sa substance.

</div>

<div class="texte" data-id="3.34">

<span>3.34 &mdash; </span>Une telle pratique créatrice et révolutionnaire véhicule une constante *autoéducation* émanant de l'expérience *poïétique*.

</div>

<div class="texte" data-id="3.341">

<span>3.341 &mdash; </span>L'apprentissage par la pratique nourrissant la pratique demeure une clef de l'autonomie.

</div>

<div class="texte" data-id="3.342">

<span>3.342 &mdash; </span>L'autonomie se trouve affirmée par le phénomène de son propre ruissellement au sein d'une multitude dont la dynamique se réalise par l'interconnexion de ses éléments autonomes.

</div>

<div class="texte" data-id="3.3421">

<span>3.3421 &mdash; </span>La conscience de l'autonomie catalyse l'autonomie elle-même.

</div>

<div class="texte" data-id="3.3422">

<span>3.3422 &mdash; </span>La multitude, par la conquête de son autonomie, s'*autoéduque* et, par conséquent, met fin aux hiérarchies qui placent des limites idéologiques entre le réel et la volonté de se saisir du réel.

</div>

<div class="texte" data-id="3.343">

<span>3.343 &mdash; </span>L'humain, transformé par une volonté commune et composant une multitude consciente d'elle-même, crée pour transformer son environnement immédiat et étendre de la sorte les espaces nouveaux du *devenir* politique et social de la multitude.

</div>

<div class="texte" data-id="3.35">

<span>3.35 &mdash; </span>La coïncidence du changement de la *praxis* et des circonstances déterminant la *praxis* correspond à une volonté de créer un environnement nouveau de l'*être*, qui conduira invariablement à des transformations politiques et sociales conséquentes des changements ontologiques opérés.

</div>

<div class="texte" data-id="3.4">

<span>3.4 &mdash; </span>Le pouvoir de la multitude consciente d'elle-même doit devenir à son image : fluide, acentré et mobile.

</div>

<div class="texte" data-id="3.41">

<span>3.41 &mdash; </span>Pour que le *pouvoir* aille à la multitude, la démocratie doit revenir à sa radicalité, c'est-à-dire à sa racine étymologique, à un *pouvoir* du peuple sans limitation aucune.

</div>

<div class="texte" data-id="3.411">

<span>3.411 &mdash; </span>La démocratie radicale se comprend comme une potentialité continue de l'intervention concertée et commune sur les structures économiques et sociales qui conditionnent la multitude dans son instant.

</div>

<div class="texte" data-id="3.412">

<span>3.412 &mdash; </span>La démocratie au sein d'une multitude consciente d'elle-même refuse toutes les formes de la représentation politique, et se renouvelle sans cesse en une permanence mouvante de la décision concertée.

</div>

<div class="texte" data-id="3.413">

<span>3.413 &mdash; </span>La démocratie radicale est une cybernétique du pouvoir de la multitude.

</div>

<div class="texte" data-id="3.4131">

<span>3.4131 &mdash; </span>Une ébauche de l'organisation démocratique radicale se découvre dans les systèmes
informatiques connectés en réseaux acentrés permettant d'envisager
l'absence de hiérarchie, l'accessibilité et la permanence
du pouvoir décisionnel.

</div>

<div class="texte" data-id="3.4132">

<span>3.4132 &mdash; </span>L'assurance d'une motilité libre du *sujet réticulaire* s'avère cruciale, ainsi que la sauvegarde d'une structure réticulaire évitant toute centralisation.

</div>

<div class="texte" data-id="3.414">

<span>3.414 &mdash; </span>Toute limite du pouvoir de la multitude, aussi faible soit-elle, doit être considérée comme antidémocratique.

</div>

<div class="texte" data-id="3.4141">

<span>3.4141 &mdash; </span>Un parlement qui ne pourrait pas accueillir la voix de n'importe quel membre de la multitude, et ce à n'importe quel instant, restreindrait inévitablement les contingences de son *être*.

</div>

<div class="texte" data-id="3.4142">

<span>3.4142 &mdash; </span>Le *sujet réticulaire* doit pouvoir se saisir politiquement de lui-même en tout instant, sans intermédiaire et sans entrave.

</div>

<div class="texte" data-id="3.415">

<span>3.415 &mdash; </span>La radicalité démocratique trouve ses fondements dans la conversion politique d'une conscience immédiate de l'*être*.

</div>

<div class="texte" data-id="3.42">

<span>3.42 &mdash; </span>Le pouvoir pour la multitude de se saisir en permanence des conditions déterminant les circonstances de sa propre existence esquisse le cadre d'une autonomie plurielle, unie et distribuée.

</div>

<div class="texte" data-id="3.421">

<span>3.421 &mdash; </span>Le renversement de la causalité politique déterminant le socle de l'environnement de la vie humaine passe par l'avènement d'une multitude qui se saisit justement du pouvoir causal sur son environnement politique en instituant une démocratie envisagée dans toute la radicalité entendue en son étymologie.

</div>

<div class="texte" data-id="3.422">

<span>3.422 &mdash; </span>L'organisation politique nouvelle d'une permanence du pouvoir de la multitude par la multitude sans aucun intermédiaire serait la conséquence politique du renversement ontologique conséquent de l'apparition d'un *sujet réticulaire* : multitude plurielle et unie, consciente et connectée, découvrant son *être* à travers le commun de son *agir*.

</div>

<div class="texte" data-id="4.">

<span>4. &mdash; </span>Un monde engendre toujours une dialectique des mondes, à laquelle se *conforme* la politique.

</div>

<div class="texte" data-id="4.1">

<span>4.1 &mdash; </span>Un monde est une organisation subjective du réel découlant d'une confrontation dialectique à celui-ci, confrontation individuelle, groupale ou sociale.

</div>

<div class="texte" data-id="4.11">

<span>4.11 &mdash; </span>La forme d'un monde est la déformation de la sphère par la constante et dynamique confrontation du sujet, pluriel ou singulier, à son espace ontologique.

</div>

<div class="texte" data-id="4.111">

<span>4.111 &mdash; </span>La pureté formelle d'un monde serait une sphère parfaite, qui aurait pour centre le sujet, si un idéalisme absolu avait un quelconque sens physique au-delà de l'idéologie qu'il véhicule.

</div>

<div class="texte" data-id="4.112">

<span>4.112 &mdash; </span>La concentration du sujet moderne en un espace clos centré sur l'individu fait du monde une forme concentrique opaque.

</div>

<div class="texte" data-id="4.113">

<span>4.113 &mdash; </span>Un monde, compris à partir du *sujet réticulaire* libéré de l'idéologie moderne, retrouverait sa forme mutante, croissante, s'adaptant à son propre devenir.

</div>

<div class="texte" data-id="4.12">

<span>4.12 &mdash; </span>La confrontation dialectique au réel, qui fait naître un monde, forme une rationalité des liens, perceptibles ou imperceptibles, unissant le sujet à sa réalité.

</div>

<div class="texte" data-id="4.13">

<span>4.13 &mdash; </span>Le monde est la forme de la réalité, en d'autres termes, il est la forme d'une subjectivation du réel.

</div>

<div class="texte" data-id="4.131">

<span>4.131 &mdash; </span>La réalité demeure cette perception du réel qui accueille l'instant de la confrontation dialectique du sujet.

</div>

<div class="texte" data-id="4.14">

<span>4.14 &mdash; </span>L'illusion la plus dangereuse qui découle d'un monde, de sa confrontation dialectique au réel, est celle de la vérité, qui tente d'imposer l'état statique d'une rationalité subjective propre à son monde et aux seuls mondes auxquels il se confronte.

</div>

<div class="texte" data-id="4.141">

<span>4.141 &mdash; </span>La confrontation du sujet à une part seule du réel, *sa* part du réel, peut lui laisser croire que le savoir, qui se construit à partir de son monde, est une universalité de ce qui *est*.

</div>

<div class="texte" data-id="4.1411">

<span>4.1411 &mdash; </span>Toute extension de la subjectivité qui anime l'appréhension du réel à l'ensemble du réel constitue le berceau de toute idéologie.

</div>

<div class="texte" data-id="4.142">

<span>4.142 &mdash; </span>Les choses n'existent pas par la vision qui s'y porte, mais seule la vérité du monde du sujet perdure par celle-ci.

</div>

<div class="texte" data-id="4.15">

<span>4.15 &mdash; </span>Les ennemis de la matérialité du monde fabriquent à leur tour des mondes. Ils opposent le monde *présent* à l'*arrière-monde*.

</div>

<div class="texte" data-id="4.151">

<span>4.151 &mdash; </span>L'*arrière-monde* serait supérieur au monde *présent* parce qu'il aurait pour substance une antinomie de la substance périssable du monde présent.

</div>

<div class="texte" data-id="4.1511">

<span>4.1511 &mdash; </span>Le monde *présent* est le seul à permettre au sujet une *présence au monde*.

</div>

<div class="texte" data-id="4.15111">

<span>4.15111 &mdash; </span>La *présence* détermine l'existence : le monde *présent est*, parce que le monde *présent* se tient seul *devant* le réel.

</div>

<div class="texte" data-id="4.15112">

<span>4.15112 &mdash; </span>Le monde *présent* est le seul à accorder au sujet la capacité de structurer sa confrontation au réel.

</div>

<div class="texte" data-id="4.1512">

<span>4.1512 &mdash; </span>Être *présent au monde* revient à faire monde dans l'immédiateté de la dialectique du sujet et du réel.

</div>

<div class="texte" data-id="4.152">

<span>4.152 &mdash; </span>L'*arrière-monde* est l'hypothèse des ennemis de la matière, qui y placent l'idée comme échappatoire, l'hallucination comme anesthésiant.

</div>

<div class="texte" data-id="4.1521">

<span>4.1521 &mdash; </span>L'idée de l'*arrière-monde* empêche toute conception d'une possible mise en abyme des mondes, d'une révélation des couches de réalités sociales qui organisent la dialectique même entre la perception du réel et le réel, façonnant l'enchevêtrement mondain que l'on nomme société.

</div>

<div class="texte" data-id="4.153">

<span>4.153 &mdash; </span>L'*arrière-monde* est une négation du *devenir*.

</div>

<div class="texte" data-id="4.16">

<span>4.16 &mdash; </span>Accepter l'hybridation de sa conception du monde est un acte nécessaire pour asseoir une compréhension de la totalité du social.

</div>

<div class="texte" data-id="4.161">

<span>4.161 &mdash; </span>C'est la facilité de la jouissance du *monde à soi* du sujet qui doit être éradiquée, par une analyse qui se renouvelle continuellement en se *jetant* dans l'enchevêtrement des mondes.

</div>

<div class="texte" data-id="4.1611">

<span>4.1611 &mdash; </span>L'ennemi demeure l'ennemi de la révélation de l'enchevêtrement des mondes et de leurs caractéristiques, niant particulièrement celles des mondes qui se juxtaposent au *monde à soi*.

</div>

<div class="texte" data-id="4.1612">

<span>4.1612 &mdash; </span>L'empêchement de la révélation de l'enchevêtrement des mondes se retrouve même dans la suffisance autoritaire de la conceptualisation du matérialisme bourgeois, qui se refuse à défaire les déterminismes sociaux de la connaissance de son monde entendu comme totalité de la *nature*, par la simple tentative de sonder le *régime de savoir* qui détermine l'affirmation de *nature*.

</div>

<div class="texte" data-id="4.1613">

<span>4.1613 &mdash; </span>La satisfaction des intérêts de classe qui investissent le matérialisme bourgeois est la cause principale de l'imposition de ses règles propres en *lois naturelles*.

</div>

<div class="texte" data-id="4.1614">

<span>4.1614 &mdash; </span>Dire la société revient toujours à dire sa société moins celle des autres, si le *régime de savoir* à partir duquel cette énonciation a lieu n'est pas déconstruit.

</div>

<div class="texte" data-id="4.162">

<span>4.162 &mdash; </span>La dialectique du réel est une composition de dialectiques mondaines, perceptives du réel, qui nécessitent d'être comprises dans leur ensemble si l'on souhaite s'approcher de la complexité totale de la dialectique du réel.

</div>

<div class="texte" data-id="4.2">

<span>4.2 &mdash; </span>Le monde, en tant que biotope, est un espace des potentialités de la vie du sujet.

</div>

<div class="texte" data-id="4.21">

<span>4.21 &mdash; </span>Le monde, composé d'un enchevêtrement de mondes, doit s'entendre, à partir du *régime de savoir* moderne, comme un biotope d'exclusion, qui dispose des séparations face à ce qui n'est pas considéré comme vivant à son image.

</div>

<div class="texte" data-id="4.211">

<span>4.211 &mdash; </span>Un biotope *autre* constitue toujours une potentielle menace pour le biotope depuis lequel la raison moderne établit l'espace de son existence, que cette raison se présente dans un déploiement national ou universel.

</div>

<div class="texte" data-id="4.212">

<span>4.212 &mdash; </span>Un biotope *autre* n'est jamais compris comme un biotope *en soi* à partir du biotope qui considère son altérité.

</div>

<div class="texte" data-id="4.2121">

<span>4.2121 &mdash; </span>L'étranger est toujours l'étrange de soi-même.

</div>

<div class="texte" data-id="4.2122">

<span>4.2122 &mdash; </span>Considérer l'étranger comme une menace revient à refuser la potentialité de l'étrange qui se dissimule dans le sujet.

</div>

<div class="texte" data-id="4.2123">

<span>4.2123 &mdash; </span>Refuser la potentialité de l'étrange restreint l'opportunité d'accroître la potentialité à devenir dans l'*être*.

</div>

<div class="texte" data-id="4.22">

<span>4.22 &mdash; </span>L'humain a opposé les biotopes entre eux, et par cette opposition, les a exploités à l'aide de sa raison, quelquefois jusqu'à leur destruction.

</div>

<div class="texte" data-id="4.221">

<span>4.221 &mdash; </span>L'opposition des biotopes entre eux découle directement d'une raison s'autorestreignant à la spatialité de son possible, et n'arrivant pas à envisager celle de son impossibilité.

</div>

<div class="texte" data-id="4.222">

<span>4.222 &mdash; </span>L'exclusion biologique que l'humain a ainsi opérée jusqu'à aujourd'hui, des interventions eugéniques sur des peuples aux cultures divergentes jusqu'aux déforestations massives détruisant des écosystèmes entiers, se constitue malgré les apparences d'agression comme une mécanique subjective de défense.

</div>

<div class="texte" data-id="4.2221">

<span>4.2221 &mdash; </span>Puisque la raison prise dans sa réflexivité se sent menacée par ce qui diffère de sa constitution, elle perçoit, dans l'appréhension concentrique et statique de son environnement, ce qui est étranger à sa manière de se déployer dans l'*être* comme une menace à sa propre existence.

</div>

<div class="texte" data-id="4.2222">

<span>4.2222 &mdash; </span>L'attaque de ce qui est étranger au sujet s'entend comme une défense du sujet, limitant le sujet, puisque le sujet devient et ne connaît pas de limites à ce qu'il est.

</div>

<div class="texte" data-id="4.2223">

<span>4.2223 &mdash; </span>Considérer un élément étranger à son espace ontologique comme un élément pathogène se révèle être au contraire l'élément pathogène lui-même, par l'atrophie du *devenir* du sujet que cela provoque.

</div>

<div class="texte" data-id="4.2224">

<span>4.2224 &mdash; </span>Établir un biotope au détriment de biotopes étrangers, c'est-à-dire à partir d'espaces où le sujet ne se retrouve pas, d'espaces considérés comme substantiellement autres, par exemple avec l'opposition d'un biotope où évolue l'humain au biotope d'une forêt dont il est absent, va au renoncement à l'*être*.

</div>

<div class="texte" data-id="4.22241">

<span>4.22241 &mdash; </span>Tout renoncement à l'*être* échoue en une affirmation de l'*avoir*.

</div>

<div class="texte" data-id="4.22242">

<span>4.22242 &mdash; </span>Le biotope où domine l'humain moderne doit satisfaire sans limites ses velléités à un bonheur d'existence dans l'*avoir*, dédaigneux des difficultés ontologiques qu'impose un épanouissement dans l'*être*.

</div>

<div class="texte" data-id="4.22243">

<span>4.22243 &mdash; </span>Les ressources de biotopes étrangers à l'humain moderne doivent servir le *bien-avoir* du biotope humain au détriment d'un *bien-être* étranger à l'humain, qui contribue pourtant à la richesse de ses potentialités à être.

</div>

<div class="texte" data-id="4.223">

<span>4.223 &mdash; </span>L'exclusion biologique se définit à un stade ontologique comme une rétention de l'extension de l'appréhension du réel.

</div>

<div class="texte" data-id="4.2231">

<span>4.2231 &mdash; </span>L'humain se restreint lui-même en prétendant étendre l'espace de son *être* par la volonté seule d'étendre l'espace de son *avoir*.

</div>

<div class="texte" data-id="4.224">

<span>4.224 &mdash; </span>L'humain doit démanteler sa société en s'associant à ce qui semble le menacer : l'humain doit s'allier à ce qui diverge de lui.

</div>

<div class="texte" data-id="4.2241">

<span>4.2241 &mdash; </span>La voie de l'*être* est la voix de l'étranger. Elle demeure toujours la parole qui se fait autre, l'ombre qui chemine contre les évidences.

</div>

<div class="texte" data-id="4.2242">

<span>4.2242 &mdash; </span>Ce qui menace l'humain d'aliénation est la peur de son aliénation.

</div>

<div class="texte" data-id="4.2243">

<span>4.2243 &mdash; </span>Une issue à la déperdition humaine s'échafaude par les marges, par l'intersociété, par l'infrasociété, par l'*asociété*.

</div>

<div class="texte" data-id="4.2244">

<span>4.2244 &mdash; </span>L'humain doit se rendre au cœur de lui-même pour entendre tout ce qui se construit au-dessus la vie brute, et tout ce que cette construction culturelle et sociale empêche.

</div>

<div class="texte" data-id="4.2245">

<span>4.2245 &mdash; </span>L'humain doit cesser de s'empêcher d'*être* en transformant sa société en un *social* ontologique, à l'image d'une eusocialité biologique qui *est* dans chacune de ses interactions environnementales. Il doit chercher à *être* par les équilibres des biotopes auxquels il se confronte, jusqu'à un potentiel degré extraterrestre, cosmique de ceux-ci, et y déceler la voie harmonieuse d'extension du *devenir* qu'offre un matérialisme social et ontologique de la communion avec ce qui est étranger.

</div>

<div class="texte" data-id="4.23">

<span>4.23 &mdash; </span>La vision concentrique de la raison humaine qui ramène exclusivement le réel au sujet humain, et dans la modernité au sujet humain individuel, et fait indirectement du réel une réalité uniquement humaine, est le processus qui requiert un renversement épistémique.

</div>

<div class="texte" data-id="4.231">

<span>4.231 &mdash; </span>Le concentrique doit se faire *ex-centrique*, et l'existence doit devenir une *ex-sistence*, une tension qui va hors d'elle-même, qui cherche un chemin vers davantage de vie, vers davantage de communauté de la vie.

</div>

<div class="texte" data-id="4.232">

<span>4.232 &mdash; </span>Le mouvement dynamique et constant qui va *hors de soi* ne se manifeste pas dans une quelconque annihilation de l'existence, quelle soit celle du sujet ou du sujet étranger, mais dans une communauté de cette tension à *devenir*, tel un champ vectoriel de la vie qui cherche sa lente et permanente transsubstantiation.

</div>

<div class="texte" data-id="4.3">

<span>4.3 &mdash; </span>La classe sociale est le fondement de la dialectique politique qui confronte les mondes humains entre eux et les limite dans leur *devenir*.

</div>

<div class="texte" data-id="4.31">

<span>4.31 &mdash; </span>La classe sociale s'imbrique dans la décomposition du réel en mondes.

</div>

<div class="texte" data-id="4.311">

<span>4.311 &mdash; </span>La classe est un monde qui se compose à son tour d'une multitude de mondes se confrontant les uns aux autres et au réel.

</div>

<div class="texte" data-id="4.3111">

<span>4.3111 &mdash; </span>Les différents mondes au sein d'une classe ne sont pas de simples degrés individuels de la décomposition, mais des degrés groupaux de celle-ci, organisant des intérêts communs issus d'une compréhension commune et dialectique de leur réalité immédiate.

</div>

<div class="texte" data-id="4.32">

<span>4.32 &mdash; </span>Les classes luttent au travers de leur compréhension dialectique du réel, elles s'affrontent par la tentative d'imposition de leur réalité à celle de l'autre, au travers de l'outil idéologique que constitue la vérité.

</div>

<div class="texte" data-id="4.321">

<span>4.321 &mdash; </span>La seule lutte des classes valable réside dans la destruction totale des classes.

</div>

<div class="texte" data-id="4.3211">

<span>4.3211 &mdash; </span>La destruction des classes sociales passe par la destruction de leur vérité propre, imposant une rationalité cloisonnée et cloisonnante sur le réel.

</div>

<div class="texte" data-id="4.3212">

<span>4.3212 &mdash; </span>Une destruction des classes sociales mène à l'élargissement de la rationalité émanant de la dialectique du sujet au réel, et de ce fait aux potentialités de l'élargissement même du sujet, le plus pluriel et global socialement puisse-t-il devenir.

</div>

<div class="texte" data-id="4.322">

<span>4.322 &mdash; </span>L'harmonie sociale, qui découle d'une possible disparition des classes, n'est pas seulement une harmonie politique, mais surtout une harmonie ontologique d'un sujet accroissant la puissance de son *devenir*.

</div>

<div class="texte" data-id="4.323">

<span>4.323 &mdash; </span>La vérité est une illusion politique formée par l'imposition d'une compréhension unique du réel. Elle élude les compréhensions d'autres mondes se juxtaposant à elle, dans le dessein d'annihiler le vertige de l'infini des perspectives dialectiques sur le réel.

</div>

<div class="texte" data-id="4.3231">

<span>4.3231 &mdash; </span>L'imposition d'une compréhension du réel est l'instrument premier d'une classe sociale qui assoit sa domination par la suppression de toute compréhension différente aux intérêts de son existence. Cet instrument est l'instrument d'une soumission.

</div>

<div class="texte" data-id="4.3232">

<span>4.3232 &mdash; </span>L'imposition d'une compréhension du réel par un sujet à l'ensemble des sujets, individuels ou groupaux, va toujours à la soumission, soit celle s'exerçant sur le sujet dominé, soit celle du sujet dominé se dominant lui-même en refusant une extension de sa perception sur le réel.

</div>

<div class="texte" data-id="4.3233">

<span>4.3233 &mdash; </span>L'imposition d'une compréhension du réel sert aussi de moyen de distanciation, lorsque ce moyen est utilisé à partir d'une classe sociale dominée, refusant de saisir stratégiquement la compréhension du réel de la classe la dominant. Cet instrument n'aura alors pas la dimension politique et organisationnelle d'un pouvoir qui s'exerce sur l'ensemble d'une société, mais se limitera à une configuration de sauvegarde de son existence, sans envisager l'annihilation totale de toute soumission qu'une classe sociale pourrait subir.

</div>

<div class="texte" data-id="4.324">

<span>4.324 &mdash; </span>Toute restriction de la compréhension du réel est l'établissement d'une verticalité du savoir, et par là, d'une restriction du sujet par le sujet, exclusion *subjective* des possibles d'une rationalité dynamique et plurielle allant vers son élargissement.

</div>

<div class="texte" data-id="4.3241">

<span>4.3241 &mdash; </span>Seul un entendement *anarchiste*, dans le sens d'un savoir refusant sa hiérarchisation, peut se présenter comme un décloisonnement politique, et ce par le décloisonnement ontologique qu'il offre en libérant le sujet de sa seule vérité.

</div>

<div class="texte" data-id="4.325">

<span>4.325 &mdash; </span>Il est regrettable que les classes dominées, en restreignant leur compréhension du réel à leur seul point de vue, écartent de la sorte la possibilité d'une révolution sociale, non celle d'un simple renversement créateur de nouvelles classes dominantes et de nouvelles classes dominées, mais celle d'une destruction du masque de vérité.

</div>

<div class="texte" data-id="4.3251">

<span>4.3251 &mdash; </span>L'opacité sociale qui s'appose sur le réel par la vérité réduit la circulation du sens parmi la totalité des mondes. Elle empêche l'harmonie dynamique d'une compréhension sociale totale qui *va* à la conquête du réel par l'élargissement de ce que peut le sujet.

</div>

<div class="texte" data-id="4.4">

<span>4.4 &mdash; </span>La multitude aliénée, prise dans l'inconscience de la forme de son monde, n'a le pouvoir de se libérer qu'en *métamorphosant* politiquement son ontologie.

</div>

<div class="texte" data-id="4.41">

<span>4.41 &mdash; </span>La métamorphose ontologique de la multitude aliénée passe par une conscience agissante sur la forme même de son monde.

</div>

<div class="texte" data-id="4.411">

<span>4.411 &mdash; </span>La conscience agissante et *transformatrice* que peut avoir la multitude d'elle-même lui révèle sa potentialité créatrice des circonstances historiques et sociales de son existence.

</div>

<div class="texte" data-id="4.412">

<span>4.412 &mdash; </span>La multitude doit se saisir de son autonomie en renversant l'imagerie d'un système déterminant, indépendamment d'elle-même, les conditions de la vie qu'elle peut envisager pour elle-même.

</div>

<div class="texte" data-id="4.42">

<span>4.42 &mdash; </span>Il s'avère crucial de quitter une passivité politique vassale, animée par le phénomène de consommation au sein des conditions offertes par l'époque moderne, pour rejoindre une activité politique créatrice, animée par une action de consumation des conditions offertes par cette même époque.

</div>

<div class="texte" data-id="4.421">

<span>4.421 &mdash; </span>Dans le cadre des démocraties représentatives, le passage politique d'un état passif à un état actif correspond au passage de l'acte inconscient de soumission à celui conscient de transformation, c'est-à-dire à l'abandon de la représentation politique de la subjectivité.

</div>

<div class="texte" data-id="4.422">

<span>4.422 &mdash; </span>La politique est *en soi* une mécanique de contrôle limitant la vie à une compréhension idéologique de celle-ci, au service d'une classe dirigeante dominant les hiérarchies sociales.

</div>

<div class="texte" data-id="4.4221">

<span>4.4221 &mdash; </span>La chose religieuse est au service de la classe dominante. La chose religieuse est une sous-traitance de l'assujettissement.

</div>

<div class="texte" data-id="4.43">

<span>4.43 &mdash; </span>La libération de la multitude passe par une conscience émancipatrice de l'existence déchargée des présences intermédiaires entre la potentialité transformatrice de sa substance et le réel lui-même, dont elle ne peut pas *physiquement* se distinguer.

</div>

<div class="texte" data-id="4.431">

<span>4.431 &mdash; </span>La conscience émancipatrice d'une existence aliénée doit être une conscience cosmique d'une destitution du lien politique et d'une communion atomique au réel pour que se crée un espace de révolution, dans le sens que la révolution permet au vivant de sentir à nouveau l'indistinction de la vie et de la physique.

</div>

<div class="texte" data-id="4.4311">

<span>4.4311 &mdash; </span>La conscience émancipatrice d'une existence aliénée demeure le moyen d'annihiler le nihilisme de la chose religieuse.

</div>

<div class="texte" data-id="4.432">

<span>4.432 &mdash; </span>La libération de la multitude est une extension de la conscience de sa contingence. Elle est un retour révolutionnaire à la vie, à une vie qui va et devient dans son *allant*.

</div>

<div class="texte" data-id="5.">

<span>5. &mdash; </span>L'assujettissement bourgeois de la société moderne entraîne une confusion de l'*être* dans l'*avoir*, qui restreint l'*être* en tant que devenir.

</div>

<div class="texte" data-id="5.1">

<span>5.1 &mdash; </span>La société bourgeoise est la société qui évolue en son immobilité ontologique.

</div>

<div class="texte" data-id="5.11">

<span>5.11 &mdash; </span>La société bourgeoise est la société dont toutes les dimensions sociales sont polarisées par la classe bourgeoise.

</div>

<div class="texte" data-id="5.111">

<span>5.111 &mdash; </span>La classe bourgeoise dans une société bourgeoise conçoit le cadre épistémique depuis lequel se clôt le social.

</div>

<div class="texte" data-id="5.1111">

<span>5.1111 &mdash; </span>La polarisation autour de la classe bourgeoise pousse non plus à un épanouissement libre dans l'*être*, mais à un épanouissement limité aux conditions fixées par le cadre épistémique bourgeois.

</div>

<div class="texte" data-id="5.1112">

<span>5.1112 &mdash; </span>L'ontologie de la bourgeoisie est un simulacre de l'*être*, où tout *étant*, qu'il soit humain ou non humain, doit être mû par un *devenir-bourgeois* fixé par la question de l'*avoir*.

</div>

<div class="texte" data-id="5.1113">

<span>5.1113 &mdash; </span>L'objectif de tout membre de la bourgeoisie est de devenir toujours plus bourgeois que lui-même, c'est-à-dire de se déployer toujours plus dans ses capacités à *avoir*.

</div>

<div class="texte" data-id="5.1114">

<span>5.1114 &mdash; </span>L'accroissement de l'*avoir* bourgeois correspond toujours à un amoindrissement des capacités à *être*.

</div>

<div class="texte" data-id="5.112">

<span>5.112 &mdash; </span>La substance de la classe bourgeoise demeure l'individu-bourgeois, pris dans sa singularité, n'envisageant la singularité de l'autre que comme le moyen d'un possible développement de son emprise sur son environnement, et ce dans l'unique immédiateté de celui-ci.

</div>

<div class="texte" data-id="5.1121">

<span>5.1121 &mdash; </span>L'emprise de l'individu-bourgeois sur son environnement vise une utilisation productive et individuante de l'espace occupé.

</div>

<div class="texte" data-id="5.1122">

<span>5.1122 &mdash; </span>Le profit de l'occupation individuante de l'espace par l'individu-bourgeois demeure celui de l'*avoir* comme négation de l'*être*.

</div>

<div class="texte" data-id="5.113">

<span>5.113 &mdash; </span>La société bourgeoise s'organise autour de la classe bourgeoise comme autour d'un agrégat de sensations égoïstes qui se refusent à une pratique transformatrice du *soi* : le refus de sa multiplication, de son ouverture plurielle à ce qui *est* autre, et par conséquent, au sein de la société bourgeoise, à ce qui *a* autrement.

</div>

<div class="texte" data-id="5.1131">

<span>5.1131 &mdash; </span>L'ego survit par l'opacité qu'il applique sur la diversité des manières de *faire* sujet.

</div>

<div class="texte" data-id="5.1132">

<span>5.1132 &mdash; </span>La loi libérale moderne de l'ego est celle de l'apparente autonomie de ses intérêts égoïstes, dont la prééminence arrange le processus créateur de classes sociales.

</div>

<div class="texte" data-id="5.11321">

<span>5.11321 &mdash; </span>Les classes sociales disposent les cloisons nécessaires à la sauvegarde de la discrimination fondatrice de la société bourgeoise : un intérêt égoïste doit s'opposer à un autre intérêt égoïste pour subsister.

</div>

<div class="texte" data-id="5.1133">

<span>5.1133 &mdash; </span>L'identité moderne de l'ego advient par le jeu des intérêts égoïstes s'opposant, et dont l'opposition structure un ensemble social régi par les lois commandant aux besoins individuels.

</div>

<div class="texte" data-id="5.12">

<span>5.12 &mdash; </span>La société bourgeoise est la structure sociale qui tient ensemble par un entrelacs d'intérêts égoïstes des individus mus par une même tension, le plus souvent inconsciente, d'accaparement de la réalité dans laquelle ils évoluent.

</div>

<div class="texte" data-id="5.121">

<span>5.121 &mdash; </span>La bourgeoisie se définit comme la classe d'individus qui recherchent un accroissement de leur capital par l'usage de leur biotope, que le capital soit économique ou culturel.

</div>

<div class="texte" data-id="5.1211">

<span>5.1211 &mdash; </span>La bourgeoisie n'a de cesse qu'elle étende son biotope afin d'étendre sa capacité à exister par un accroissement de son *avoir* et de la jouissance de son *avoir*.

</div>

<div class="texte" data-id="5.1212">

<span>5.1212 &mdash; </span>L'extension de l'*avoir* se donne, dans la société bourgeoise, l'apparence d'une extension de l'*être* pour masquer son immobilité dans l'*être*.

</div>

<div class="texte" data-id="5.1213">

<span>5.1213 &mdash; </span>Les conséquences de l'immobilité ontologique de la société bourgeoise vont à l'épuisement de ce qu'elle *est* par son *avoir*, en d'autres termes, elles vont à une consommation totale et destructrice des moyens de subsistance au sein de son biotope.

</div>

<div class="texte" data-id="5.12131">

<span>5.12131 &mdash; </span>L'individu, centre de la société bourgeoise moderne, doit *avoir* pour *être*. Cet *avoir*, du fait des ressources limitées qui peuvent l'accroître, se détermine par une comparaison à l'*avoir* d'autrui. Pour *être*, il ne suffit pas d'*avoir* dans un absolu de l'*avoir*, mais il faut *avoir* au détriment de l'*avoir* de l'autre.

</div>

<div class="texte" data-id="5.12132">

<span>5.12132 &mdash; </span>La limite des ressources disponibles du biotope de la société bourgeoise est la clef de la mécanique discriminante de l'*avoir*. La limite des ressources à disposition de l'*avoir* est à la fois l'agent de sa finalité et le sens de sa fin.

</div>

<div class="texte" data-id="5.12133">

<span>5.12133 &mdash; </span>La société bourgeoise *existe* par la destruction de sa puissance dans l'*être*.

</div>

<div class="texte" data-id="5.1214">

<span>5.1214 &mdash; </span>La société bourgeoise est proprement nihiliste, mais d'un nihilisme *réactif*, sans vision. Elle détruit en tant qu'elle consomme, et elle consomme en tant qu'apparences d'un *mode d'être*, c'est-à-dire en tant que *mode d'avoir*, sans chercher à se renouveler ou à renouveler son biotope dans l'*être*.

</div>

<div class="texte" data-id="5.1215">

<span>5.1215 &mdash; </span>Le *mode d'avoir* de la société bourgeoise est un colonialisme de l'espace, qui la conduit à se projeter en celui-ci afin d'envisager un accaparement toujours plus grand du réel, compensant ses consommations destructrices de celui-ci.

</div>

<div class="texte" data-id="5.122">

<span>5.122 &mdash; </span>L'individuation du capital s'inscrit dans un tissu de liens avec d'autres capitaux individuels, dont les interactions renforcent la volonté d'*avoir* de l'égoïsme bourgeois et menacent les biotopes étrangers à celui-ci.

</div>

<div class="texte" data-id="5.1221">

<span>5.1221 &mdash; </span>Le capital doit s'entendre simplement comme la somme tant de l'*avoir* que des capacités à *avoir*. Le capital est donc tant l'*avoir* lui-même que sa potentialité réelle ou symbolique.

</div>

<div class="texte" data-id="5.1222">

<span>5.1222 &mdash; </span>Le capitalisme est la propension systémique à une accumulation du capital et de ses virtualités.

</div>

<div class="texte" data-id="5.13">

<span>5.13 &mdash; </span>La société bourgeoise est devenue le tout du social avec la modernité. Elle l'a étendu au tout de l'espace terrestre afin de maximiser son idéologie d'accaparement individuant.

</div>

<div class="texte" data-id="5.131">

<span>5.131 &mdash; </span>L'essor urbain a permis à la bourgeoisie de se constituer par la concentration d'un réseau de liens capitalistes, au sein duquel chaque individu participant au réseau bourgeois a pu plus facilement chercher à développer son capital propre.

</div>

<div class="texte" data-id="5.1311">

<span>5.1311 &mdash; </span>La société bourgeoise moderne a vu une juxtaposition du réseau des intérêts capitalistes et du réseau urbain, jusqu'à la confusion de ceux-ci.

</div>

<div class="texte" data-id="5.1312">

<span>5.1312 &mdash; </span>La juxtaposition du réseau bourgeois et du réseau urbain cherche à s'étendre sur ce qui leur est étranger, pour mieux accroître leur capacité à *avoir*.

</div>

<div class="texte" data-id="5.13121">

<span>5.13121 &mdash; </span>L'accroissement de l'*avoir* nécessite la création *culturelle* de la *nature* pour que s'y fabrique l'espace *étranger* où l'extension de l'*avoir* peut advenir. Cette création permet de fabriquer une frontière ontologique qui ne doit pas cesser de reculer. La création de la *nature* fait la carte pour faire le territoire d'une colonisation.

</div>

<div class="texte" data-id="5.13122">

<span>5.13122 &mdash; </span>Une frontière ontologique distingue à la fois les capacités d'accaparement et le champ de leur potentialité.

</div>

<div class="texte" data-id="5.132">

<span>5.132 &mdash; </span>La mécanique opérationnelle de la bourgeoisie, à savoir le capitalisme, a pu se développer par la facilitation offerte par le réseau urbain.

</div>

<div class="texte" data-id="5.1321">

<span>5.1321 &mdash; </span>Le capitalisme y a installé une dynamique de la possession, facilitant l'échange dans le cadre d'un réseau d'intérêts égoïstes.

</div>

<div class="texte" data-id="5.1322">

<span>5.1322 &mdash; </span>Le capitalisme a dirigé, à partir de son essor urbain, un mouvement ontologique constitutif de la société bourgeoise allant de l'environnement à l'individu, ou plus précisément des ressources de l'environnement à la puissance d'*avoir* de l'individu.

</div>

<div class="texte" data-id="5.1323">

<span>5.1323 &mdash; </span>La ville moderne, en tant que terreau du capitalisme, est ainsi une représentation de ceux qui la peuplent. Elle est l'extension destructrice sur le biotope, qui perdure jusqu'à l'épuisement ou du biotope lui-même ou des capacités individuelles à l'accaparement de ce qui l'environne.

</div>

<div class="texte" data-id="5.1324">

<span>5.1324 &mdash; </span>La ville moderne cherche à asseoir son existence par la possession individuante et colonisatrice de son environnement immédiat.

</div>

<div class="texte" data-id="5.14">

<span>5.14 &mdash; </span>La société bourgeoise doit être considérée comme un tout centré sur la classe bourgeoise et dont la périphérie, notamment prolétarienne, n'est là que pour servir sa dynamique d'accaparement égoïste, tout en faisant miroiter à cette périphérie un mouvement centripète de la possible accession à la classe bourgeoise par un travail servile à cette société même.

</div>

<div class="texte" data-id="5.141">

<span>5.141 &mdash; </span>La classe bourgeoise a pour caractéristique de se représenter comme le centre moteur d'une totalité mue par un mouvement d'inclusion, mais qui s'avère en réalité être un mouvement double, celui d'une exclusion-inclusion, où l'autoreprésentation de l'inclusion sert l'exclusion sociale comme outil d'asservissement psychologique utile aux intérêts d'un nombre restreint d'individus profitant de l'espoir des masses travailleuses d'une accession à davantage de possessions.

</div>

<div class="texte" data-id="5.142">

<span>5.142 &mdash; </span>Si l'on considère la société bourgeoise comme un cercle, son centre se trouve être la concentration maximale de capital, et les différentes strates sociales concentriques, des strates où se cristallisent certaines classes sociales, allant d'une aristocratie financière au lumpenprolétariat le plus excentré, sont animées par cette force centripète qui fait espérer à chaque classe sociale l'accession à une strate plus proche du centre de la société.

</div>

<div class="texte" data-id="5.143">

<span>5.143 &mdash; </span>Le fondement de la force centripète de la société bourgeoise, substance même de sa dynamique d'attraction sociale, est la dialectique principale entre deux classes majeures, le prolétariat et la bourgeoisie commerçante.

</div>

<div class="texte" data-id="5.1431">

<span>5.1431 &mdash; </span>Le prolétariat et la bourgeoisie commerçante se présentent comme des classes intermédiaires : une classe ouvrière servile au capital et une classe de contremaîtres servile à ce même capital, possession des seules classes industrielles et financières les dominant.

</div>

<div class="texte" data-id="5.144">

<span>5.144 &mdash; </span>Il s'avère indispensable pour l'hégémonie de la classe bourgeoise d'opérer une anesthésie de la classe prolétarienne, productrice de l'*avoir*, afin de maintenir intacte la force d'influence centripète de la société bourgeoise et d'éviter que périclite la polarisation centrale de cette société.

</div>

<div class="texte" data-id="5.1441">

<span>5.1441 &mdash; </span>L'élément clef de la modernité libérale, que nous qualifions d'hypermodernité, par rapport à un capitalisme industriel classique et encore restreint autour d'une certaine idée identitaire de nation, est d'avoir fait croire à un dépassement des classes sociales en diffusant culturellement le pouvoir d'attraction du capital en chaque acte social.

</div>

<div class="texte" data-id="5.14411">

<span>5.14411 &mdash; </span>L'hypermodernité est l'état du capitalisme tardif des échanges de l'*avoir* facilités par les techniques de l'information et de la communication.

</div>

<div class="texte" data-id="5.14412">

<span>5.14412 &mdash; </span>L'hypermodernité n'est pas un schisme au sein de la modernité, mais la libéralisation outrancière de ses capacités d'accaparement. L'hypermodernité est un état de la modernité.

</div>

<div class="texte" data-id="5.14413">

<span>5.14413 &mdash; </span>Ce qui est hypermoderne demeure moderne par essence.

</div>

<div class="texte" data-id="5.1442">

<span>5.1442 &mdash; </span>La culture de la société bourgeoise s'adapte à la plastique structurante de son idéologie.

</div>

<div class="texte" data-id="5.1443">

<span>5.1443 &mdash; </span>La culture de la société bourgeoise est à la fois le résultat de son idéologie et l'outil premier qui promeut son assise hégémonique.

</div>

<div class="texte" data-id="5.1444">

<span>5.1444 &mdash; </span>La tension culturelle sourde qui doit faire désirer en chaque acte social l'affirmation de sa direction vers le centre capitaliste de la société bourgeoise s'entend telle une essence indépassable.

</div>

<div class="texte" data-id="5.1445">

<span>5.1445 &mdash; </span>L'essentialisation de chaque acte social autour d'un désir de l'*avoir* est produite par l'omniprésence symbolique du capital.

</div>

<div class="texte" data-id="5.1446">

<span>5.1446 &mdash; </span>Le maximum de la concentration du capital équivaut à une pureté polarisante de son essence, qui doit être atteinte quel que soit le moyen.

</div>

<div class="texte" data-id="5.2">

<span>5.2 &mdash; </span>L'ontologie bourgeoise tient tout entière dans le cycle clos d'une corrélation individuante entre l'*être* et l'*avoir* : je suis parce que j'ai, j'ai parce que je suis.

</div>

<div class="texte" data-id="5.21">

<span>5.21 &mdash; </span>Il ne s'agit plus d'*être* pour *se* posséder, mais de posséder pour *être*.

</div>

<div class="texte" data-id="5.211">

<span>5.211 &mdash; </span>Le capitalisme exécute une confusion profitable, en faisant de l'*avoir* l'*être* de l'*avoir*.

</div>

<div class="texte" data-id="5.212">

<span>5.212 &mdash; </span>Toute action bourgeoise va à l'*avoir* : *ego cogito, ergo habeo*.

</div>

<div class="texte" data-id="5.22">

<span>5.22 &mdash; </span>C'est en la possession que se situe la symbolique du pouvoir.

</div>

<div class="texte" data-id="5.221">

<span>5.221 &mdash; </span>Il faut posséder pour être, que la possession soit en puissance ou actualisée.

</div>

<div class="texte" data-id="5.222">

<span>5.222 &mdash; </span>Pour la mécanique capitaliste, la symbolique du pouvoir politique se possède à partir de la possession elle-même.

</div>

<div class="texte" data-id="5.223">

<span>5.223 &mdash; </span>Il n'est pas question d'établir une distinction entre propriété et possession, mais de placer la symbolique du pouvoir dans la *puissance* possédante.

</div>

<div class="texte" data-id="5.2231">

<span>5.2231 &mdash; </span>La justification de la *puissance* possédante opérera une adaptation de la règle de propriété pour sauvegarder l'entièreté de cette *puissance*.

</div>

<div class="texte" data-id="5.224">

<span>5.224 &mdash; </span>Il faut faire montre socialement de ce que l'on possède pour faire montre
de son pouvoir politique au sein du réseau d'intérêts égoïstes que
représente la société bourgeoise.

</div>

<div class="texte" data-id="5.225">

<span>5.225 &mdash; </span>La force révolutionnaire de la bourgeoisie, classe détenant le capital, est d'avoir placé le pouvoir dans une récursivité symbolique de la possession : la possession conduit à un pouvoir symbolique conduisant lui-même à une tension vers davantage de possession.

</div>

<div class="texte" data-id="5.2251">

<span>5.2251 &mdash; </span>Le capitalisme a fait du geste d'accaparement une boucle d'accroissement de sa puissance.

</div>

<div class="texte" data-id="5.23">

<span>5.23 &mdash; </span>Le capitalisme a permis de se délester d'une religiosité ancestrale liée à une dynamique portant l'*être* de l'individu à l'*être* de son environnement en instaurant un mouvement inverse portant l'*avoir* de l'individu aux potentialités de l'*avoir* dans l'*être* de son environnement.

</div>

<div class="texte" data-id="5.231">

<span>5.231 &mdash; </span>Le déplacement du sacré de l'*être* à l'*avoir* a permis à la bourgeoisie, par une religiosité entourant la mécanique capitaliste, d'éroder les pouvoirs politiques ancestraux dont la verticalité se caractérisait par une opacité et une imperméabilité sociale.

</div>

<div class="texte" data-id="5.2311">

<span>5.2311 &mdash; </span>Les pouvoirs politiques ancestraux étaient directement issus d'une ontologie de l'*être*, et ce déplacement a soutenu l'adoption d'une ontologie de l'*avoir* centrée sur le capital, c'est-à-dire sur la valeur marchande qui le détermine.

</div>

<div class="texte" data-id="5.2312">

<span>5.2312 &mdash; </span>L'ontologie de l'*avoir* fonde dans l'ère capitaliste moderne une religiosité nouvelle, à partir de laquelle peut advenir une nouvelle prise de pouvoir politique.

</div>

<div class="texte" data-id="5.2313">

<span>5.2313 &mdash; </span>L'ontologie de l'*avoir* est la croyance en une valeur du réel conscrite à sa préhension marchande.

</div>

<div class="texte" data-id="5.2314">

<span>5.2314 &mdash; </span>Tout ce qui *est* a la potentialité de la marchandise. Tout ce qui *est* peut être *eu*.

</div>

<div class="texte" data-id="5.2315">

<span>5.2315 &mdash; </span>La marchandise est un morcellement du réel ramené à sa seule fonction d'échange. Par cette division du réel qui opacifie les liens dynamiques qui le sous-tendent, la marchandise obstrue l'*être* du réel en le bornant aux capacités de l'*avoir*.

</div>

<div class="texte" data-id="5.232">

<span>5.232 &mdash; </span>La verticalité de la prise de pouvoir politique dans la modernité se caractérise par une transparence et une perméabilité sociale apparente.

</div>

<div class="texte" data-id="5.2321">

<span>5.2321 &mdash; </span>Il est nécessaire de souligner l'apparence de cette perméabilité, qui agit comme une catalyse des échanges économiques et sociaux stimulant le capitalisme.

</div>

<div class="texte" data-id="5.2322">

<span>5.2322 &mdash; </span>L'adjonction d'une perméabilité sociale apparente à une transparence de la verticalité du pouvoir moderne limite l'ascension au pouvoir politique en établissant des voies uniques vers celui-ci, dont l'accès est conditionné par les mœurs d'un certain entre-soi des classes dominantes.

</div>

<div class="texte" data-id="5.2323">

<span>5.2323 &mdash; </span>L'idéologie moderne, par l'hégémonie culturelle qu'elle a installée dans la société bourgeoise, promeut la démocratie tout en s'assurant de l'efficacité de son illusion.

</div>

<div class="texte" data-id="5.233">

<span>5.233 &mdash; </span>Par la mécanique capitaliste, un renversement ontologique a lieu : l'*avoir* remplace l'*être*, l'individu ne va plus à son environnement en quête de lui-même, en quête des échos de ce qu'il *est*, mais rapporte sa réalité immédiate à sa personne pour faire de sa domination sur cette réalité, au travers de sa capacité d'*avoir*, une religiosité de la possession.

</div>

<div class="texte" data-id="5.234">

<span>5.234 &mdash; </span>L'appréhension de la société en tant que société bourgeoise est une restriction qui s'impose par l'idéologie cloisonnant la perception sociale dans la seule logique de la religiosité entourant l'*avoir* et sa production.

</div>

<div class="texte" data-id="5.24">

<span>5.24 &mdash; </span>La religiosité est passée de l'immanence d'une *essence* de l'environnement à une ubiquité de l'appréhension marchande de l'environnement : l'espace s'efface devant la technique d'emprise sur l'espace.

</div>

<div class="texte" data-id="5.241">

<span>5.241 &mdash; </span>Le transfert opératoire et apparent du sacré de l'*être* à l'*avoir* a fait du capitalisme un mouvement dissociatif de la symbolique d'un pouvoir politique ayant procédé d'une restriction idéologique de l'*être* jusqu'à la modernité, comme l'ont développé les religions dans la fondation de leur pouvoir politique.

</div>

<div class="texte" data-id="5.2411">

<span>5.2411 &mdash; </span>Le sacré lui-même demeure, inchangé dans sa substance, au cœur de la physique.

</div>

<div class="texte" data-id="5.2412">

<span>5.2412 &mdash; </span>Le sacré reste accessible au sujet qui tente par sa poursuite de l'*être* d'entendre derrière l'entendement qui s'impose.

</div>

<div class="texte" data-id="5.2413">

<span>5.2413 &mdash; </span>La poursuite de l'*être* est une brèche dans l'idéologie moderne.

</div>

<div class="texte" data-id="5.242">

<span>5.242 &mdash; </span>La modification de la verticalité du pouvoir politique par la bourgeoisie capitaliste est une révolution en ce qu'elle a transféré la religiosité dans le capital lui-même, faisant du capitalisme la religion moderne organisant les nouvelles structures du pouvoir politique autour d'une règle première : l'assouvissement des intérêts égoïstes.

</div>

<div class="texte" data-id="5.2421">

<span>5.2421 &mdash; </span>Le foisonnement moteur des intérêts égoïstes a été au fil des siècles un mouvement révolutionnaire de fond, dans le sens que ces intérêts égoïstes ont opéré une érosion lente du pouvoir politique issu du pouvoir ontologique en modifiant l'ontologie par son transfert vers un culte du rapport entre l'individu et le capital, à savoir le passage d'une mainmise sur la société à partir d'une maîtrise collective de l'*être* à une mainmise sur la société à partir d'une maîtrise individuelle de l'*avoir*.

</div>

<div class="texte" data-id="5.3">

<span>5.3 &mdash; </span>La concentration maximale de capital est masquée dans l'hypermodernité par l'idée de la concentration maximale de bonheur.

</div>

<div class="texte" data-id="5.31">

<span>5.31 &mdash; </span>Le bonheur, derrière le jeu d'ombres de sa représentation sociale, et tout en continuant de se situer dans la satisfaction des intérêts égoïstes du sujet individuel moderne, s'appréhende comme le nouveau centre à atteindre de la société bourgeoise hypermoderne.

</div>

<div class="texte" data-id="5.311">

<span>5.311 &mdash; </span>Au cœur des brumes communicationnelles de l'hypermodernité, le bonheur est le nouveau capital, il est l'opium en tant qu'opium, la douce perfusion de l'idéologie qui laisse étourdies, dans quelque jouissance aussi faible puisse-t-elle être, les masses opérantes du capital.

</div>

<div class="texte" data-id="5.3111">

<span>5.3111 &mdash; </span>Le bonheur est un bonheur par l'*avoir* : dans l'hypermodernité, le bonheur n'est plus le bonheur, quêtant une proximité harmonieuse des mouvements de l'*être*.

</div>

<div class="texte" data-id="5.3112">

<span>5.3112 &mdash; </span>Le bonheur est un objectif social, qui conditionne tant la vie de l'individu que ses rapports sociaux.

</div>

<div class="texte" data-id="5.3113">

<span>5.3113 &mdash; </span>Le désir du bonheur devient le bonheur lui-même, pour parer à l'illusion hypermoderne d'un *avoir* prétendu accessible à tous.

</div>

<div class="texte" data-id="5.3114">

<span>5.3114 &mdash; </span>Le bonheur exclut toute forme de bonheur étrangère à celui-ci.

</div>

<div class="texte" data-id="5.3115">

<span>5.3115 &mdash; </span>Le bonheur permet d'établir une éthique de la vie hypermoderne, qui l'encadre strictement.

</div>

<div class="texte" data-id="5.31151">

<span>5.31151 &mdash; </span>Le bien marchand devient le *bien* de l'éthique.

</div>

<div class="texte" data-id="5.32">

<span>5.32 &mdash; </span>La bourgeoisie est à la recherche de la pure jouissance, et son capital n'a de raison que dans la monstration de celle-ci.

</div>

<div class="texte" data-id="5.321">

<span>5.321 &mdash; </span>*La recherche du bonheur* illustre l'immobilité de la société bourgeoise tournée uniquement vers elle-même, et dont le seul moteur est l'autosatisfaction d'une puissance vectorielle d'accaparement de son environnement immédiat.

</div>

<div class="texte" data-id="5.3211">

<span>5.3211 &mdash; </span>Un matérialisme pris dans la fixité des apparences du réel, dans ses surfaces, ne satisfait que le point de vue depuis lequel il est émis, et n'a pas la capacité dynamique de creuser sous les évidences et contre ses intérêts, afin de révéler ce qui s'y dissimule.

</div>

<div class="texte" data-id="5.322">

<span>5.322 &mdash; </span>Pour libérer la société de la société bourgeoise, il s'impose de *désœuvrer* toute action sociale de son désir d'une accumulation de bonheur en tant qu'accumulation de capital.

</div>

<div class="texte" data-id="5.3221">

<span>5.3221 &mdash; </span>Le désir du bonheur hypermoderne est l'instrument idéologique qui alimente la permanence de la fonction *travail* de la société bourgeoise.

</div>

<div class="texte" data-id="5.3222">

<span>5.3222 &mdash; </span>Il faut purger l'idéologie bourgeoise en remontant à sa substance égoïste : l'individu à la poursuite du bonheur.

</div>

<div class="texte" data-id="5.3223">

<span>5.3223 &mdash; </span>S'avère nécessaire la déconstruction de la présence idéologique de l'*avoir* au centre du bonheur, afin de remédier, dans le cadre moderne, à l'aliénation par le travail.

</div>

<div class="texte" data-id="5.3224">

<span>5.3224 &mdash; </span>L'individu heureux est l'individu qui a bien travaillé : cette construction suit au plus près l'idéologie qui commande à un assujettissement bourgeois empêchant tout retour révolutionnaire à l'*être*.

</div>

<div class="texte" data-id="5.33">

<span>5.33 &mdash; </span>Le bonheur en tant qu'objectif d'existence doit être renié pour purger l'idéologie bourgeoise qui imprègne la totalité du social.

</div>

<div class="texte" data-id="5.331">

<span>5.331 &mdash; </span>La signification du bonheur passe dans la modernité soit, le plus souvent, par cette perspective de l'*avoir* où l'accaparement est maître, soit, dans de rares circonstances, par une éthique de l'*être* qui se tourne vers un dépouillement égoïste de l'*avoir* centré sur l'individu.

</div>

<div class="texte" data-id="5.3311">

<span>5.3311 &mdash; </span>Qu'il soit gouverné par un accaparement de l'*avoir* ou une éthique individuelle de l'*être*, le bonheur demeure une destination des intérêts égoïstes centrée sur l'*avoir*, destination figée dans le prisme de l'idéologie bourgeoise.

</div>

<div class="texte" data-id="5.3312">

<span>5.3312 &mdash; </span>Il est impossible d'accepter le bonheur moderne sans renoncer à la révolution ontologique qui doit permettre la métamorphose du sujet.

</div>

<div class="texte" data-id="5.332">

<span>5.332 &mdash; </span>S'en prendre directement au bonheur, qui est de nos jours le cache-sexe du jeu d'intérêts égoïstes moteur du capitalisme, pousse à s'en prendre à la mécanique censée mener au bonheur, c'est-à-dire à une accumulation individuelle du capital, accumulation qui paraît comme une symbolique sociale du pouvoir au sein du réseau d'interactions individuelles.

</div>

<div class="texte" data-id="5.333">

<span>5.333 &mdash; </span>La déconstruction du bonheur permet de percevoir les rouages individuants qui mènent à l'autoaliénation au sein de la société mercantile, où chacun marchande le propre sens de son temps à la recherche de l'assouvissement heureux de ses besoins, raison pour laquelle chacun se marchande lui-même par le labeur de cette recherche.

</div>

<div class="texte" data-id="5.334">

<span>5.334 &mdash; </span>Le bonheur évacué, la mécanique capitaliste est à nu, et se révèle la société derrière la société bourgeoise hypermoderne : un ensemble social parfois plus vaste, qui inclut ce dont la société bourgeoise se sert en silence pour asseoir sa dynamique capitaliste, comme la classe ouvrière clandestine ou d'autres franges illégales du lumpenprolétariat.

</div>

<div class="texte" data-id="5.3341">

<span>5.3341 &mdash; </span>La différence première entre la société bourgeoise et la société masquée par la société bourgeoise n'est toutefois pas l'étendue de cette dernière, mais la disparition du désir moteur d'une accumulation maximale du capital.

</div>

<div class="texte" data-id="5.3342">

<span>5.3342 &mdash; </span>La représentation concentrique de la société bourgeoise, débarrassée de son centre, se dépouille de la rigueur verticale de son organisation pour se modeler en une structure horizontale acentrée, où peuvent commencer à s'entrevoir des intérêts communs, au-delà de l'*avoir* et de l'idée restrictive de classe sociale.

</div>

<div class="texte" data-id="5.3343">

<span>5.3343 &mdash; </span>Il faut transgresser l'idéologie bourgeoise, par un émiettement des bornes des classes sociales qu'elle impose, par une modification de la destination centrale de son monde capitaliste, et revenir à la société même, diverse et unie par sa diversité, et se situer à l'orée d'un temps nouveau où se distingue une dynamique partageuse tournée vers une quête ontologique de la confrontation à l'environnement, à ce qui s'y cache, là où la vie interroge la vie même.

</div>

<div class="texte" data-id="5.4">

<span>5.4 &mdash; </span>La déconstruction de la société bourgeoise doit révéler une société qui peut enfin, au plus près de la physique, devenir un *social* sans adjectif, ni bourgeois ni même humain.

</div>

<div class="texte" data-id="5.41">

<span>5.41 &mdash; </span>Le *social* sans adjectif peut s'entendre comme une communauté du vivant, incluant tout ce que les sociétés ont exclu jusqu'à aujourd'hui : exclusion des autres, des modes autres du vivant. Cela peut aller des vies humaines étrangères aux mœurs locales, et bien plus loin pour l'entendement moderne, jusqu'au vivant quel qu'il soit : de l'animal au végétal, du bactériel jusqu'aux possibles du vivant extraterrestre.

</div>

<div class="texte" data-id="5.411">

<span>5.411 &mdash; </span>Le social se comprend comme un champ partagé de l'*être* qui cherche son extension.

</div>

<div class="texte" data-id="5.412">

<span>5.412 &mdash; </span>Le social se fait non seulement totalité de ce qui devient par la confrontation du réel, mais aussi communauté de ce *devenir*, englobant la forme humaine la plus typique à son environnement, tout comme la bactérie la plus étrangère à celui-ci.

</div>

<div class="texte" data-id="5.42">

<span>5.42 &mdash; </span>Le déplacement du sens du social, du social *sociétal* au social *social*, ouvre une perspective sur la distinction entre une communauté de l'immobilité, qui se cloisonne dans une compréhension fixe d'elle-même, et une communauté du mouvement, qui ne fixe qu'une dynamique commune à sa croissance.

</div>

<div class="texte" data-id="5.421">

<span>5.421 &mdash; </span>Malgré la proximité des termes *social* et *sociétal*, leur divergence éclairante quant à une politique *désœuvrante* de la société bourgeoise réside dans la perspective sur le réel qu'ils établissent.

</div>

<div class="texte" data-id="5.4211">

<span>5.4211 &mdash; </span>*Sociétal* insiste sur les composantes caractéristiques de la société et part de celles-ci pour établir ce qui les lie, tandis que le *social* procède inversement en établissant le liant, le champ de fluidité dans lequel s'inscrivent des éléments qui font *communauté* par ce qui leur est *commun*.

</div>

<div class="texte" data-id="5.4212">

<span>5.4212 &mdash; </span>Pour entendre le *commun*, il est possible de remonter jusqu'aux particules subatomiques qui lient ensemble, par leur identité *physique*, toutes les entités du réel.

</div>

<div class="texte" data-id="5.4213">

<span>5.4213 &mdash; </span>La langue allemande distingue les termes *sociétal* et *social* en opposant le *gesellschaftlich* au *sozial*. Le terme de *Gesellschaft*, société, se réfère à *Geselle*, le compagnon, le compagnonnage, tandis que *sozial*, remonte au latin *socius*, qui signifie ce qui est partagé, ce qui unit, avec cette idée de *suite* et de *liaison*, que l'on entend jusque dans la racine indo-européenne du verbe latin *suivre*, *sequor*.

</div>

<div class="texte" data-id="5.43">

<span>5.43 &mdash; </span>Un social de la non-exclusion émerge, contre la société humaine, à partir de l'entendement du *commun*.

</div>

<div class="texte" data-id="5.431">

<span>5.431 &mdash; </span>La société humaine est encore une restriction biologique aux formes de vie qui demeurent hermétiques à la communication humaine.

</div>

<div class="texte" data-id="5.432">

<span>5.432 &mdash; </span>Dans son incompréhension d'une sensation autre du vivant, l'humain aliène les formes étrangères à ses capacités communicationnelles, puisqu'il se trouve dans l'incapacité de faire mouvoir sa raison en dehors de son espace propre, de son *topos* d'existence.

</div>

<div class="texte" data-id="5.433">

<span>5.433 &mdash; </span>La raison doit s'échapper de son espace, à la recherche de l'extension permanente de celui-ci, pour découvrir ce qui lui est *commun* en dehors d'elle-même, et par cette découverte, pour devenir avec l'étrange qui peuple le *commun*.

</div>

<div class="texte" data-id="5.44">

<span>5.44 &mdash; </span>Pour déconstruire la société bourgeoise et retrouver le *social* qu'elle dissimule, il n'y a pas à distinguer un quelconque matérialisme de classe d'un matérialisme bourgeois, pour finir par l'y opposer.

</div>

<div class="texte" data-id="5.441">

<span>5.441 &mdash; </span>L'espoir ne se trouve pas dans un transfert d'un matérialisme de classe à un autre matérialisme de classe, par exemple, dans celui d'un matérialisme bourgeois à un matérialisme prolétaire.

</div>

<div class="texte" data-id="5.442">

<span>5.442 &mdash; </span>Il y a à détruire tout matérialisme de classe, à laisser le matérialisme le plus brut possible, dénué d'un quelconque intérêt politique discriminant. Le matérialisme peut alors confronter sa capacité analytique à l'ensemble du *social* et le rapporter ainsi à la matrice *physique* en laquelle toute matière advient et *devient*.

</div>

<div class="texte" data-id="6.">

<span>6. &mdash; </span>La critique constitue la dimension révolutionnaire de la *praxis* par ce qu'elle offre de transformation à la conscience du *devenir*, et ainsi au *devenir* même.

</div>

<div class="texte" data-id="6.1">

<span>6.1 &mdash; </span>La critique doit se faire parallaxe.

</div>

<div class="texte" data-id="6.11">

<span>6.11 &mdash; </span>Changer d'angle de vision entraîne la possibilité de mettre fin à l'illusion de l'indépendance des circonstances sociales et historiques.

</div>

<div class="texte" data-id="6.111">

<span>6.111 &mdash; </span>Le fait d'accepter une quelconque causalité où la vie humaine serait le fruit de circonstances sociales et historiques passées, mais ne pourrait pas agir directement sur leur état présent, revient à faire de la causalité une abstraction aliénante.

</div>

<div class="texte" data-id="6.1111">

<span>6.1111 &mdash; </span>Une action transformatrice dans le présent ne se contente pas d'être une action transformatrice de ce qui vient, elle est une action transformatrice d'elle-même, et ainsi de sa *présence au monde*.

</div>

<div class="texte" data-id="6.1112">

<span>6.1112 &mdash; </span>Le *devenir* est un mouvement qui advient dans l'instant de la *praxis*.

</div>

<div class="texte" data-id="6.1113">

<span>6.1113 &mdash; </span>Agir sur l'instant de la *praxis* rayonne tant dans le passé que dans l'avenir, sans oublier dans l'instant lui-même.

</div>

<div class="texte" data-id="6.1114">

<span>6.1114 &mdash; </span>La compréhension abstraite des circonstances causales déterminant la vie humaine représente le centre névralgique des démocraties contemporaines, qui font accroire une maîtrise politique indirecte de ces circonstances.

</div>

<div class="texte" data-id="6.1115">

<span>6.1115 &mdash; </span>La représentation politique moderne se nourrit de l'idée d'une impossibilité d'agir sur l'*agir* lui-même, dans son instant. Cela permet de limiter la grande majorité du pouvoir décisionnel autonome de la multitude, conservée par une classe sociale dirigeante, qui joue de cette abstraction causale pour justifier son pouvoir et dissimuler tant sa non-représentativité de la multitude que son exercice aliénant du pouvoir.

</div>

<div class="texte" data-id="6.112">

<span>6.112 &mdash; </span>La causalité sociale et historique de la vie humaine peut être saisie rétroactivement dans le présent.

</div>

<div class="texte" data-id="6.1121">

<span>6.1121 &mdash; </span>Une cause n'existe pas abstraitement en tant que cause, elle est toujours un effet si l'on change la perspective à partir de laquelle on l'aborde.

</div>

<div class="texte" data-id="6.1122">

<span>6.1122 &mdash; </span>L'appréhension d'une cause en tant qu'effet est un élément majeur de la parallaxe qui doit animer la critique.

</div>

<div class="texte" data-id="6.1123">

<span>6.1123 &mdash; </span>La cause est toujours un effet, mais elle ne l'est pas nécessairement d'une cause passée, elle peut l'être d'une cause à venir.

</div>

<div class="texte" data-id="6.1124">

<span>6.1124 &mdash; </span>La négation d'une unidirectionnalité de la causalité accorde à la *praxis* une conscience de son pouvoir transformateur.

</div>

<div class="texte" data-id="6.113">

<span>6.113 &mdash; </span>Se crée l'illusion que la vie se subit dans l'espoir d'une transformation politique indirecte à venir.

</div>

<div class="texte" data-id="6.1131">

<span>6.1131 &mdash; </span>Au sein de la société moderne dite démocratique, l'action décisionnelle ayant déjà été exercée par le vote, l'action elle-même s'amenuise dans l'attente du changement qui vient.

</div>

<div class="texte" data-id="6.1132">

<span>6.1132 &mdash; </span>L'illusion démocratique moderne, qui met en scène un pouvoir indirect de la multitude sur l'instant du vivant, sert une idéologie de la division sociale entre la minorité dirigeante mue par ses intérêts économiques et la majorité dirigée dans le but de servir les intérêts économiques des dirigeants.

</div>

<div class="texte" data-id="6.1133">

<span>6.1133 &mdash; </span>La majorité n'a toutefois pas conscience d'être une majorité dirigée, puisqu'elle considère avoir agi sur les circonstances politiques par le fait de choisir, à des intervalles relativement éloignés, une représentation de son pouvoir politique.

</div>

<div class="texte" data-id="6.1134">

<span>6.1134 &mdash; </span>La représentation politique de la majorité transfère de fait, dans les régimes politiques modernes, la quasi-totalité de sa puissance actionnelle à des personnages issus d'une classe sociale minoritaire, classe ne servant que ses intérêts propres et concentrant la quasi-totalité du pouvoir économique et politique : *la bourgeoisie décisionnelle*.

</div>

<div class="texte" data-id="6.1135">

<span>6.1135 &mdash; </span>Même dans le cadre d'une représentation populaire correspondant avec justesse à l'identité sociologique de la multitude, la représentation elle-même dégraderait les représentants en des agents politiques ne pouvant plus représenter avec conformité la multitude, en raison de la distance sociale créée par toute institution de la représentation.

</div>

<div class="texte" data-id="6.11351">

<span>6.11351 &mdash; </span>La distance sociale créée par toute institution de la représentation entretient à terme l'idéologie dominante par le gouvernement qu'elle impose à une multitude censée être représentée.

</div>

<div class="texte" data-id="6.1136">

<span>6.1136 &mdash; </span>Le système de la représentation politique corrompt par l'empêchement que la représentation oppose à l'action qui voudrait consciemment se saisir de son instant.

</div>

<div class="texte" data-id="6.114">

<span>6.114 &mdash; </span>Une classe sociale sert toujours ses intérêts propres. C'est la raison pour laquelle la critique doit mener à une société sans classe, grâce à une *praxis* consciente du *devenir* dans lequel elle s'inscrit.

</div>

<div class="texte" data-id="6.12">

<span>6.12 &mdash; </span>Les circonstances sociales et historiques qui déterminent la vie humaine ne sont pas des éléments extérieurs à la vie humaine.

</div>

<div class="texte" data-id="6.121">

<span>6.121 &mdash; </span>Établir une critique de la vie telle une parallaxe subjective sur le réel commande non seulement à l'inversion d'une causalité subjective, mais aussi à l'inscription de la vie détachée de ses oripeaux modernes dans une dynamique où se confondent sujet et objet, cause et effet.

</div>

<div class="texte" data-id="6.2">

<span>6.2 &mdash; </span>La critique doit être révolution et révolution d'elle-même.

</div>

<div class="texte" data-id="6.21">

<span>6.21 &mdash; </span>La révolution est une permanence du mouvement.

</div>

<div class="texte" data-id="6.211">

<span>6.211 &mdash; </span>Le mouvement astral demeure le modèle de toutes choses et de toutes révolutions.

</div>

<div class="texte" data-id="6.2111">

<span>6.2111 &mdash; </span>Lorsqu'un astre entame sa révolution, il ne revient pas en son seul point initial, puisque ce point évolue dans l'espace et le temps, comme l'indiquent les études sur la morphologie du cosmos et la dynamique de sa croissance.

</div>

<div class="texte" data-id="6.2112">

<span>6.2112 &mdash; </span>Après sa révolution, l'astre se retrouve certes en un point de son orbite elliptique qui semble identique, mais ce point se voit transformé dans l'espace-temps par la période qu'il a fallu à l'astre pour opérer son déplacement orbital.

</div>

<div class="texte" data-id="6.2113">

<span>6.2113 &mdash; </span>Les contempteurs de la révolution sociale utilisent parfois l'argument du retour sur soi, tantôt cosmologique tantôt étymologique, pour dire que la révolution n'est pas une transformation, mais un simple retour à un point de départ. Ils oublient néanmoins que tout retour est une transformation, et que la révolution d'un astre autour d'un point se place dans un *devenir* temporel juxtaposé à un déplacement spatial causé par la croissance de l'espace lui-même.

</div>

<div class="texte" data-id="6.212">

<span>6.212 &mdash; </span>Le phénomène de révolution ne se contente pas d'être un accident séparateur, une simple discontinuité au sein d'une continuité plus vaste, mais correspond à un mouvement transformateur de retour sur soi qui annonce déjà le mouvement révolutionnaire suivant.

</div>

<div class="texte" data-id="6.2121">

<span>6.2121 &mdash; </span>La révolution se comprend telle la permanence d'un recommencement transformateur.

</div>

<div class="texte" data-id="6.2122">

<span>6.2122 &mdash; </span>Appliqué à la critique et plus largement au phénomène social, lorsque la révolution s'adjoint à une conscience de son mouvement rotatif, non comme un retour en arrière, mais comme une dynamique du tournoiement propulsé dans le *devenir* plus vaste de son environnement, la révolution acquiert une autonomie transformatrice du sujet.

</div>

<div class="texte" data-id="6.22">

<span>6.22 &mdash; </span>La critique révolutionnaire n'est pas révolution en tant qu'événement singulier, mais permanence d'un tournoiement perceptif et transformateur.

</div>

<div class="texte" data-id="6.221">

<span>6.221 &mdash; </span>Le renversement critique qui a lieu par la critique révolutionnaire est un mouvement, mais il ne se contente pas d'être celui du déplacement, il est celui du retour perpétuel, celui de la révolution.

</div>

<div class="texte" data-id="6.222">

<span>6.222 &mdash; </span>La critique révolutionnaire est cette dynamique ontologique de la critique qui offre au sujet une autonomie de son *devenir* parallèle au *devenir* du réel.

</div>

<div class="texte" data-id="6.2221">

<span>6.2221 &mdash; </span>Le miroitement ontologique de la critique transforme la vision initiale du sujet à la suite des transformations successives tant de l'objet perçu que de la perception même.

</div>

<div class="texte" data-id="6.22211">

<span>6.22211 &mdash; </span>Le point de vue fait le sujet, et non l'inverse, et sa transformation le libère.

</div>

<div class="texte" data-id="6.223">

<span>6.223 &mdash; </span>La critique exige sa propre révolution si elle souhaite demeurer un discernement des mouvements à venir de l'*être*.

</div>

<div class="texte" data-id="6.224">

<span>6.224 &mdash; </span>La critique qui se fait révolutionnaire, en tant que permanence transformatrice par un retour sur soi, se fait de la sorte outil de libération des carcans sociaux qui s'imposent au sujet, surnuméraires à ceux du seul cadre de la physique.

</div>

<div class="texte" data-id="6.23">

<span>6.23 &mdash; </span>Un retour de la critique sur elle-même n'est pas suffisant si elle délaisse le mouvement de la *révolution suivante*.

</div>

<div class="texte" data-id="6.231">

<span>6.231 &mdash; </span>L'erreur réside dans le contentement du retour à une position première, considérant l'objet perçu et la perception dans un lien d'immobilité d'espace et de temps.

</div>

<div class="texte" data-id="6.2311">

<span>6.2311 &mdash; </span>La permanence du mouvement pousse la critique à s'élancer à nouveau depuis un point de vue purgé des liens qui l'entravent.

</div>

<div class="texte" data-id="6.2312">

<span>6.2312 &mdash; </span>Dans l'exemple d'une opposition entre le terrestre et le divin, il semble pertinent de résoudre l'encombrement divin des ciels par l'encombrement terrestre d'une structure familiale patriarcale, de déconstruire le phénomène social afin d'évider les lueurs stellaires de leurs superstitions. Toutefois, une fois cette saine opération effectuée, la révolution critique permet également au sujet un retour aux ciels, purgés des dieux, emplis d'un humain transformant et se transformant, conscient de s'y placer et d'y placer son *devenir* dans une connexion atomique avec ce que les ciels lui cachent encore.

</div>

<div class="texte" data-id="6.232">

<span>6.232 &mdash; </span>L'enchevêtrement spatio-temporel de la perception ne se contente pas de demeurer dans un état statique et abstrait, il se modifie continuellement en fonction de sa propre dynamique et de celle de son environnement.

</div>

<div class="texte" data-id="6.24">

<span>6.24 &mdash; </span>La révolution de la critique nécessite tant une conscience négatrice de son achèvement que le renouvellement de son processus perceptif, pour que la critique s'inscrive de manière vectorielle dans le mouvement spatio-temporel de l'enchevêtrement de l'objet perçu, du sujet percevant et de la perception les reliant.

</div>

<div class="texte" data-id="6.241">

<span>6.241 &mdash; </span>Le sujet doit mettre en mouvement le point de vue qui le façonne, s'il souhaite se dégager des obstructions qui l'empêchent de considérer les différences entre la forme passée et présente de son *devenir*.

</div>

<div class="texte" data-id="6.2411">

<span>6.2411 &mdash; </span>La perception *devient* avec le *devenir* de ce qui est perçu.

</div>

<div class="texte" data-id="6.2412">

<span>6.2412 &mdash; </span>Le mouvement perceptif va au mouvement du réel.

</div>

<div class="texte" data-id="6.242">

<span>6.242 &mdash; </span>La révolution continuelle du sujet par celle de son point de vue libère la critique et lui permet de déterminer la *praxis* à mener pour *devenir* avec autonomie.

</div>

<div class="texte" data-id="6.3">

<span>6.3 &mdash; </span>La critique doit aller à l'*être*, au plus près.

</div>

<div class="texte" data-id="6.31">

<span>6.31 &mdash; </span>La critique ne se projette pas simplement en amont du sujet, mais revient à l'*être* du sujet, dans un tournoiement qui lui accorde une autonomie de sa projection.

</div>

<div class="texte" data-id="6.311">

<span>6.311 &mdash; </span>Le sujet opère sa libération en se confondant à la dynamique de son environnement.

</div>

<div class="texte" data-id="6.312">

<span>6.312 &mdash; </span>Le déterminisme d'une telle vision, qui peut sembler au contraire cloisonner le sujet à un *devenir* régi par des causes extérieures, ne doit pas se limiter à être une contrainte, mais se présenter comme l'ensemble des conditions parmi lesquelles une conscience de celles-ci offre au sujet un champ plus vaste de son autonomie.

</div>

<div class="texte" data-id="6.3121">

<span>6.3121 &mdash; </span>Puisque toute autonomie demeure limitée par la physique, cette spatialité qui croît et dans laquelle tout *devient* en cette croissance même, une conscience critique de la croissance correspond à une conscience de la croissance du sujet conscient, et par là, à l'étendue possible de sa transformation.

</div>

<div class="texte" data-id="6.3122">

<span>6.3122 &mdash; </span>Comprendre le déterminisme en œuvre équivaut à comprendre l'étendue des possibles.

</div>

<div class="texte" data-id="6.32">

<span>6.32 &mdash; </span>L'idée de circularité de la critique entraîne une rotation perceptive où s'accumulent les variations de la perception du réel.

</div>

<div class="texte" data-id="6.321">

<span>6.321 &mdash; </span>Les variations perceptives du réel sont un enrichissement qui permet de saisir les variations d'un réel *devenant*, inscrit dans une accélération de son *devenir*.

</div>

<div class="texte" data-id="6.3211">

<span>6.3211 &mdash; </span>La circularité de la critique n'a de sens que dans le retour à l'endroit du sujet, après qu'il a *jeté hors de lui* et hors des conditions établies de sa perception sa capacité critique.

</div>

<div class="texte" data-id="6.322">

<span>6.322 &mdash; </span>La division analytique du réel s'établit par une permanence de la distinction lorsque la critique se déploie en un mouvement *global* allant au réel.

</div>

<div class="texte" data-id="6.323">

<span>6.323 &mdash; </span>La dynamique de la critique féconde un point de vue autre et multiple sur le réel, point de vue qui se présente en amont du sujet et enclenche sa transformation.

</div>

<div class="texte" data-id="6.324">

<span>6.324 &mdash; </span>Le renversement de la critique ne doit pas se contenter d'un simple exercice de parallaxe par rapport à l'idéologie empreignant les mécaniques perceptives du réel, elle doit se renverser elle-même à la recherche de ce qu'elle *est* et peut *être* dans l'*être*.

</div>

<div class="texte" data-id="6.33">

<span>6.33 &mdash; </span>Par sa fusion aux mutations *physiques* de l'espace, la *praxis* critique est une *praxis* de la conquête de l'espace ontologique.

</div>

<div class="texte" data-id="6.331">

<span>6.331 &mdash; </span>La conscience du mouvement critique est une autorégulation de la vision qui purge les impuretés de son optique en discernant ce qui s'impose à son point de vue, en outre de ce qui l'oblige *naturellement*.

</div>

<div class="texte" data-id="6.3311">

<span>6.3311 &mdash; </span>Il est important de souligner que ladite *nature* n'est pas, mais qu'elle devient indissociablement de ce qui advient en elle.

</div>

<div class="texte" data-id="6.3312">

<span>6.3312 &mdash; </span>La *nature* *n'est pas*, puisqu'elle *est* une *physique* en tant que *phusis*, c'est-à-dire une croissance, et qu'il se trouve impossible d'imposer un *état* de *nature* si ce n'est celui de la perpétuelle transformation.

</div>

<div class="texte" data-id="6.3314">

<span>6.3314 &mdash; </span>La transcription dans le social d'une fixité de la *nature* est le témoignage d'une idéologie réactionnaire qui refuse le vaste spectre d'une dite *nature* qui n'a de sens que par sa constante *re-naissance*.

</div>

<div class="texte" data-id="6.3315">

<span>6.3315 &mdash; </span>La *nature* doit s'envisager comme une *physique* dans toute sa dimension génératrice, perceptible du végétal à l'animal, du bactériel au stellaire, puisque la naissance est une naissance à nouveau, elle n'est pas une origine, mais un continuum de ce qui génère et se transforme, de ce qui se renouvelle.

</div>

<div class="texte" data-id="6.33151">

<span>6.33151 &mdash; </span>La naissance de l'objet est une naissance à nouveau du sujet.

</div>

<div class="texte" data-id="6.3316">

<span>6.3316 &mdash; </span>Toute revendication d'une quelconque *nature* est une intervention culturelle qui restreint le réel.

</div>

<div class="texte" data-id="6.332">

<span>6.332 &mdash; </span>La critique s'inscrit dans la transformation de l'espace de son *devenir* par un retour sur soi, et accroît sa puissance par la conscience de ce retour.

</div>

<div class="texte" data-id="6.3321">

<span>6.3321 &mdash; </span>Le retour de la critique sur elle-même est un retour de l'espace à l'espace, telle une transformation temporelle du sujet.

</div>

<div class="texte" data-id="6.333">

<span>6.333 &mdash; </span>La recherche de la critique est celle d'une représentation non aliénatrice.

</div>

<div class="texte" data-id="6.3331">

<span>6.3331 &mdash; </span>Une représentation non aliénatrice est consciente de n'être qu'une représentation.

</div>

<div class="texte" data-id="6.334">

<span>6.334 &mdash; </span>La *praxis* critique a lieu par une projection perceptive qui revient *révolutionnairement* à son point de vue initial en y opérant une mutation, et de ce fait, une mutation du sujet.

</div>

<div class="texte" data-id="6.335">

<span>6.335 &mdash; </span>Le mouvement de la *praxis* critique permet une résolution altératrice de ses fondements, grâce à une dynamique assainissant les contradictions sociales qui contraignaient son discernement initial et à une conscience de celles qui le contraignent encore.

</div>

<div class="texte" data-id="7.">

<span>7. &mdash; </span>La révolution des fondements ontologiques d'une société est une conséquence de la révolution des fondements épistémiques d'une société.

</div>

<div class="texte" data-id="7.1">

<span>7.1 &mdash; </span>La révolution politique est primordialement une révolution ontologique, et l'avènement de celle-ci réside dans les bouleversements de la technique sur l'agencement du savoir.

</div>

<div class="texte" data-id="7.11">

<span>7.11 &mdash; </span>Nous qualifions le déterminisme qui commande à la révolution ontologico-politique de *nécessité technique*.

</div>

<div class="texte" data-id="7.111">

<span>7.111 &mdash; </span>La révolution ontologico-politique, commandée par la *nécessité technique*, renverse la structuration de l'*épistémè* qui gouverne le social.

</div>

<div class="texte" data-id="7.1111">

<span>7.1111 &mdash; </span>Par *épistémè*, il faut entendre le cadre, socialement accepté, des connaissances de la réalité structurant *la* vérité, c'est-à-dire le *régime de savoir*, dans laquelle et par laquelle évolue une société. L'*épistémè* est donc le savoir contextualisé dans le social.

</div>

<div class="texte" data-id="7.112">

<span>7.112 &mdash; </span>La *nécessité technique* correspond à un état de la société qui n'arrive plus à se contenter de son *régime de savoir* limitant les perceptions et les interactions avec son environnement immédiat.

</div>

<div class="texte" data-id="7.1121">

<span>7.1121 &mdash; </span>Il serait erroné de concevoir la *nécessité technique* soit comme une cause soit comme un effet de la transformation *nécessaire* de l'*épistémè*. Elle demeure les deux à la fois dans un renversement de la causalité, qui fait de toute cause un effet, et inversement.

</div>

<div class="texte" data-id="7.1122">

<span>7.1122 &mdash; </span>La *nécessité technique* n'est pas un état de la technique produisant une *nécessité* du dépassement de la technique, ou une conscience du sujet qui en appelle à une transformation *nécessaire* de la technique par la conscience de ses propres limites. Elle se situe dans un même temps à ces deux niveaux, tout en ne se contentant pas de ce qu'ils disent tous deux du sens unique d'une causalité justifiant les transformations du réel.

</div>

<div class="texte" data-id="7.1123">

<span>7.1123 &mdash; </span>Le déterminisme de la *nécessité technique* ne s'entend pas à partir d'une linéarité causale expliquant le réel, mais par une vision réticulaire de celui-ci où tout événement se représente davantage comme une interaction toujours en devenir que comme une simple action entendue dans l'abstraction de ce qui l'anime et de ce qu'elle anime.

</div>

<div class="texte" data-id="7.113">

<span>7.113 &mdash; </span>La *nécessité technique* trouve dans l'avènement des techniques de l'information et de la communication, qui permettent de partager librement une information sur un réseau à la configuration acentrée, les conditions réunies pour qu'une nouvelle révolution ontologique advienne.

</div>

<div class="texte" data-id="7.114">

<span>7.114 &mdash; </span>Une révolution ontologique conduira la modernité à sa fin, par la remise en question de l'individu comme centre de l'ontologie gouvernant la réalité.

</div>

<div class="texte" data-id="7.12">

<span>7.12 &mdash; </span>La *nécessité technique* remet en cause l'*épistémè* d'une société en fracturant la structure close de son savoir.

</div>

<div class="texte" data-id="7.121">

<span>7.121 &mdash; </span>Une *fracture épistémique* transforme le *régime de savoir* d'une société, puisqu'elle impacte directement les fondements ontologiques à partir desquels le savoir se constitue.

</div>

<div class="texte" data-id="7.122">

<span>7.122 &mdash; </span>Une *fracture épistémique* est un marqueur d'une certaine chronologie de l'histoire humaine, qui ne se fonde pas directement sur l'événement, mais sur ses conditions.

</div>

<div class="texte" data-id="7.1221">

<span>7.1221 &mdash; </span>Les deux derniers événements qui ont créé une *fracture épistémique* capable de transformer les fondements de l'*épistémè* et de provoquer la transformation ontologique d'une époque ont été l'invention du caractère mobile de Gutenberg et le moteur *Boulton & Watt*. Ces inventions se trouvent à l'origine de ce que nous nommons respectivement l'ère classique où l'humain fut le centre épistémique et l'ère moderne où l'individu devint l'élément capital.

</div>

<div class="texte" data-id="7.1222">

<span>7.1222 &mdash; </span>Les révolutions de la fin du XVIII^e^ siècle n'ont été possibles qu'à travers l'institution de l'individu, en tant que granularité ontologique du phénomène social.

</div>

<div class="texte" data-id="7.1223">

<span>7.1223 &mdash; </span>L'avènement de l'individu correspond à celle d'une force de travail individualisée mue par une logique individualisante de la liberté, devant le contraindre à travailler pour pouvoir en tant qu'agent social dit libre consommer grâce au fruit seul de sa force de travail.

</div>

<div class="texte" data-id="7.1224">

<span>7.1224 &mdash; </span>L'individu et sa potentialité libérée des hérédités ont ainsi constitué le socle d'une ère nouvelle, basée sur l'illusion d'une autodétermination de l'action individuelle, qui perdure jusqu'à ce jour : la modernité.

</div>

<div class="texte" data-id="7.1225">

<span>7.1225 &mdash; </span>Ce n'est pas le travail qui rend libre, mais c'est l'institution de la liberté individuelle qui rend le travail non seulement possible, mais le constitue comme une condition *épistémique* indépassable de la modernité. La liberté individuelle est l'anesthésiant des aliénations nécessaires pour assurer la pérennité d'un marché capitaliste libre où seule la marchandise doit connaître pratiquement la liberté.

</div>

<div class="texte" data-id="7.1226">

<span>7.1226 &mdash; </span>Le capitalisme architecture le social autour de l'individu afin d'établir une hiérarchie entre des individus séparés les uns des autres par leurs intérêts égoïstes. Le modèle de l'individu libre pouvant gravir la hiérarchie sociale par sa force de travail au sein d'une hiérarchie considérée comme perméable est une abstraction qui assoit l'aliénation de l'individu par la croyance en sa liberté travailleuse.

</div>

<div class="texte" data-id="7.1227">

<span>7.1227 &mdash; </span>La séparation épistémique entre les individus dans la modernité permet de sauvegarder la notion centrale de propriété individuelle.

</div>

<div class="texte" data-id="7.12271">

<span>7.12271 &mdash; </span>La propriété de l'individu moderne est la propriété exclusive qu'il peut imposer sur sa réalité. Elle transfère par son exclusivité la gravitation sociale de l'individu à la réalité appropriée, c'est-à-dire à la marchandise.

</div>

<div class="texte" data-id="7.12272">

<span>7.12272 &mdash; </span>La propriété d'une marchandise, dans le cadre du capitalisme moderne qui assure la liberté de son transfert, prévaut sur le propriétaire de la marchandise.

</div>

<div class="texte" data-id="7.12273">

<span>7.12273 &mdash; </span>Dans la logique capitaliste, il ne s'agit pas d'un individu qui transmet, par le fruit de son travail, une marchandise à un autre individu, mais il s'agit d'une marchandise se servant d'une société monétaire fondée sur le travail individuel pour assurer sa circulation.

</div>

<div class="texte" data-id="7.1228">

<span>7.1228 &mdash; </span>Le capitalisme cloisonne la société par une architecture concrète, déterminée socialement, qui se sert d'abstractions afin d'étendre le plus possible sa vérité en la diffusant en toutes choses de sa réalité, en d'autres termes, en toutes marchandises entendues comme le tout objectif du réel, faisant du sujet un objet lui-même, se déplaçant et pérennisant ce tout objectif et marchand du réel.

</div>

<div class="texte" data-id="7.12281">

<span>7.12281 &mdash; </span>L'abstraction fait simulacre pour servir la verticalité concrète des aliénations, qui ont lieu dans la société capitaliste moderne.

</div>

<div class="texte" data-id="7.12282">

<span>7.12282 &mdash; </span>Toute la mécanique capitaliste réside dans le fait de jouer d'abstractions pour consolider en permanence une fluidification de la circulation de la marchandise.

</div>

<div class="texte" data-id="7.123">

<span>7.123 &mdash; </span>Il nous paraît nécessaire de spéculer sur les modifications épistémiques en cours en affirmant que la pensée réticulaire va prendre le pas sur la pensée individuelle.

</div>

<div class="texte" data-id="7.1231">

<span>7.1231 &mdash; </span>Nous vivons à nouveau les prémices des transformations consécutives à une *fracture épistémique* causée par l'invention d'un réseau informatique mondial acentré, où chaque personne peut librement y accéder, tant pour recevoir qu'émettre une information, malgré les résistances capitalistes qui tentent d'éroder sa caractéristique intrinsèque d'absence de centre pour réagir aux potentialités d'une telle *fracture épistémique*.

</div>

<div class="texte" data-id="7.1232">

<span>7.1232 &mdash; </span>La facilité technique de s'adjoindre au réseau, selon son principe de décentralisation de l'information, constitue l'élément clef de la *fracture épistémique* contemporaine.

</div>

<div class="texte" data-id="7.124">

<span>7.124 &mdash; </span>Il n'est en rien étonnant que les mécaniques du capitalisme moderne tentent de restreindre le principe moteur de décentralisation qui anime le réseau en constituant des pôles au sein du réseau limitant les individus dans leur volonté de se saisir des capacités techniques de décentralisation.

</div>

<div class="texte" data-id="7.1241">

<span>7.1241 &mdash; </span>La restriction de la décentralisation est une tentative de sauvegarde des mécaniques de centralisation indispensables au capitalisme.

</div>

<div class="texte" data-id="7.12411">

<span>7.12411 &mdash; </span>La recherche du profit au travers de la production de marchandises crée invariablement la recherche de l'accroissement d'un phénomène gravitationnel de concentration de la demande autour de l'offre. La marchandise cherche ainsi à se produire elle-même par l'intermédiaire de la notion de profit.

</div>

<div class="texte" data-id="7.1242">

<span>7.1242 &mdash; </span>La marchandise n'est pas une chose composant le réel, mais elle est le concept qui simule, à même la chose composant le réel, une valeur permettant à la fois un échange producteur du concept de marchandise et la diffusion de celui-ci.

</div>

<div class="texte" data-id="7.13">

<span>7.13 &mdash; </span>La pensée réticulaire advient par l'avènement d'un réseau acentré, cherchant à maintenir sa décentralisation, où l'intervention de sujets individuels laisse émerger la prééminence de leurs interactions sur leur identité.

</div>

<div class="texte" data-id="7.131">

<span>7.131 &mdash; </span>La pensée réticulaire, dont l'émotivité devient visible parmi les interactions sociales qui ponctuent les échanges entre inconnus sur le réseau informatique mondial, compose les prémices du *sujet réticulaire*.

</div>

<div class="texte" data-id="7.1311">

<span>7.1311 &mdash; </span>La pensée réticulaire se structure par une décentralisation du réseau qui assure son mouvement.

</div>

<div class="texte" data-id="7.1312">

<span>7.1312 &mdash; </span>La pensée réticulaire organise une rationalité nouvelle et collective, dont la mécanique relève d'une cybernétique autonomiste.

</div>

<div class="texte" data-id="7.132">

<span>7.132 &mdash; </span>Le *sujet réticulaire*, issu d'une multitude plurielle et connectée, sera caractérisé par une pensée dont l'unité introduira la dynamique d'une volonté collective et mouvante.

</div>

<div class="texte" data-id="7.133">

<span>7.133 &mdash; </span>La permanence de la réorganisation du *sujet réticulaire* se fera naturellement par une constante régulation des liens entre les nœuds du réseau ontologique, né du réseau technique.

</div>

<div class="texte" data-id="7.1331">

<span>7.1331 &mdash; </span>Les nœuds du réseau ontologique, individus modernes augmentés par les découvertes de l'ingénierie des sciences du vivant, joignant l'informatique au métabolisme humain, seront toujours davantage liés les uns aux autres par des connexions constamment renouvelées entre ceux-ci.

</div>

<div class="texte" data-id="7.1332">

<span>7.1332 &mdash; </span>La connectivité sociale accrue déterminera une autorégulation par l'interaction des nœuds du réseau ontologique. Elle organisera de la sorte une *épistémè* où le savoir véhiculé par les connexions entre les éléments constitutifs du réseau primera sur les connaissances engrangées par les éléments connectés eux-mêmes, dans leur individuation au sein du réseau.

</div>

<div class="texte" data-id="7.2">

<span>7.2 &mdash; </span>La fluidité de la circulation du savoir est la règle constitutive de la révolution ontologique qui mettra un terme à la modernité.

</div>

<div class="texte" data-id="7.21">

<span>7.21 &mdash; </span>Le flux d'informations, fondement de l'ontologie constitutive du *sujet réticulaire*, soulève la question cruciale de l'autonomie de sa circulation.

</div>

<div class="texte" data-id="7.211">

<span>7.211 &mdash; </span>Toute entrave au flux d'informations deviendra une attaque ontologique et politique mettant en péril l'organisation libre et démocratique d'un sujet pluriel organisé en un réseau autorégulé.

</div>

<div class="texte" data-id="7.22">

<span>7.22 &mdash; </span>L'essaim est la forme du *sujet réticulaire*.

</div>

<div class="texte" data-id="7.221">

<span>7.221 &mdash; </span>Aucun sujet *dans* l'essaim ne prime sur le sujet *de* l'essaim.

</div>

<div class="texte" data-id="7.222">

<span>7.222 &mdash; </span>Aucun sujet *dans* l'essaim ne perdurerait sans l'intelligence distribuée de l'essaim.

</div>

<div class="texte" data-id="7.223">

<span>7.223 &mdash; </span>Il n'y a pas à chercher un esprit de l'essaim, à disputer une théorie éthérée de son fonctionnement, mais à observer sa mécanique instinctive, son autorégulation, l'organisation collective de sa volonté, la distribution des tâches nécessaires à son développement.

</div>

<div class="texte" data-id="7.224">

<span>7.224 &mdash; </span>Au travers de la représentation du *sujet réticulaire* en tant qu'essaim demeure
néanmoins la conscience des hiérarchies qui perdurent en son cœur.

</div>

<div class="texte" data-id="7.225">

<span>7.225 &mdash; </span>Par le phénomène de l'essaim, nous insistons sur l'intelligence d'agrégation qui découle du mode de vie sociale préexistant à la vie animale.

</div>

<div class="texte" data-id="7.2251">

<span>7.2251 &mdash; </span>C'est dans la déconstruction du phénomène animal de l'essaim en un réseau acentré, à l'image d'une topologie maillée d'un réseau informatique, que la représentation du *sujet réticulaire* trouve une juste continuité.

</div>

<div class="texte" data-id="7.2252">

<span>7.2252 &mdash; </span>Par une autorégulation anarchiste du vouloir, au sein d'un système pair à pair dynamique, nous proposons un mode d'autonomie de la pensée réticulaire qui favorisera la radicalité de l'organisation démocratique du sujet pluriel lui-même.

</div>

<div class="texte" data-id="7.2253">

<span>7.2253 &mdash; </span>Ce n'est pas l'anachorète qui est le modèle, mais l'essaim parcourant le désert, dévorant l'espace qui se présente à lui.

</div>

<div class="texte" data-id="7.23">

<span>7.23 &mdash; </span>La pensée réticulaire se confond indistinctement à sa dimension actionnelle précisément dans le déploiement de son intelligence d'agrégation.

</div>

<div class="texte" data-id="7.231">

<span>7.231 &mdash; </span>Il ne s'agit pas de mettre en avant une théorie ontologique du savoir, mais de montrer que l'ontologie peut avant tout devenir une *praxis* du savoir, en d'autres termes une pratique consciente des potentialités transformatrices de la mise en action sociale du savoir.

</div>

<div class="texte" data-id="7.3">

<span>7.3 &mdash; </span>Le savoir est un ordre de la perception de l'ordre du réel, qui ne connaît aucune hiérarchie, ni dans ce qui l'engendre ni dans ce qu'il engendre.

</div>

<div class="texte" data-id="7.31">

<span>7.31 &mdash; </span>La considération du désordre du réel est une incompréhension de l'ordre du réel.

</div>

<div class="texte" data-id="7.32">

<span>7.32 &mdash; </span>Le savoir s'organise en répondant aux nécessités matérielles de l'organisme qui tente de créer du sens à partir de ses perceptions.

</div>

<div class="texte" data-id="7.321">

<span>7.321 &mdash; </span>La construction du savoir se fait de manière anarchiste, c'est-à-dire que sa concrétion ne connaît pas d'*arkhè*.

</div>

<div class="texte" data-id="7.3211">

<span>7.3211 &mdash; </span>La concrétion du savoir ne résulte pas d'une nécessité première, mais se modèle en fonction des nécessités présentes.

</div>

<div class="texte" data-id="7.322">

<span>7.322 &mdash; </span>La *nécessité technique* qui préside au savoir est une variation de la réponse à l'accident, ce dernier advenant dans sa situation spatiale singulière par les conditions structurant la physique même.

</div>

<div class="texte" data-id="7.323">

<span>7.323 &mdash; </span>Il n'est pas possible de considérer la physique animant le réel comme une *arkhè*, puisque sa substance est le *devenir*, la permanence d'un mouvement n'ayant de cesse de se transformer lui-même.

</div>

<div class="texte" data-id="7.3231">

<span>7.3231 &mdash; </span>La physique est omniprésente dans la construction par l'humain du sens accordé à sa réalité, telle une *antiarkhè*, dont la perpétuelle transformation avive la transformation de toutes choses.

</div>

<div class="texte" data-id="7.3232">

<span>7.3232 &mdash; </span>La primauté de la physique est absente. Elle n'est que l'ordre harmonieux, sans hiérarchie, d'une spatialité qui fait place à une ubiquité de la transformation.

</div>

<div class="texte" data-id="7.3233">

<span>7.3233 &mdash; </span>La physique ne se contente pas d'être un ordonnancement humain de lois qui décrivent les mécaniques d'un espace restreint à l'univers tel que l'envisage l'humain, mais se fait harmonie des transformations potentielles de l'espace dans le multivers qu'elles induisent.

</div>

<div class="texte" data-id="8.">

<span>8. &mdash; </span>L'infinie contingence de la matière compose un matérialisme de l'infinitude, qui ne peut être réduit ni à aucune intuition de la sensation ni à aucune matérialité de la matière, mais qui doit *être* avec la matière, dans une sensualité de son *devenir*.

</div>

<div class="texte" data-id="8.1">

<span>8.1 &mdash; </span>L'usage abstrait du réel correspond à l'usage d'un outil d'aliénation sociale qui empêche de sentir l'immédiateté concrète du réel.

</div>

<div class="texte" data-id="8.11">

<span>8.11 &mdash; </span>Le sensible n'a de réalité abstraite qu'en tant qu'outil rhétorique servant le discours dominant, celui qui a le luxe de parler du sensible et de s'en satisfaire.

</div>

<div class="texte" data-id="8.111">

<span>8.111 &mdash; </span>La modernité *travaille* la vie pour produire le simulacre économique consistant à placer dans le travail une ontologie creuse de la matérialité, une ontologie de l'*avoir* : il faut travailler pour *gagner* sa vie.

</div>

<div class="texte" data-id="8.1111">

<span>8.1111 &mdash; </span>Le simulacre moderne consiste à placer dans la vie la seule croyance économique en son développement. L'économie s'y entend comme un ensemble de règles qui renferme la réalité d'une société sur elle-même, en lui permettant de fixer une valeur aux éléments de celle-ci, qui découle directement de la fragmentation préhensible et transmissible de cette réalité.

</div>

<div class="texte" data-id="8.1112">

<span>8.1112 &mdash; </span>L'idéologie dominante de la modernité, par le biais de sa religiosité *évaluante*, dans le glissement moderne qui délaisse le centre gravitationnel de la famille pour occuper celui de l'individu, va du salut de l'âme à la consommation de son salut.

</div>

<div class="texte" data-id="8.1113">

<span>8.1113 &mdash; </span>L'impératif catégorique de la modernité, celui de la nécessité universelle du travail comme fonction émancipatrice, travail permettant seul et contradictoirement d'offrir la vacance du travail, assoit l'illusion d'une liberté sociale acquise uniquement par le biais de la valeur du labeur individuel. La liberté sociale se construit donc, dans la modernité, comme une liberté individuelle, exclusive de l'autre.

</div>

<div class="texte" data-id="8.1114">

<span>8.1114 &mdash; </span>Travailler est l'action qui place l'humain dans un rôle d'agent économique individuel, dont le temps s'adjoint à celui de l'autre tout en l'excluant, au travers de l'établissement d'une concurrence interindividuelle. Cette concurrence demeure oublieuse des mécaniques collectives, qui commandent tant au simulacre de l'économie moderne qu'à la vie même que cette économie tente d'obstruer.

</div>

<div class="texte" data-id="8.1115">

<span>8.1115 &mdash; </span>Pour assurer la stabilité de l'économie moderne, la vacance doit uniquement consister en une négativité *gagnée* du travail, et jamais en une négation du travail *vaincu*.

</div>

<div class="texte" data-id="8.112">

<span>8.112 &mdash; </span>La qualité du sensible pour les masses travailleuses n'est celle que de son labeur et du repos en vue du travail à reprendre.

</div>

<div class="texte" data-id="8.1121">

<span>8.1121 &mdash; </span>Le sensible est marqué, au cours de la journée travailleuse de l'humain moderne, par son objectif salarial servant le temps de repos accordé par le temps de travail.

</div>

<div class="texte" data-id="8.1122">

<span>8.1122 &mdash; </span>Le sensible, en tant que satisfaction de sa propre jouissance, se limite pour les masses travailleuses au simulacre de la vacance du travail, qui ne s'obtient que par le travail lui-même. Le simulacre consiste à faire disparaître la possibilité d'une absence du travail par la négation du travail.

</div>

<div class="texte" data-id="8.1123">

<span>8.1123 &mdash; </span>Les satisfactions qui s'échappent d'une vie mesurée par le travail ne se construisent que dans l'acceptation du travail comme condition de la vie moderne.

</div>

<div class="texte" data-id="8.113">

<span>8.113 &mdash; </span>Le prolétariat demeure la classe qui ne dispose pas d'une maîtrise suffisante sur le réel pour sonder les limites du sensible.

</div>

<div class="texte" data-id="8.1131">

<span>8.1131 &mdash; </span>Le prolétariat est la classe qui ne peut que se contenter d'engendrer sa suite travailleuse au bénéfice des classes dominantes, soit dans l'illusion de cet engendrement soit dans l'incapacité d'y remédier.

</div>

<div class="texte" data-id="8.1132">

<span>8.1132 &mdash; </span>Le monde sensible pour le prolétariat, parfois obstrué par un phénomène religieux, le plus souvent masqué par la consommation capitaliste comprise comme instrument de jouissance de la vacance du travail, passe uniquement par le prisme du travail.

</div>

<div class="texte" data-id="8.1133">

<span>8.1133 &mdash; </span>Il importe à la classe dominante de pérenniser l'impossibilité pour le prolétariat de disposer ontologiquement du réel.

</div>

<div class="texte" data-id="8.1134">

<span>8.1134 &mdash; </span>L'ontologie moderne s'assure par la consolidation d'une vacance qui fait accroire au prolétariat que la vie consiste en une jouissance temporelle des débordements du temps de travail obtenus par le seul travail.

</div>

<div class="texte" data-id="8.1135">

<span>8.1135 &mdash; </span>Le prolétariat est affirmé dans l'inconscience de son aliénation par une ontologie qui place dans le seul travail le moyen d'obtenir une vacance du travail. Toute idée sociale de réforme du travail qui perpétue le travail participe à l'inconscience de cette aliénation.

</div>

<div class="texte" data-id="8.1136">

<span>8.1136 &mdash; </span>La classe dominante consolide l'ontologie moderne en ramenant toute chose de la réalité moderne à la valeur du travail qui la fonde.

</div>

<div class="texte" data-id="8.1137">

<span>8.1137 &mdash; </span>L'ontologie moderne peut s'entendre comme une temporalité du travail, qui ordonne la totalité de la vie.

</div>

<div class="texte" data-id="8.114">

<span>8.114 &mdash; </span>La politique moderne se servira tantôt de la jouissance individuelle, tantôt de sa potentielle négation, au travers de phénomènes collectifs menacés qui ont pourtant une consistance étiolée dans la modernité, telles la famille et la patrie, pour assurer sur sa réalité une emprise totale de l'ontologie moderne de l'*avoir*.

</div>

<div class="texte" data-id="8.1141">

<span>8.1141 &mdash; </span>L'usage négatif de phénomènes collectifs, qui se dissolvent pourtant invariablement dans le présent individuant de la modernité, doit s'entendre à partir du seul individu, agent économique de la consommation. La politique moderne invoque la famille ou la patrie négativement, dans l'absence de leur consistance passée, pour faire face à de potentielles menaces extérieures qui remettraient en cause la jouissance moderne de l'individu. Cette invocation fortifie l'illusion du travail individuel, seul facteur d'une jouissance individuelle menacée par la présence de l'autre.

</div>

<div class="texte" data-id="8.1142">

<span>8.1142 &mdash; </span>Il faut que la croyance en l'aliénation sociale se place dans la figure étrangère plutôt que dans le travail lui-même.

</div>

<div class="texte" data-id="8.1143">

<span>8.1143 &mdash; </span>La xénophobie latente à l'usage négatif de la famille ou de la patrie ne sert que la pérennisation de la mécanique capitaliste assurant à la marchandise sa libre domination sur la société moderne.

</div>

<div class="texte" data-id="8.12">

<span>8.12 &mdash; </span>La pensée doit s'assumer en tant qu'action et se déployer contre toute abstraction, y compris celle qui s'empare du sensible pour en faire une catégorie idéelle de plus.

</div>

<div class="texte" data-id="8.121">

<span>8.121 &mdash; </span>Le sensible demeure un élément de la langue, qui évoque médiatement une catégorie idéelle de l'immédiat.

</div>

<div class="texte" data-id="8.1211">

<span>8.1211 &mdash; </span>L'histoire du mot sensible véhicule une capacité abstractive qui a contrario crée un espace où règne la seule abstraction.

</div>

<div class="texte" data-id="8.1212">

<span>8.1212 &mdash; </span>La notion éthérée du sensible a l'arrogance d'évoquer la confrontation perceptive au réel tout en se plaçant au-dessus de celui-ci, dans une abstraction de la langue.

</div>

<div class="texte" data-id="8.1213">

<span>8.1213 &mdash; </span>Dans un cadre social, l'évocation du sensible devrait dire l'immédiateté du quotidien travailleur à la recherche de son émancipation. Toutefois, en gardant à distance la qualification précise et douloureuse de l'ordinaire du labeur, la propension à se saisir *sensiblement* de l'environnement immédiat du travail s'aliène. Dans un cadre social, l'usage du terme sensible relève de l'idéologie dominante et se contente de dire l'immédiateté du quotidien sans le travail, tout en le figeant dans la distance de son aliénation.

</div>

<div class="texte" data-id="8.1214">

<span>8.1214 &mdash; </span>Parler du sensible à l'humain aliéné par son travail, sans tenter d'épuiser la liste exhaustive des causes douloureuses de son aliénation, est une cruauté de la pensée bourgeoise, puisque, par la dureté du labeur, le lien qui l'unit au réel s'éprouve dans la douleur de la domination économique.

</div>

<div class="texte" data-id="8.1215">

<span>8.1215 &mdash; </span>Sentir l'immédiateté du réel doit être le moyen de sa traversée : une voie résolvant la médiate immédiateté suivante.

</div>

<div class="texte" data-id="8.122">

<span>8.122 &mdash; </span>Le désir d'accroître ses capacités de possession du réel mène au souhait d'une toujours plus grande croissance, non de l'*être*, mais de l'*avoir*.

</div>

<div class="texte" data-id="8.1221">

<span>8.1221 &mdash; </span>La mécanique substantielle du capitalisme, stimulant ses acteurs vers un renforcement de la puissance d'accaparement de leur environnement, instille à travers toutes les classes sociales une aliénation dans l'*être* lui-même, tentant d'empêcher le déploiement de toute ontologie.

</div>

<div class="texte" data-id="8.1222">

<span>8.1222 &mdash; </span>La classe dominante qui détient le capital profite seule, par l'exploitation économique des masses travailleuses, d'une jouissance du lien sensible au réel, mais dans toute sa dimension sensuelle. Elle bénéficie de ses possessions pour goûter au réel, lorsqu'elle n'est pas prise elle-même par un certain désir d'accumuler davantage de possessions, la dissociant de l'immédiateté de son existence.

</div>

<div class="texte" data-id="8.1223">

<span>8.1223 &mdash; </span>La classe détenant le plus de capital se trouvera souvent aussi empêchée de jouir de ce qu'elle *a* déjà, par l'universalité de la notion de travail qui fonde l'ontologie moderne de l'*avoir* et obstrue les jonctions sensibles vers l'*être*. Elle ne pourra que médiatement se connecter aux plaisirs aristocratiques que lui propose pourtant l'ère moderne qu'elle domine.

</div>

<div class="texte" data-id="8.1224">

<span>8.1224 &mdash; </span>La modernité fait de la classe qui possède le plus la classe la *meilleure*, et non la classe qui jouit le plus de ses possessions, pour que se promeuve automatiquement une ontologie de l'*avoir* centrée sur la notion de valeur marchande, et conséquemment sur celle de travail nécessaire à sa production.

</div>

<div class="texte" data-id="8.123">

<span>8.123 &mdash; </span>Une des réponses politiques au toujours plus de la croissance, qui anime la modernité, mais dont la modernité commence à découvrir sa puissance de destruction, est la décroissance. Toutefois, cette réponse est une continuité de la logique ontologique moderne centrée sur l'*avoir*.

</div>

<div class="texte" data-id="8.1231">

<span>8.1231 &mdash; </span>La croissance n'est pas le problème de la modernité, c'est la croissance dans l'*avoir* qui le demeure.

</div>

<div class="texte" data-id="8.1232">

<span>8.1232 &mdash; </span>Répondre à la croissance par une décroissance, en demeurant dans une ontologie de l'*avoir*, ne réglera pas les difficultés d'*existence* du sujet.

</div>

<div class="texte" data-id="8.1233">

<span>8.1233 &mdash; </span>Il est nécessaire d'abandonner une croissance dans l'*avoir* pour affirmer une croissance collective dans l'*être*. L'affirmation d'une croissance collective dans l'*être*, refusant toute croissance individuelle ou collective dans l'*avoir*, permettra au sujet d'exister harmonieusement dans son *devenir*.

</div>

<div class="texte" data-id="8.124">

<span>8.124 &mdash; </span>L'abstraction des mécaniques qui consolident l'*avoir* permet à l'idéologie de diffuser son écrasement en devenant un tout du savoir, rendant impossible la construction de toute pensée hors du cadre de son *régime de savoir*.

</div>

<div class="texte" data-id="8.1241">

<span>8.1241 &mdash; </span>L'idéologie dominante de la modernité use abstraitement du réel pour le quantifier en tant que marchandises échangeables.

</div>

<div class="texte" data-id="8.1242">

<span>8.1242 &mdash; </span>L'idéologie dominante de la modernité se contente de la seule mesure de l'*avoir* pour dire ce qui est et former autour de cette illusion de l'*être* les potentialités de sa valeur d'échange.

</div>

<div class="texte" data-id="8.125">

<span>8.125 &mdash; </span>La tendance à l'accaparement du réel, qui contamine la granularité moderne qu'est l'individu, disparaît du phénomène de la multitude lorsqu'elle est considérée comme une unité plurielle et mouvante, et non comme un seul agrégat d'individus mus par des intérêts égoïstes.

</div>

<div class="texte" data-id="8.1251">

<span>8.1251 &mdash; </span>La multitude *est* avant d'*avoir*, et elle *a* dans le respect du *devenir* de son *être*.

</div>

<div class="texte" data-id="8.1252">

<span>8.1252 &mdash; </span>Envisager l'unité ontologique de la pluralité imprime un schisme dans l'idéologie séparatrice de la modernité, qui interdit l'acception d'unité ontologique de ce qui est multiple au-delà de toutes les mécaniques de l'*avoir*.

</div>

<div class="texte" data-id="8.1253">

<span>8.1253 &mdash; </span>L'accaparement ne s'impose pas au phénomène pluriel de la multitude, qui ne se trouve pas animée par une volonté de posséder, mais par celle de sa croissance ontologique.

</div>

<div class="texte" data-id="8.1254">

<span>8.1254 &mdash; </span>La multitude n'a que faire de posséder son environnement au détriment d'un *mode d'être* harmonieux de l'espace qu'elle occupe. La multitude cherche à se posséder elle-même.

</div>

<div class="texte" data-id="8.2">

<span>8.2 &mdash; </span>Le sensible ne se découvre que dans une *praxis* de la vie se confrontant à elle-même et à l'environnement qui la maintient.

</div>

<div class="texte" data-id="8.21">

<span>8.21 &mdash; </span>Le sensible relie l'humain à sa réalité immédiate, et ce, à chaque instant, pour que cette connexion à son environnement trace les contours mouvants de son *devenir* propre.

</div>

<div class="texte" data-id="8.211">

<span>8.211 &mdash; </span>Le sensible ne commence à devenir pratique qu'à partir de l'instant où un peu d'autonomie s'offre à l'humain afin qu'il puisse se placer dans un lien conscient avec son environnement, qui relie le *tout* matériel du réel à celui de sa sensibilité propre.

</div>

<div class="texte" data-id="8.212">

<span>8.212 &mdash; </span>La permanence du sensible a été érodée tantôt par la religiosité du discours dominant qui empesa la vie humaine durant des siècles, tantôt par la reformulation moderne de cette religiosité en une vision de la vie humaine centrée sur le seul individu consommateur, vision à partir de laquelle a pu se développer une logique économique qui voila la pratique du sensible derrière la seule perception de son exploitation quantifiable.

</div>

<div class="texte" data-id="8.2121">

<span>8.2121 &mdash; </span>L'idéologie moderne *totalise* l'économie de sa réalité.

</div>

<div class="texte" data-id="8.21211">

<span>8.21211 &mdash; </span>La réalité moderne est la totalité du réel, par l'intermédiaire d'une ontologie de
l'*avoir* qui donne à toute chose une valeur, et impose de ce
fait à toute chose son paradigme ontologique, le travail.

</div>

<div class="texte" data-id="8.21212">

<span>8.21212 &mdash; </span>Rien n'existe au-delà du sensible moderne, espace où advient le travail.

</div>

<div class="texte" data-id="8.2122">

<span>8.2122 &mdash; </span>Dans l'emprise économique de la modernité sur le réel, tout espace est un espace de l'économie de ce qui *est*.

</div>

<div class="texte" data-id="8.2123">

<span>8.2123 &mdash; </span>La religiosité qui précéda la modernité donnait l'espoir d'un dépassement lénifiant du sensible, résumé en la seule souffrance terrestre, tandis que la modernité fabrique, à partir de cette religiosité ancienne centrée sur la souffrance, une impossibilité ontologique de son dépassement. Tout n'*est* que dans l'économie de la perception moderne du réel.

</div>

<div class="texte" data-id="8.21231">

<span>8.21231 &mdash; </span>La seule solution moderne à la souffrance causée par le travail est le travail lui-même, au cœur du sensible, dans l'évacuation de toute idée de transcendance.

</div>

<div class="texte" data-id="8.21232">

<span>8.21232 &mdash; </span>L'abstrait des tromperies religieuses, par l'idée abstraite de transcendance, présentait l'avantage d'accorder aux masses laborieuses l'espoir d'une paix déconnectée des souffrances du labeur quotidien. L'intelligence de la modernité fut de clore la réalité moderne sur elle-même en faisant du réel le simulacre d'une totalité économique qui ne connaît aucun dépassement, mais où chaque individu-consommateur, en d'autres termes le sujet moderne, pourrait connaître par son seul travail une rédemption.

</div>

<div class="texte" data-id="8.21233">

<span>8.21233 &mdash; </span>La rédemption moderne consiste en un *bien-avoir* présenté comme un *bien-être*, par une accumulation suffisante de capital permise par le travail, donnant l'illusion de pouvoir disposer de manière inépuisable de la réalité immédiate.

</div>

<div class="texte" data-id="8.2124">

<span>8.2124 &mdash; </span>La modernité ne s'embarrasse plus des affres provoquées par le travail, et de ses possibles apaisements au-delà du sensible, qui promettent à la vie une continuité parmi les contingences idéalistes du réel.

</div>

<div class="texte" data-id="8.2125">

<span>8.2125 &mdash; </span>Tout se restreint aux lois économiques qui s'apposent sur le réel en tant que *lois naturelles*.

</div>

<div class="texte" data-id="8.213">

<span>8.213 &mdash; </span>Le mouvement de libération de la conception moderne du sensible ne consiste pas à *tirer le monde à soi*, mais à sentir son *devenir* en parallèle du *devenir* du monde.

</div>

<div class="texte" data-id="8.2131">

<span>8.2131 &mdash; </span>Le sensible s'entend pour l'existence qui va hors d'elle-même, pour l'existence qui *ex-siste*.

</div>

<div class="texte" data-id="8.2132">

<span>8.2132 &mdash; </span>C'est dans la recherche de l'autonomie qui offre à la multitude une autorégulation que s'abandonne l'idée du sensible en tant que perception médiate de l'individu, par le crible économique de la valeur accordée au réel, pour avancer celle d'une *praxis* du sensible en tant que sensualité pratique de la multitude.

</div>

<div class="texte" data-id="8.23">

<span>8.23 &mdash; </span>Le *bien-être* social ne se situe pas dans les discours qui promettent des améliorations du quotidien par l'*avoir*, mais dans une pratique qui dépasse la simple satisfaction des besoins et accorde à l'envi la plénitude des sens.

</div>

<div class="texte" data-id="8.231">

<span>8.231 &mdash; </span>La plénitude des sens ne peut être envisagée, avant même d'être atteinte, que par le dépassement du simulacre qui fait du *bien-être* un *bien-avoir*.

</div>

<div class="texte" data-id="8.2311">

<span>8.2311 &mdash; </span>La jouissance moderne s'inscrit toujours dans un marché de la jouissance, qui a pour substance sa valeur d'échange.

</div>

<div class="texte" data-id="8.2312">

<span>8.2312 &mdash; </span>La modernité identifie la résolution des souffrances des classes travailleuses, soit par une jouissance d'un *avoir* acquis par la seule force de travail, soit par l'invocation d'une menace extérieure, personnifiée souvent par les figures étrangères du travail illégal, mettant en cause le libre accès au marché du travail et ainsi à celui de la jouissance de l'*avoir*.

</div>

<div class="texte" data-id="8.232">

<span>8.232 &mdash; </span>La pratique collective du *bien-être* social par les sens constitue l'assurance d'une justice sociale distribuée dans la totalité du phénomène social.

</div>

<div class="texte" data-id="8.2321">

<span>8.2321 &mdash; </span>Un pouvoir commun de la disposition de *soi* témoigne de l'équilibre d'une pluralité humaine à même de prendre conscience, par la conscience de la physicalité du *soi* social, d'une concordance des échelles du *bien-être*, de celle du corps humain à celle du corps social, jusqu'à celle du corps non animal, voire jusqu'à celle d'un au-delà du corps, au cœur d'un environnement qui doit être aujourd'hui senti dans sa dimension cosmique.

</div>

<div class="texte" data-id="8.2322">

<span>8.2322 &mdash; </span>À l'échelle de l'humain comme à celle du social, le corps se comprend comme un nœud de sens qui en appelle à l'épanouissement physique afin de densifier les possibles de sa croissance.

</div>

<div class="texte" data-id="8.2323">

<span>8.2323 &mdash; </span>Le dépassement de la modernité passe par la disparition de la représentation du sujet qui fait face au cosmos et par la construction de celle du sujet qui *est* le cosmos.

</div>

<div class="texte" data-id="8.233">

<span>8.233 &mdash; </span>Le sensible n'a pas de réalité abstraite qui pourrait être catégorisée, il est une chose concrète dont la compréhension parcellaire met en exergue les violences sociales, qui fondent de tout temps les mécaniques de discrimination dans l'exercice des sens.

</div>

<div class="texte" data-id="8.2331">

<span>8.2331 &mdash; </span>L'humain considéré socialement inférieur ne devra pas pleinement jouir de ses sens, dans tous les cas, il ne le pourra pas à l'image de l'humain considéré socialement supérieur. L'idéologie fait en sorte que ce devoir devienne une impossibilité dans le pouvoir.

</div>

<div class="texte" data-id="8.23311">

<span>8.23311 &mdash; </span>La jouissance du pauvre est une démesure qui offense l'économie de la réalité.

</div>

<div class="texte" data-id="8.2332">

<span>8.2332 &mdash; </span>En opposition aux idéologies astreignant les classes socialement dominées à une maîtrise réduite du sensible, il s'avère nécessaire de détruire l'idée du sensible pour la refondre en celle d'une sensualité collective, qui permettra à la multitude de goûter une dimension pratique de la confrontation au réel dans ce qu'elle peut avoir d'éclairant sur l'étendue qui s'offre à la capacité humaine diffuse de sentir, et ainsi d'*être* collectivement.

</div>

<div class="texte" data-id="8.3">

<span>8.3 &mdash; </span>Une écologie de la sensualité propose au sujet une manière d'habiter l'espace de son *être*, par le biais d'une raison découlant des sens et se confrontant harmonieusement à cet espace.

</div>

<div class="texte" data-id="8.31">

<span>8.31 &mdash; </span>L'écologie de la sensualité se façonne en tant que *praxis* de la multitude dans l'*être*. Elle y affirme une communion avec l'environnement immédiat du sujet et avec l'espace contingent du cosmos, là où l'*être* projette les puissances de son extension.

</div>

<div class="texte" data-id="8.311">

<span>8.311 &mdash; </span>L'écologie de la sensualité, qui va du *soi* du sujet au cosmos, révèle que l'*être* en dehors de l'*être* de la multitude demeure une identité de l'*être* de la multitude.

</div>

<div class="texte" data-id="8.3111">

<span>8.3111 &mdash; </span>Les spécificités d'un *mode d'être* conservent une identité avec leur *non-être* par leur contingence à *être*, et à *être* totalement autrement.

</div>

<div class="texte" data-id="8.3112">

<span>8.3112 &mdash; </span>L'identité est toujours un *devenir* de la déviance.

</div>

<div class="texte" data-id="8.3113">

<span>8.3113 &mdash; </span>Tout ce qui diverge de l'*être* prolonge l'*être*.

</div>

<div class="texte" data-id="8.312">

<span>8.312 &mdash; </span>Tout en l'*être* va à une harmonie de l'*être*, c'est-à-dire à un *bien-être* de son accroissement.

</div>

<div class="texte" data-id="8.32">

<span>8.32 &mdash; </span>La sensualité met en lumière la dimension concrète des sens humains qui ont cette capacité à être satisfaits. Le terme latin de *sensualitas* nous suggère une capacité de sentir, malgré une langue française qui fait de la sensualité la seule recherche de la satisfaction des sens.

</div>

<div class="texte" data-id="8.321">

<span>8.321 &mdash; </span>La pratique qui sous-tend la recherche d'une satisfaction des sens ne doit pas être considérée comme une pratique recherchant sa réalisation, mais comme une manière d'*être* s'installant dans une harmonie avec le réel.

</div>

<div class="texte" data-id="8.3211">

<span>8.3211 &mdash; </span>La *praxis* de la sensualité n'est harmonieuse que lorsqu'elle cherche à établir une jonction constructrice avec ce qui l'entoure.

</div>

<div class="texte" data-id="8.3212">

<span>8.3212 &mdash; </span>Toute sensualité qui se développerait aux dépens des autres ou de son environnement correspondrait à un nihilisme revenant finalement à une destruction des équilibres propices à la satisfaction sensorielle de l'ensemble de la multitude.

</div>

<div class="texte" data-id="8.3213">

<span>8.3213 &mdash; </span>Harmonie sociale et harmonie environnementale se construisent uniformément par une *praxis* des sens à une échelle collective.

</div>

<div class="texte" data-id="8.3214">

<span>8.3214 &mdash; </span>Une sensualité commune à la multitude ne peut être qu'une sensualité de la préservation et de l'extension de la multitude.

</div>

<div class="texte" data-id="8.322">

<span>8.322 &mdash; </span>La sensualité, qui s'entend de manière contre-intuitive dans une modernité où plaisir rime avec individuation du plaisir, étend son potentiel d'harmonie lorsque sa *praxis* est distribuée avec équivalence au sein du phénomène collectif que compose la multitude.

</div>

<div class="texte" data-id="8.3221">

<span>8.3221 &mdash; </span>La distribution équilibrée de la *praxis* de la sensualité participe à l'union d'une multitude bariolée. Elle affermit les liens souples qui unissent ses mouvements pluriels, telles les cadences foisonnantes de l'essaim cherchant la plénitude de son *être* dans l'espace qu'il compose ontologiquement, en d'autres termes dans l'espace qu'il *est*, puisqu'il *est* avec l'espace.

</div>

<div class="texte" data-id="8.3222">

<span>8.3222 &mdash; </span>La sensualité pratique de la multitude est l'expression concrète d'un équilibre qui se bâtit sur le *bien-être* de toutes les entités formant le phénomène social, et ce par une juste reconnaissance de leur existence physique qui appelle non seulement à l'assouvissement de leurs besoins premiers, mais également à l'épanouissement des agréments sensoriels de leur *devenir*, puisque tout *devenir* demeure une concrétude qui s'enchevêtre dans des champs matériels plus vastes de *devenirs*.

</div>

<div class="texte" data-id="8.33">

<span>8.33 &mdash; </span>Le *bien-être* d'un groupe social n'est pas une chose abstraite, mais un phénomène concret composé d'une diversité d'éléments qui appellent au contentement de l'*être* afin de densifier l'harmonie collective qu'ils composent.

</div>

<div class="texte" data-id="8.331">

<span>8.331 &mdash; </span>La sensualité pratique correspond à une satisfaction de la vie singulière dans son unicité connectée à la dynamique multiple du vivant, comprise comme un *bien-être* social qui s'affirme d'abord par la plénitude sensorielle de chacun.

</div>

<div class="texte" data-id="8.332">

<span>8.332 &mdash; </span>C'est l'épanouissement des entités d'un groupe social qui entraîne l'épanouissement du groupe lui-même en tant qu'unité, et établit de ce fait un *bien-être* social étendu à tout le phénomène collectif.

</div>

<div class="texte" data-id="8.3321">

<span>8.3321 &mdash; </span>La sensualité participe à la constitution en chaque entité d'une plus grande acuité de l'interconnexion entre toutes les entités du phénomène collectif et social les reliant. Elle développe ainsi une fluidité des échanges qui porte l'épanouissement général de la multitude.

</div>

<div class="texte" data-id="8.3322">

<span>8.3322 &mdash; </span>L'harmonie du *devenir* de la multitude s'assure par le *bien-être* de sa pluralité entendue comme unité.

</div>

<div class="texte" data-id="8.333">

<span>8.333 &mdash; </span>La *praxis* de la sensualité est pour la multitude une disposition du nombre en quête de son harmonie à la fois interne et externe, dans laquelle les entités composant le nombre vivraient connectées les unes aux autres et à leur environnement dans un équilibre sensoriel protecteur de leur *bien-être*, mais également de celui de leur environnement, espace de leur croissance ontologique.

</div>

<div class="texte" data-id="8.4">

<span>8.4 &mdash; </span>Le matérialisme est une perception ontologique de la matière qui se construit au-delà des intuitions de la perception.

</div>

<div class="texte" data-id="8.41">

<span>8.41 &mdash; </span>L'intuition est une complaisance de la vision qui se devance elle-même, qui appréhende sa réalité en l'immédiateté de la confrontation à celle-ci et fait de cette immédiateté une totalité du réel.

</div>

<div class="texte" data-id="8.411">

<span>8.411 &mdash; </span>L'intuition se saisit de l'immédiateté de sa réalité, dirige aveuglément la raison en elle, et stagne en l'immobilité de ses présupposés.

</div>

<div class="texte" data-id="8.4111">

<span>8.4111 &mdash; </span>L'intuition n'est qu'un éclaireur de la raison, elle lui indique un point de départ, parmi d'autres, pour fouiller derrière les évidences.

</div>

<div class="texte" data-id="8.41111">

<span>8.41111 &mdash; </span>Pour entendre l'*intuition*, ses dangers et ses opportunités, il peut être utile de confronter son étymologie latine, qui remonte jusqu'à la racine *in-tueor*, observer, veiller *dans*, à sa traduction allemande *An-schauung*, le regard qui se porte *à*, qui se porte *contre*.

</div>

<div class="texte" data-id="8.4112">

<span>8.4112 &mdash; </span>L'intuition pour l'intuition est une inintelligence de la raison, puisqu'elle construit une *vision* sans la dynamique nécessaire à toute exploration du réel, sans la méthode de *tenir ensemble*, en se satisfaisant uniquement de ce qui se présente à elle.

</div>

<div class="texte" data-id="8.4113">

<span>8.4113 &mdash; </span>L'intuition n'a d'utilité que si elle est le moyen pour la raison de se devancer elle-même, de découvrir *une* voie parmi les voies possibles d'exploration, et non *la* voie.

</div>

<div class="texte" data-id="8.412">

<span>8.412 &mdash; </span>L'intuition perçoit les surfaces, mais le matérialisme commande à percevoir sous les surfaces.

</div>

<div class="texte" data-id="8.42">

<span>8.42 &mdash; </span>Le matérialisme intuitif ne peut pas être la totalité du matérialisme, il ne peut être qu'un matérialisme des surfaces, un *superficialisme*.

</div>

<div class="texte" data-id="8.421">

<span>8.421 &mdash; </span>Un matérialisme restreint à l'intuition est un matérialisme mutilé. C'est une taxidermie de la matière.

</div>

<div class="texte" data-id="8.422">

<span>8.422 &mdash; </span>Les surfaces composent la totalité de ce qu'appréhende l'intuition, mais l'intuition ne doit pas être entendue comme une finalité de l'entendement, mais comme un simple avènement de celle-ci, puisqu'elle n'a pas la capacité de se projeter après l'immédiateté de son avènement.

</div>

<div class="texte" data-id="8.43">

<span>8.43 &mdash; </span>Le contre-intuitif a cela de supérieur à l'intuitif qu'il force la raison à voguer contre le courant des évidences, et à découvrir la face cachée de son cheminement docile, déterminé par les conditions sociales de sa constitution.

</div>

<div class="texte" data-id="8.431">

<span>8.431 &mdash; </span>Par une raison pratiquant le contre-intuitif, la mécanique de son déploiement peut être reconstruite *contre* les évidences que lui dictent ses axiomes actuels.

</div>

<div class="texte" data-id="8.4311">

<span>8.4311 &mdash; </span>Le matérialisme doit permettre à la raison de percevoir le réel au-delà de la vérité qu'elle établit.

</div>

<div class="texte" data-id="8.4312">

<span>8.4312 &mdash; </span>Le matérialisme est pour la raison une manière de ne jamais se satisfaire d'elle-même.

</div>

<div class="texte" data-id="8.432">

<span>8.432 &mdash; </span>Le contre-courant du contre-intuitif déconstruit l'opacité qui empêche la contingence des traversées du réel.

</div>

<div class="texte" data-id="8.4321">

<span>8.4321 &mdash; </span>Les traversées du réel ne préexistent pas au sujet, mais sont une forme de la contingence d'une *praxis* rationnelle de son espace ontologique.

</div>

<div class="texte" data-id="8.44">

<span>8.44 &mdash; </span>L'idée de contemplation émerge de celle d'intuition, mais la contemplation est une immobilité de l'entendement qui croit mettre une certaine gravité en celle-ci, gravité lui permettant de voir *dans*.

</div>

<div class="texte" data-id="8.441">

<span>8.441 &mdash; </span>Pour entendre *dans*, et non plus seulement voir *dans*, il convient que la raison sinue *dans* le réel, qu'elle s'y confronte et qu'elle s'y mêle, qu'elle y goûte et qu'elle y sente, qu'elle interroge tout ce que la vision ne lui dit pas, qu'elle y cherche la métamorphose.

</div>

<div class="texte" data-id="8.442">

<span>8.442 &mdash; </span>Toute contemplation demeure une autosatisfaction du sujet qui croit pouvoir par un déploiement atmosphérique de ses sens rejoindre le *sens*, tout en demeurant dans une immobilité de ceux-ci.

</div>

<div class="texte" data-id="8.4421">

<span>8.4421 &mdash; </span>Le sujet, par la contemplation, tente vainement de faire *sens*, mais il ne fait qu'établir une compréhension à partir de ce qu'il préjuge, du fait d'un déterminisme social qui le structure dans et par ses sens.

</div>

<div class="texte" data-id="8.4422">

<span>8.4422 &mdash; </span>Toute contemplation demeure une production sociale qui s'inscrit dans la matrice des déterminismes prédisposant le sujet contemplatif.

</div>

<div class="texte" data-id="8.443">

<span>8.443 &mdash; </span>L'intuition peut devenir un outil d'exploration si elle se débarrasse de la réjouissance de ce qu'elle découvre et qu'elle le replace dans le contexte d'une exploration appelant à un excès de la vision.

</div>

<div class="texte" data-id="8.4431">

<span>8.4431 &mdash; </span>Il faut sentir derrière l'immédiateté de la sensation : voilà le seul point de départ pour se départir de l'intuition entendue comme finalité.

</div>

<div class="texte" data-id="8.4432">

<span>8.4432 &mdash; </span>Il faut avoir *des yeux derrière les yeux*, savoir les retourner vers la blancheur des intériorités pour les guider sous le derme de nos réalités, leur donner en offrande l'entrevue de ce qui est commun : la substance changeante du réel.

</div>

<div class="texte" data-id="8.444">

<span>8.444 &mdash; </span>Sentir doit se muer en un phénomène d'exploration dépassant ce qui s'établit dans le savoir, interrogeant ce qui *est* derrière les évidences. S'impose une traversée des marécages idéels qui amoindrissent la raison voulant l'extension de ses capacités à *sentir*.

</div>

<div class="texte" data-id="8.4441">

<span>8.4441 &mdash; </span>Sentir doit être une circulation entre les mondes. Sentir doit refaire continuellement sa circulation entre les mondes.

</div>

<div class="texte" data-id="8.4442">

<span>8.4442 &mdash; </span>Sentir doit être une sensualité des profondeurs, une *praxis* collective des échos de la matière.

</div>

<div class="texte" data-id="8.4443">

<span>8.4443 &mdash; </span>Sentir doit sonder les atomes, tenter la pénétration subatomique, y prospecter les abysses, à la lisière desquels la raison découvre le périmètre de son savoir. Cette découverte est une invitation à l'extension de celui-ci.

</div>

<div class="texte" data-id="8.445">

<span>8.445 &mdash; </span>Le sens des choses est une dynamique qui ne peut être fixée par les sens. Le sens des choses doit être poursuivi par les sens en symbiose avec sa dynamique propre.

</div>

<div class="texte" data-id="8.45">

<span>8.45 &mdash; </span>La raison matérialiste doit être au fait de la relativité sociale de l'intuition, elle ne peut donc pas se contenter de l'immobilité de l'intuition, mais doit s'en servir comme une simple piste pour ses explorations.

</div>

<div class="texte" data-id="8.451">

<span>8.451 &mdash; </span>Le contentement intuitif crée de fait une immobilité de la compréhension, qui stagne autour de ce que présuppose la confrontation, et n'est plus en capacité de questionner l'altérité de la confrontation.

</div>

<div class="texte" data-id="8.452">

<span>8.452 &mdash; </span>Le matérialisme se contentant de sa seule intuition correspond à un matérialisme des contempteurs de la dialectique des mondes, de ces sphères subjectives et cloisonnantes qui donnent l'illusion au sujet, qui en est son centre, que le réel se borne au rayon de ce qu'il peut.

</div>

<div class="texte" data-id="8.453">

<span>8.453 &mdash; </span>L'intuition comme absolu est la certitude d'un accès à la vérité pour l'individu esseulé dans son monde.

</div>

<div class="texte" data-id="8.4531">

<span>8.4531 &mdash; </span>L'entendement du réel est limité par le contentement de ses limites.

</div>

<div class="texte" data-id="8.4532">

<span>8.4532 &mdash; </span>L'entrechoc des entendements limités du réel, qui s'entraînent les uns les autres vers une finalité de leur propre sensation, fabrique l'illusion d'un accès *véritable* au réel, sous ses surfaces, alors que triomphe une suffisance de la sensation, circonscrivant les possibles exploratoires de sa continuité.

</div>

<div class="texte" data-id="8.4533">

<span>8.4533 &mdash; </span>Le contentement intuitif, l'intuition prise comme fin *en soi*, est une perspective spéculaire de l'individu sur lui-même, qui croit trouver dans son miroitement une *réalité vraie* du réel, la réalité étant toujours cette subjectivité s'accaparant le réel par une découpe suivant la forme de son entendement, et la vérité demeurant l'imposition de cette découpe à tout ce qui est autre.

</div>

<div class="texte" data-id="8.4534">

<span>8.4534 &mdash; </span>Considérer l'individu comme un axiome social est une invention moderne qui permet de nier la perception de la structure moléculaire que sa singularité intuitive compose par l'agglomération à d'autres singularités intuitives, singularités étant à leur tour, dans leur subjectivité propre, exploratrices du réel et, de ce fait, constructrices d'une dynamique rationnelle autre. Cette considération *excentrique* peut aller au non-humain, au non-animal, voire au non-terrestre.

</div>

<div class="texte" data-id="8.454">

<span>8.454 &mdash; </span>La perception de la mécanique transformant l'évidence de la représentation en objectivité ontologique s'expose seule par une autocritique de l'intuition et de son immobilité dans l'immédiateté des choses.

</div>

<div class="texte" data-id="8.455">

<span>8.455 &mdash; </span>Toute intuition devrait être une invitation à la traversée rationnelle du réel, entraînant l'extension de l'espace ontologique du sujet.

</div>

<div class="texte" data-id="8.456">

<span>8.456 &mdash; </span>La sensation doit toujours chercher, dans un dépassement de ses intuitions, à *être* plus qu'elle-même.

</div>

<div class="texte" data-id="8.5">

<span>8.5 &mdash; </span>Le matérialisme doit devenir un matérialisme de l'infinitude s'il souhaite se rapprocher du *mode d'être* de la matière, c'est-à-dire de sa contingence ontologique.

</div>

<div class="texte" data-id="8.51">

<span>8.51 &mdash; </span>Le matérialisme de l'infinitude conçoit le réel comme une puissance ontologique de la matière, où sa substance est toujours une mutation de sa substance.

</div>

<div class="texte" data-id="8.511">

<span>8.511 &mdash; </span>La finitude de toute chose se subsume à l'infinitude des interactions de toute chose avec toute chose.

</div>

<div class="texte" data-id="8.512">

<span>8.512 &mdash; </span>Toute finitude de la matière s'inscrit dans une infinitude de son *devenir*.

</div>

<div class="texte" data-id="8.5121">

<span>8.5121 &mdash; </span>La matière *devient* par la matière, dans la matière.

</div>

<div class="texte" data-id="8.513">

<span>8.513 &mdash; </span>Tout est *devenir* dans la matière, mais tout est aussi *devenir* de la matière.

</div>

<div class="texte" data-id="8.5131">

<span>8.5131 &mdash; </span>Le vide est un *devenir* de la matière par la puissance de son *non-être*.

</div>

<div class="texte" data-id="8.5132">

<span>8.5132 &mdash; </span>Le vide dispose un espace du *non-être* de la matière.

</div>

<div class="texte" data-id="8.5133">

<span>8.5133 &mdash; </span>Le *non-être* est, dans le vide qu'il densifie par sa puissance à *être*.

</div>

<div class="texte" data-id="8.514">

<span>8.514 &mdash; </span>Le matérialisme de l'infinitude fait du réel un champ d'exploration ontologique pour une dynamique de la raison, qui ne cesse de sonder les profondeurs du réel et leurs constantes prolongations.

</div>

<div class="texte" data-id="8.5141">

<span>8.5141 &mdash; </span>Plus la raison approfondit le réel, plus le réel *devient* profond.

</div>

<div class="texte" data-id="8.5142">

<span>8.5142 &mdash; </span>La raison qui anime le matérialisme de l'infinitude est à la recherche des échos communs à toutes les choses, composant l'*être* commun de toutes les choses : le *devenir*.

</div>

<div class="texte" data-id="8.5143">

<span>8.5143 &mdash; </span>L'*intelligence* de la raison matérialiste est la méthode de tenir ensemble les éléments qui se présentent à son discernement. Elle y sonde les interstices de leur assemblage au travers d'un accroissement contingent des capacités de cette méthode.

</div>

<div class="texte" data-id="8.52">

<span>8.52 &mdash; </span>Le réel perdure en son infinitude.

</div>

<div class="texte" data-id="8.521">

<span>8.521 &mdash; </span>L'obscurité a la consistance des accidents qui *peuvent* la mouvoir.

</div>

<div class="texte" data-id="8.522">

<span>8.522 &mdash; </span>Les contempteurs de l'infinitude du réel refusent la négation de ce qu'ils sont par ce qui leur échappe. Ils refusent la possibilité d'une profondeur inaccessible.

</div>

<div class="texte" data-id="8.523">

<span>8.523 &mdash; </span>Le refus de l'infinitude du réel correspond à un refus de ses extensions potentielles.

</div>

<div class="texte" data-id="8.524">

<span>8.524 &mdash; </span>Dans le cadre de la modernité, le refus de l'infinitude du réel donne à l'intuition une dimension politique, où elle devient sa propre fin, en tant que signe d'une propension individuelle et unique à une dite réussite sociale : l'élévation par l'*avoir* dans la hiérarchie sociale.

</div>

<div class="texte" data-id="8.5241">

<span>8.5241 &mdash; </span>L'unicité de la propension individuelle à réussir socialement constitue un moteur de la discrimination entre individus, qui se présente comme la nécessité d'établir par la seule *sensation* individuelle une réussite matérielle au détriment de l'autre : l'individu moderne doit avoir le *sens* des affaires.

</div>

<div class="texte" data-id="8.5242">

<span>8.5242 &mdash; </span>L'idéologie bourgeoise, animée par la discrimination égoïste, se sert du matérialisme intuitif pour qualifier ses constructions économiques de *lois naturelles*.

</div>

<div class="texte" data-id="8.5243">

<span>8.5243 &mdash; </span>L'intuition bourgeoise est l'intuition économique à partir d'une singularité qui se confronte à d'autres singularités, sans jamais considérer la fusion rationnelle pouvant naître d'un agglomérat perceptif de sujets formant un sujet pluriel, et se situant dans une *antinature* de l'économie.

</div>

<div class="texte" data-id="8.53">

<span>8.53 &mdash; </span>L'affirmation de l'infinitude du réel dessine une tension vers l'inexploré, ou une insatisfaction de l'exploré, à partir de laquelle peut se façonner une politique collective du mouvement exploratoire de l'*être* vers un *toujours plus* de l'*être*.

</div>

<div class="texte" data-id="8.531">

<span>8.531 &mdash; </span>Dans une perspective réticulaire du sujet apparaît une construction subjective à partir des confrontations des subjectivités intuitives en œuvre. Cette dialectique permanente des subjectivités se nourrit de ce qui reste d'inconnu après l'addition des intuitions singulières pour former une politique de l'extension de l'*être*.

</div>

<div class="texte" data-id="8.5311">

<span>8.5311 &mdash; </span>La rencontre des intuitions diverses conduit à leur épuisement, puis à leur métamorphose.

</div>

<div class="texte" data-id="8.532">

<span>8.532 &mdash; </span>Par ses intuitions, le *sujet réticulaire* tente de se devancer lui-même et de transcrire cette conquête extensive de l'*être*, qui va de soi à soi, et de soi à l'infinitude du réel.

</div>

<div class="texte" data-id="8.533">

<span>8.533 &mdash; </span>Il n'existe pas de contentement de l'intuition au stade collectif du *sujet réticulaire*, mais seulement au stade individuel de l'intérêt égoïste.

</div>

<div class="texte" data-id="8.534">

<span>8.534 &mdash; </span>La mise en commun des sensations de l'infinitude du réel conduit à une distribution rationnelle de l'entendement qui en découle. Son partage collectif arrange la saine et permanente agitation d'une raison dirigée vers un *toujours plus* ontologique de ses explorations.

</div>

<div class="texte" data-id="8.535">

<span>8.535 &mdash; </span>Le matérialisme de l'infinitude présente une idée pour *faire* politique, celle d'un communisme réticulaire, acentré, cosmique, tendu vers une conquête ontologique sensible de l'espace où le sujet n'*est* pas encore.

</div>

<div class="texte" data-id="9.">

<span>9. &mdash; </span>Le sacré est une interrogation qui mène la vie humaine au-devant d'elle-même, dans les modulations mythiques de son récit s'adaptant à la dialectique du sujet et de son environnement.

</div>

<div class="texte" data-id="9.1">

<span>9.1 &mdash; </span>Le sacré est l'aura qui nimbe ce que le sujet ne peut pas résoudre de sa *présence au monde*.

</div>

<div class="texte" data-id="9.11">

<span>9.11 &mdash; </span>Le sacré est l'opacité qui à la fois sépare le sujet d'une connaissance entière du réel et stimule le sujet à désirer une réunion encore plus entière avec le réel.

</div>

<div class="texte" data-id="9.111">

<span>9.111 &mdash; </span>La raison du sujet va creuser l'opacité qui l'entoure pour entendre l'écho de ce qu'elle ne peut savoir d'elle-même, et entendre en cela les puissances de son *devenir*.

</div>

<div class="texte" data-id="9.1111">

<span>9.1111 &mdash; </span>Ce sont les puissances du *devenir* de la raison qui peuvent transformer sa *présence au monde*, et ainsi son *mode d'être*.

</div>

<div class="texte" data-id="9.1112">

<span>9.1112 &mdash; </span>Le mystère est la contingence de ce *devenir* dans l'opacité qui entoure la raison du sujet.

</div>

<div class="texte" data-id="9.112">

<span>9.112 &mdash; </span>Le sacré s'arrange dans un amoncellement dialectique de ce qu'*est* la raison et de l'impossible actualisation de ce qu'elle *peut* être dans sa *présence au monde*.

</div>

<div class="texte" data-id="9.12">

<span>9.12 &mdash; </span>Le sacré répond à une absence de réponse par l'absence de réponse.

</div>

<div class="texte" data-id="9.121">

<span>9.121 &mdash; </span>C'est autour des remous interrogatifs qui excitent la raison que s'invente le besoin du sacré pour que se conserve une certaine dynamique transformatrice de la vie.

</div>

<div class="texte" data-id="9.122">

<span>9.122 &mdash; </span>La stupeur interrogative doit se faire tension interrogative.

</div>

<div class="texte" data-id="9.123">

<span>9.123 &mdash; </span>Le sacré fait en sorte qu'aucune réponse ne soit possible, mais que la dynamique résultant du questionnement demeure la vitalité qui pousse la substance à sa transsubstantiation.

</div>

<div class="texte" data-id="9.13">

<span>9.13 &mdash; </span>À partir du besoin vital du sacré, le visible va chercher ce qu'il y a de *soi* dans l'invisible.

</div>

<div class="texte" data-id="9.131">

<span>9.131 &mdash; </span>Le sacré est un besoin vital parce qu'il offre à la vie de *pré-sentir* son *être* sans pouvoir y accéder.

</div>

<div class="texte" data-id="9.132">

<span>9.132 &mdash; </span>C'est la *pré-sensation* de l'*être* qui aiguille à un accroissement de sa quête sensible.

</div>

<div class="texte" data-id="9.2">

<span>9.2 &mdash; </span>Le besoin du sacré produit le mythe en tant que récit qui tente d'articuler la position interrogative de la raison face à sa réalité.

</div>

<div class="texte" data-id="9.21">

<span>9.21 &mdash; </span>La raison n'essaie en rien d'apporter une quelconque réponse face à ce qu'elle n'entend pas de sa réalité, mais façonne un récit autour de cette absence de réponse, récit par lequel la question ontologique pourra produire son cadre social.

</div>

<div class="texte" data-id="9.211">

<span>9.211 &mdash; </span>Le mythe se sert de l'allégorie pour former une construction sociale et impermanente du questionnement autour de la nécessité physique et permanente du *devenir*.

</div>

<div class="texte" data-id="9.2111">

<span>9.2111 &mdash; </span>L'impermanence du mythe s'explique par une nécessité d'adaptation du récit aux transformations de la raison et de son environnement, et de ce que ces transformations engendrent dans le social.

</div>

<div class="texte" data-id="9.2112">

<span>9.2112 &mdash; </span>La caractéristique évolutive du mythe demeurera une plasticité du récit formée autour d'un besoin du sacré, qui demeure quant à lui une permanence de la raison faisant face à ses limites.

</div>

<div class="texte" data-id="9.212">

<span>9.212 &mdash; </span>La concrétion mythique du sens engendre un foisonnement de récits dont l'ordonnance révèle l'invisible des structurations du social.

</div>

<div class="texte" data-id="9.2121">

<span>9.2121 &mdash; </span>La latence de l'organisation mythique d'une société satisfait un besoin sourd de l'intuition groupale, celui de trouver l'ordre répondant le mieux aux confrontations de la raison à l'ampleur mystérieuse du réel.

</div>

<div class="texte" data-id="9.2122">

<span>9.2122 &mdash; </span>L'intuition mythique s'hérite d'âge en âge, se module à chaque âge, détermine l'âge suivant tout en lui laissant la tâche de travestir l'héritage, de s'en écarter, d'oublier les liens qui se tissent, dans les mythes et par les mythes, entre les sociétés depuis que l'humain a découvert la potentialité de sa raison.

</div>

<div class="texte" data-id="9.2123">

<span>9.2123 &mdash; </span>La latence de l'organisation mythique, qui suinte invariablement dans le quotidien de chaque humain, permet de faire face à la persistance du questionnement pour que la vie sociale puisse continuer sa reproduction, sans la pesanteur d'une conscience permanente du mystère.

</div>

<div class="texte" data-id="9.213">

<span>9.213 &mdash; </span>L'inconscience du mystère porte le geste vivant qui tente de provoquer le geste vivant suivant.

</div>

<div class="texte" data-id="9.22">

<span>9.22 &mdash; </span>Aussi démythifiée soit une société, elle ne pourra pas dépasser le besoin du sacré qui remue la raison face à ses ignorances, et même si cette raison, au travers de sa technicité, doit devenir le mythe en tant que reliquat de la démythification sociale.

</div>

<div class="texte" data-id="9.221">

<span>9.221 &mdash; </span>Les sociétés démythifiées se situent toujours dans une démesure de la quête ou du rejet du sens, puisqu'il ne demeure qu'un support parcellaire et inconscient à leur besoin du sacré.

</div>

<div class="texte" data-id="9.222">

<span>9.222 &mdash; </span>La société moderne est une société démythifiée, qui a placé ce qui lui reste de mythe dans un rapport technique à l'*avoir*.

</div>

<div class="texte" data-id="9.2221">

<span>9.2221 &mdash; </span>La société moderne croit en sa domination rationnelle sur sa réalité, mais ne perçoit pas le caractère mythique tronquée qu'elle place en cette domination, sans toutefois lui accorder une conscience qui pourrait permettre un retour révolutionnaire à l'*être*.

</div>

<div class="texte" data-id="9.2222">

<span>9.2222 &mdash; </span>L'inconscience du mythe moderne de la raison, reliquat d'une démythification de la société moderne imposée par une économie libérale centrée sur l'individu, fonde une ontologie de l'*avoir* qui explique la démesure du rapport de la société moderne à son environnement.

</div>

<div class="texte" data-id="9.223">

<span>9.223 &mdash; </span>La société moderne a égaré l'importance du mythe dans une distension des liens qui l'unissent aux mystères environnants de sa raison.

</div>

<div class="texte" data-id="9.2231">

<span>9.2231 &mdash; </span>La raison moderne n'est plus une raison collective, mais une explication du comportement individuel d'un agent économique dans le cadre moderne du marché.

</div>

<div class="texte" data-id="9.2232">

<span>9.2232 &mdash; </span>Le marché représente la réalité entendue comme totalité spatiale de la raison moderne, c'est-à-dire la raison économique individuelle. Le marché doit alors pouvoir se saisir de la totalité de l'espace que la raison peut appréhender.

</div>

<div class="texte" data-id="9.2233">

<span>9.2233 &mdash; </span>La caractéristique totalitaire du marché s'explique par l'inconscience du mythe parcellaire moderne qui ampute la raison de la possible construction d'un mythe au-delà de la préhension de son environnement.

</div>

<div class="texte" data-id="9.224">

<span>9.224 &mdash; </span>L'émiettement du récit moderne, malgré un besoin du sacré qui demeure invariable même s'il reste inconscient, justifie le rapport destructeur de la raison à son environnement, puisqu'elle n'y place plus rien de commun à ce qu'elle *est*.

</div>

<div class="texte" data-id="9.2241">

<span>9.2241 &mdash; </span>Le retour d'un phénomène religieux vide au sein de la modernité, vide car subissant la démythification moderne de l'*avoir*, ne peut advenir qu'à travers la violence consubstantielle de l'ontologie moderne de l'*avoir*. Ce retour se fera par l'imposition de la violence de l'*avoir* dans le social.

</div>

<div class="texte" data-id="9.2242">

<span>9.2242 &mdash; </span>Les violences religieuses modernes tentent de forcer autrui à partager une croyance identique en des réponses évidées de leur consistance dogmatique, qui ne se trouvent être plus que le simulacre de leur autorité ancienne, afin de pallier la démesure d'une ontologie de l'*avoir*, mais sans avoir conscience que tout phénomène religieux moderne est invariablement une production sociale dont chaque caractéristique sera déterminée par les mécaniques de l'*avoir*.

</div>

<div class="texte" data-id="9.23">

<span>9.23 &mdash; </span>La concrétion anarchiste du savoir répond au besoin du sacré par la nécessité du constant débordement du savoir de ce qu'il *est*.

</div>

<div class="texte" data-id="9.231">

<span>9.231 &mdash; </span>La concrétion anarchiste du savoir, dans laquelle le savoir refuse tout *régime de savoir*, ne se contente pas d'*être*, mais tend vers ce qu'il n'*est* pas et trouve sa force motrice dans un rapport dialectique au mystère.

</div>

<div class="texte" data-id="9.232">

<span>9.232 &mdash; </span>Le savoir est mû négativement par ce qu'il n'*est* pas. C'est l'ignorance qui pousse le savoir vers sa métamorphose, et cette tension qui va de la connaissance au mystère de la connaissance se justifie par le besoin du sacré.

</div>

<div class="texte" data-id="9.2321">

<span>9.2321 &mdash; </span>Le sacré demeure tout ce qui *est* dans le *non-être* du savoir.

</div>

<div class="texte" data-id="9.2322">

<span>9.2322 &mdash; </span>Le sacré est l'espace inexploré du savoir parce qu'il est l'espace que ne peut pas envisager le savoir. C'est la zone qui est au-delà de la tension entre la connaissance et le mystère de la connaissance. Cette zone se situe dans le mystère qui vient.

</div>

<div class="texte" data-id="9.233">

<span>9.233 &mdash; </span>Suivant le principe anarchiste de la concrétion du savoir, la concrétion mythique du savoir déguise un fonctionnalisme social en fable des origines, afin de chasser l'angoisse nihiliste d'un savoir sans destination, qui mène la vie à sa propre extinction, et de poursuivre une volonté sourde de transsubstantiation, qui attise en la raison ce mouvement de l'engendrement du dépassement.

</div>

<div class="texte" data-id="9.2331">

<span>9.2331 &mdash; </span>La question, qui va de la vie à la vie en passant par le crible invisible de l'ubiquité physique, est une question qui n'appelle pas à une réponse, mais se façonne dans la raison qu'elle agite, tel un moteur imposant la nécessité de s'adapter et de se transformer.

</div>

<div class="texte" data-id="9.2332">

<span>9.2332 &mdash; </span>La question demeure sans réponse, et l'absence de réponse est la nécessité du *devenir*.

</div>

<div class="texte" data-id="9.24">

<span>9.24 &mdash; </span>Ce n'est pas le mythe qui fabrique la pratique sociale, mais la pratique sociale qui fabrique le mythe à partir du besoin du sacré.

</div>

<div class="texte" data-id="9.241">

<span>9.241 &mdash; </span>Le mythe doit être considéré comme une fondation idéologique de la pratique sociale, mais pas en tant que pensée originelle fondant le social, ce qui reviendrait à véhiculer indirectement une idéologie des origines, mais comme le besoin du sacré de créer un récit de la causalité du présent s'adaptant aux conditions d'existence en ce présent.

</div>

<div class="texte" data-id="9.2411">

<span>9.2411 &mdash; </span>Le mythe simule les stabilités de l'*arkhè*, tout en adaptant son contenu à son époque, pour construire les liens qui articulent la vie commune à un groupe.

</div>

<div class="texte" data-id="9.242">

<span>9.242 &mdash; </span>La *praxis* qui meut une société agglomère des pratiques sociales dont les acteurs se révèlent moins importants que les pratiques elles-mêmes et leur circulation.

</div>

<div class="texte" data-id="9.2421">

<span>9.2421 &mdash; </span>La considération d'une certaine autonomie des pratiques sociales par rapport à leurs acteurs accorde à ces pratiques une propriété créatrice du lien social.

</div>

<div class="texte" data-id="9.2422">

<span>9.2422 &mdash; </span>Ce n'est pas l'acteur social qui constitue la société, mais les pratiques sociales et les interactions entre elles.

</div>

<div class="texte" data-id="9.2423">

<span>9.2423 &mdash; </span>La pratique sociale fait le lien social et, par conséquent, fait l'acteur social. C'est la pratique sociale qui constitue son acteur, et non l'inverse.

</div>

<div class="texte" data-id="9.2424">

<span>9.2424 &mdash; </span>La pratique sociale compose la société, la module, et en ce sens, il serait possible d'évoquer une vision cybernétique de la société où les pratiques sociales s'agglomèrent dans une confrontation pratique à leur environnement immédiat.

</div>

<div class="texte" data-id="9.243">

<span>9.243 &mdash; </span>L'agglomération des pratiques sociales fonde la concrétion mythique du savoir et produit une intelligence de l'allégorie qui tente de catalyser ces pratiques structurantes.

</div>

<div class="texte" data-id="9.2431">

<span>9.2431 &mdash; </span>Le corollaire religieux de la concrétion mythique du savoir apparaît indirectement comme une dérive, celle du besoin de figer le récit afin de servir les intérêts de certaines classes sociales contre d'autres classes.

</div>

<div class="texte" data-id="9.2432">

<span>9.2432 &mdash; </span>La volonté de figer le récit mythique d'une société se rapporte à une volonté de domination sur le social. Elle est le plus souvent celle de la classe sociale dominant la société, mais elle peut aussi être celle d'une classe sociale dominée voulant renverser cette hiérarchie du pouvoir et devenir à son tour une classe dominant d'autres classes sociales.

</div>

<div class="texte" data-id="9.3">

<span>9.3 &mdash; </span>Le phénomène religieux est un phénomène uniquement politique, qui n'a de liens que distants avec le sacré, par le figement idéologique du mythe qu'il provoque.

</div>

<div class="texte" data-id="9.31">

<span>9.31 &mdash; </span>La dérive du religieux se fait par l'imposition d'une hiérarchie au cœur de la dialectique entre le sujet et son environnement.

</div>

<div class="texte" data-id="9.311">

<span>9.311 &mdash; </span>Le religieux est la négation du sacré.

</div>

<div class="texte" data-id="9.3111">

<span>9.3111 &mdash; </span>Le sacré qui se situe à même la matière, telle la conséquence d'une matière qui dialectise avec elle-même, est perverti par l'impérieux besoin de domination de la classe souhaitant imposer le phénomène religieux.

</div>

<div class="texte" data-id="9.3112">

<span>9.3112 &mdash; </span>La présence du sacré en toutes choses dessert le besoin de domination sociale et doit être purgé, avec autorité et contrainte, par une dite élévation du questionnement autour de *la chose* vers un questionnement autour de *l'essence* de la chose.

</div>

<div class="texte" data-id="9.3113">

<span>9.3113 &mdash; </span>L'abstraction sacrée du phénomène religieux évide la puissance du sacré distribuée en chaque chose, en chaque vie, disponible par chaque geste et pour chaque *étant* qui veut s'en saisir. Le phénomène religieux, pour perdurer dans sa dimension politique, se doit d'empêcher tout accès, par ce qu'il représente, à un mystère l'outrepassant.

</div>

<div class="texte" data-id="9.3114">

<span>9.3114 &mdash; </span>Le simple fait, pour le phénomène religieux, de vouloir apporter des réponses strictes, sans laisser aucune place à un rapport malléable et collectif de la raison au mystère, démontre que tout phénomène religieux sera toujours une restriction du vivant dans l'*être*.

</div>

<div class="texte" data-id="9.312">

<span>9.312 &mdash; </span>La négation est *en soi* le fondement de la production du phénomène religieux, qui n'est autre qu'un corpus d'interprétations restrictives du réel que nous pouvons qualifier de *religion*, ou de *chose religieuse* afin d'y percevoir la production sociale normative qu'elle constitue.

</div>

<div class="texte" data-id="9.3121">

<span>9.3121 &mdash; </span>Rapporter la religion à cette idée matérielle de *chose*, de *chose* religieuse, permet d'extraire l'éther asphyxiant d'une immanence imposée à la vie que la religion veut assujettir. Dans le terme de *chose*, la religion s'entend précisément comme la mécanique matérielle issue des structures géographiques, biologiques et historiques qui conditionnent une société, mécanique n'ayant pour but que de marquer une influence directrice sur ces structures génératrices.

</div>

<div class="texte" data-id="9.3122">

<span>9.3122 &mdash; </span>Le phénomène religieux se calque sur le passage du mouvement vital au travers de la matière pour refuser à la question qui sonde le sens du *devenir* métamorphique de la matière son autonomie.

</div>

<div class="texte" data-id="9.3123">

<span>9.3123 &mdash; </span>Dans l'affirmation de sa négation, la chose religieuse fige le réel en des essences masquées par une matière obstructrice, où chaque acte ne transforme pas ce à quoi il se confronte, mais *capitalise* sa valeur de confrontation en rapport à une cause finale de la matière.

</div>

<div class="texte" data-id="9.3124">

<span>9.3124 &mdash; </span>La transformation permanente de la matière, mettant en cause le rapport unidirectionnel entre la cause et l'effet, est ignorée pour permettre un figement idéologique de la valeur de la cause déterminant le poids de son effet sur le sujet soumis à la chose religieuse.

</div>

<div class="texte" data-id="9.3125">

<span>9.3125 &mdash; </span>La chose religieuse peut prendre diverses formes, y compris celle d'un athéisme d'État, tant qu'elle fige un récit structurant le social et servant les intérêts d'une classe dominante.

</div>

<div class="texte" data-id="9.313">

<span>9.313 &mdash; </span>La chose religieuse doit figer le récit qui la soutient afin d'asseoir sa normativité.

</div>

<div class="texte" data-id="9.3131">

<span>9.3131 &mdash; </span>La chose religieuse place dans la somme d'instants qui compose la vie le contrôle et l'accusation.

</div>

<div class="texte" data-id="9.3132">

<span>9.3132 &mdash; </span>Le bâton du berger est le berger, comme la damnation est la chose religieuse : un outil de direction du consentement créateur de la fonction sociale.

</div>

<div class="texte" data-id="9.3133">

<span>9.3133 &mdash; </span>La chose religieuse nie le principe de la physique qui fait du réel une suite de combinaisons et de séparations, une perpétuelle transformation de celles-ci.

</div>

<div class="texte" data-id="9.3134">

<span>9.3134 &mdash; </span>L'immuabilité de la chose religieuse veut une éternité de la fin, et par là une éternité du jugement de la fin, empêchant toute contingence de la transformation matérielle.

</div>

<div class="texte" data-id="9.3135">

<span>9.3135 &mdash; </span>S'obstrue dans la chose religieuse l'idée d'un mouvement du *devenir*, celle des fluctuations de l'*être*, et ce jusque dans le récit.

</div>

<div class="texte" data-id="9.3136">

<span>9.3136 &mdash; </span>Toute idéologie religieuse rejette toute expression de *tout se transforme* pour mieux affirmer toute expression de *tout se fige*.

</div>

<div class="texte" data-id="9.3137">

<span>9.3137 &mdash; </span>La pensée du *devenir* dans la chose religieuse, par exemple au travers d'un cycle de réincarnations, demeure une fixité de l'*être*, puisqu'une pensée du *devenir* s'y établit par une immuabilité du récit, dont l'illusion du mouvement sert une stabilité de l'emprise politique éloignée de toute idée radicale de démocratie. Cette pensée du *devenir* ne peut souffrir les variations du savoir conséquentes de son évolution dans et par la matière.

</div>

<div class="texte" data-id="9.314">

<span>9.314 &mdash; </span>L'emprise sur le sujet de la chose religieuse requiert une ontologie dualiste, dans le sens d'une distinction entre une essence de la matière et sa possible transcendance, et non pas seulement dans la seule vision occidentale d'une séparation du corps et de l'esprit.

</div>

<div class="texte" data-id="9.3141">

<span>9.3141 &mdash; </span>L'ontologie dualiste nécessaire à la chose religieuse se trouve être la seule structure permettant d'enclore la vie dans une menace de ce qui se situerait *au-delà* d'elle, tandis que les formes d'un monisme matérialiste laissent entendre une circulation libre de la vie dans la matière, en rejetant l'idée d'une situation finaliste *après la vie*, *parallèle à la vie*, *juxtaposée à la vie*, puisque la situation de la vie demeure un continuum qui traverse le réel, mute par cette traversée, et y nie toute immanence.

</div>

<div class="texte" data-id="9.3142">

<span>9.3142 &mdash; </span>Pour la chose religieuse, l'*au-delà* de la vie est une situation possible, mais qui demeure incluse dans l'*au-dedans* de son récit.

</div>

<div class="texte" data-id="9.3143">

<span>9.3143 &mdash; </span>L'*au-delà* du récit est une situation impossible pour la chose religieuse, parce qu'elle permettrait non pas un *au-delà* de la vie, mais une vie *au-delà* du récit.

</div>

<div class="texte" data-id="9.32">

<span>9.32 &mdash; </span>La chose religieuse se présente comme une forme politique de contrainte, hiérarchisant le social pour renforcer les structures de domination, et ceci grâce aux menaces qui découlent d'une opposition entre l'essence de la matière et la promesse de sa libération.

</div>

<div class="texte" data-id="9.321">

<span>9.321 &mdash; </span>La chose religieuse, par une injonction de la peur politique en l'instabilité de ce qui *est* dans l'*être*, fige l'idée de passage en celle de fin, qui deviendrait l'écho de la somme des actes ponctuant la dialectique entre toute vie et sa réalité, et dont le jugement permettrait d'établir une conformité sociale, ou son absence, à l'idéologie productrice de la chose religieuse.

</div>

<div class="texte" data-id="9.3211">

<span>9.3211 &mdash; </span>La fin, à l'inverse du passage, cherche à stigmatiser la vie comme un lieu de fautes, fautes dont l'agencement compose négativement une morale en tant que cloison politique imposée par la classe productrice de la chose religieuse.

</div>

<div class="texte" data-id="9.3212">

<span>9.3212 &mdash; </span>La morale, production invariable de la chose religieuse, est la fixation d'un récit normatif par le jugement politique de l'acte qui s'y conforme ou qui y diverge.

</div>

<div class="texte" data-id="9.3213">

<span>9.3213 &mdash; </span>La morale est un phénomène politique qui nécessite une certaine organisation structurelle afin de mieux enserrer la société qu'elle tente de contrôler, et cette nécessité ne peut supporter une fluidité transformatrice de la valeur.

</div>

<div class="texte" data-id="9.32131">

<span>9.32131 &mdash; </span>Une valeur s'inscrit, malgré les présupposés de la morale, dans une transformation fluide d'elle-même, puisque la valeur ne peut que suivre la valeur de la physique et que cette valeur demeure un mouvement perpétuel.

</div>

<div class="texte" data-id="9.32132">

<span>9.32132 &mdash; </span>La valeur cosmique est une continuelle transformation de la valeur qui la détermine.

</div>

<div class="texte" data-id="9.3214">

<span>9.3214 &mdash; </span>Toute théologie est une téléologie politique, et toute fin une négation de la malléabilité de l'instant.

</div>

<div class="texte" data-id="9.3215">

<span>9.3215 &mdash; </span>Considérer la somme des actes qui ponctuent la confrontation d'une vie à sa réalité n'a d'intérêt que dans l'établissement d'une morale permettant de juger le sujet à partir duquel cette somme s'établit.

</div>

<div class="texte" data-id="9.32151">

<span>9.32151 &mdash; </span>Le jugement d'un sujet à partir de ses actes passés est un refus de la malléabilité du présent, car il n'est qu'une potentialité coupable éclipsant l'instant de l'acte et pesant sur le futur du sujet. De telle manière, le présent du sujet demeure sous l'emprise du passé et son avenir n'est déterminé que par celui-ci.

</div>

<div class="texte" data-id="9.32152">

<span>9.32152 &mdash; </span>Dans le cadre de la morale, le passé est toujours sa lecture subjective à partir des intérêts de la classe productrice de la chose religieuse.

</div>

<div class="texte" data-id="9.32153">

<span>9.32153 &mdash; </span>Le refus de la malléabilité du présent permet d'imposer une représentation de celui-ci conforme aux intérêts de la classe productrice de la chose religieuse.

</div>

<div class="texte" data-id="9.3216">

<span>9.3216 &mdash; </span>S'architecture dans la chose religieuse une morale emprisonnant l'acte en ce qu'elle juge *bien*, c'est-à-dire favorable aux intérêts de classe l'animant.

</div>

<div class="texte" data-id="9.3217">

<span>9.3217 &mdash; </span>La somme des actes d'un sujet est inévitablement la somme de leur considération morale, c'est-à-dire politique, à l'aune de la menace que représente l'éternité de sa valeur cristallisée dans la mort.

</div>

<div class="texte" data-id="9.32171">

<span>9.32171 &mdash; </span>La chose religieuse est toujours une contrainte politique de la vie par la représentation de la mort.

</div>

<div class="texte" data-id="9.3218">

<span>9.3218 &mdash; </span>La dimension morale de la chose religieuse n'a pas un autre but que celui politique de l'asservissement.

</div>

<div class="texte" data-id="9.322">

<span>9.322 &mdash; </span>Les arguties religieuses ne servent qu'un nihilisme politique de la restriction du réel, et la chose religieuse n'a que faire du sacré, elle tente d'éloigner le plus possible de son centre politique les champs d'exploration qu'il ouvre.

</div>

<div class="texte" data-id="9.3221">

<span>9.3221 &mdash; </span>Nous pouvons reconnaître, au travers de l'histoire des religions, la cible politique que fut le matérialisme, et au-delà de cette menace d'une matière cherchant son autonomie, la quasi-omniprésence du dualisme dans les religions dominantes s'emparant du pouvoir politique ou le parasitant, leur versant moniste, même non matérialiste, étant cantonné à des formes mystiques secondaires écartées à dessein du pouvoir politique que doit servir le phénomène religieux.

</div>

<div class="texte" data-id="9.323">

<span>9.323 &mdash; </span>La valeur de la vie, par la force intrinsèque de transsubstantiation qui la meut, nie la cristallisation des fins.

</div>

<div class="texte" data-id="9.3231">

<span>9.3231 &mdash; </span>Retourner à la valeur de la vie conduit à abattre la morale et à destituer son pouvoir politique.

</div>

<div class="texte" data-id="9.3232">

<span>9.3232 &mdash; </span>C'est la valeur de la vie à laquelle se confrontent les interrogations du sacré, c'est bien cette confrontation destituante que la chose religieuse cherche par tout moyen à marginaliser, à contraindre, voire à détruire, afin de préserver sa puissance de domination vierge d'atteintes révélatrices de cette puissance.

</div>

<div class="texte" data-id="9.33">

<span>9.33 &mdash; </span>La chose religieuse demeure dirigée, dans un schéma tripartite des fonctions sociales, par une tension à la continuité de la domination politique sur la fonction productrice, ou au renversement de la domination par une domination nouvelle sur celle-ci, dans le cas où la chose religieuse émanerait d'une minorité.

</div>

<div class="texte" data-id="9.331">

<span>9.331 &mdash; </span>La chose religieuse ne dépend pas d'une vision globale de la société, mais d'une vision globale du groupe, dans lequel elle se crée, c'est-à-dire du groupe compris comme un globe refusant l'existence de ce qu'il n'englobe pas.

</div>

<div class="texte" data-id="9.3311">

<span>9.3311 &mdash; </span>Il est possible de découvrir au sein d'une minorité soumise à une classe dirigeante une chose religieuse divergeant de l'idéologie dominante, mais contenant également cette même force de contrôle au sein de son organisation sociale minoritaire.

</div>

<div class="texte" data-id="9.3312">

<span>9.3312 &mdash; </span>La chose religieuse demeure, dans la majorité des cas, le fait de la classe dirigeante, et la chose religieuse des minorités se trouve combattue, tout en demeurant animée par le souhait d'une domination du social et donc d'une inversion du rapport de force.

</div>

<div class="texte" data-id="9.332">

<span>9.332 &mdash; </span>La chose religieuse et la chose guerrière ne font qu'une dans leur velléité de domination de la fonction sociale de production.

</div>

<div class="texte" data-id="9.3321">

<span>9.3321 &mdash; </span>La chose religieuse n'existe que par sa dimension guerrière, et la chose guerrière que par sa dimension religieuse.

</div>

<div class="texte" data-id="9.3322">

<span>9.3322 &mdash; </span>La chose religieuse n'a plus de sens si elle ne souhaite plus imposer sur la société où elle évolue une quelconque domination. Elle se trouve alors purgée de sa dimension politique et retourne aux questionnements agitant le sacré. Toutefois, il est nécessaire de rappeler que le sacré est aussi, comme n'importe quel élément qui ponctue la vie au sein d'une société, une production sociale, mais dont la substance demeure mobile et au plus près du lien qui unit le vivant à la physique, dans l'absence de volonté politique d'y figer une compréhension de ce lien.

</div>

<div class="texte" data-id="9.333">

<span>9.333 &mdash; </span>Le sacré ne peut pas souffrir la fixation religieuse dans un phénomène
social stable sans distendre ce qui lie le vivant au réel et hybrider le besoin primordial
d'interroger sans cesse ce qui environne le vivant. La chose religieuse,
quant à elle, ne peut asseoir sa domination sans se servir des
questionnements issus de la quête du sacré pour apporter des réponses
englobant toute la vie et lui commandant une conception finie de
celle-ci. Nous constatons donc la tension inverse du sacré et du
religieux, et l'impossibilité axiomatique de leur coexistence,
l'existence de l'un menaçant celle de l'autre.

</div>

<div class="texte" data-id="9.3331">

<span>9.3331 &mdash; </span>La différence entre le sacré et le religieux se situe dans l'outil de domination que représente la chose religieuse, tandis que les questions motrices du sacré se retrouvent dans l'insoumission nécessaire à leur pleine mobilité.

</div>

<div class="texte" data-id="9.3332">

<span>9.3332 &mdash; </span>La chose religieuse, fonction des variations des structures sociales,
reste mue par la nécessité de régir le rapport entre le vivant et son environnement, au travers d'un
assujettissement s'opposant à toute possible autonomie
du sujet.

</div>

<div class="texte" data-id="9.34">

<span>9.34 &mdash; </span>La chose religieuse est une production sociale qui sert à imposer politiquement son récit par une tyrannie sur le corps. Tout en elle est tourné vers la contrainte du corps, et c'est seulement en contraignant le corps que la société peut être mise au pas.

</div>

<div class="texte" data-id="9.341">

<span>9.341 &mdash; </span>La mécanique politique de la chose religieuse souhaite un contrôle matériel du corps sur le corps, puisqu'il s'agit du support *organique* de la vie.

</div>

<div class="texte" data-id="9.3411">

<span>9.3411 &mdash; </span>La chose religieuse vise la vie afin de mieux détruire sa motilité, de l'empêcher d'atteindre cet état fluide de transsubstantiation.

</div>

<div class="texte" data-id="9.342">

<span>9.342 &mdash; </span>La chose religieuse n'a de sens que par la domination sur le corps qu'elle prescrit.

</div>

<div class="texte" data-id="9.3421">

<span>9.3421 &mdash; </span>La chose religieuse affirme son savoir comme une cloison qui s'abat sur le corps, par la négation de tout ce qui est envisageable au-delà de sa doctrine.

</div>

<div class="texte" data-id="9.3422">

<span>9.3422 &mdash; </span>Quelle que soit la chose religieuse ou la classe où elle se propage, la mécanique demeure celle de la fixation du sacré, dans une perspective politique de contrôle du corps.

</div>

<div class="texte" data-id="9.3423">

<span>9.3423 &mdash; </span>Envisager, pour une minorité, un renversement des hiérarchies sociales par la chose religieuse demeure une conservation des hiérarchies sociales.

</div>

<div class="texte" data-id="9.343">

<span>9.343 &mdash; </span>La mort comme terme du corps cristallise la question qui agite la chose religieuse.

</div>

<div class="texte" data-id="9.3431">

<span>9.3431 &mdash; </span>Pour la chose religieuse, pour cet instrument politique de classe, le corps n'a de sens que comme support de la souffrance du corps, afin d'y promettre soit son éternité coupable soit sa disparition béate. Cette dernière promesse demeure pourtant oublieuse de la possibilité d'une autre promesse, celle de la jouissance du corps en cette même éternité de la fin.

</div>

<div class="texte" data-id="9.344">

<span>9.344 &mdash; </span>L'injonction à la culpabilité doit empêcher le sujet d'avoir une pleine conscience de son action, et doit ainsi venir limiter ce qui compose l'esprit par un mélange inextricable : le corps.

</div>

<div class="texte" data-id="9.3441">

<span>9.3441 &mdash; </span>Au-delà du contrôle de l'esprit que la chose religieuse suggère, et contre les évidences de la plupart de ses variations doctrinales, elle sous-entend une inexistence de l'esprit, ou plutôt d'un esprit purement immatériel, délié du corps, pour instiller une volonté de lier l'esprit, en lui apposant tout autour de lui un champ matériel de fautes.

</div>

<div class="texte" data-id="9.3442">

<span>9.3442 &mdash; </span>Il importe peu d'un point de vue politique que l'esprit existe *au-delà* du corps, ce qui importe reste la seule promesse d'une libération de l'esprit du corps.

</div>

<div class="texte" data-id="9.3443">

<span>9.3443 &mdash; </span>La promesse faite à l'esprit du sujet par la chose religieuse est celle de son immatérialité paisible, mais cette promesse ne trouve de raison que dans l'illusion d'immatérialité que sème la matérialité pourtant concrète d'une politique restrictive.

</div>

<div class="texte" data-id="9.3444">

<span>9.3444 &mdash; </span>Les tenants de la chose religieuse ne croient pas en une quelconque immatérialité, ou alors ils se trompent eux-mêmes, par cette croyance, pour mieux s'autocontrôler.

</div>

<div class="texte" data-id="9.35">

<span>9.35 &mdash; </span>La *praxis* peut permettre au sujet de se distancier de la chose religieuse. L'inversion du substrat doit répondre à l'inversion du substrat. Le renversement religieux du mythe doit subir le renversement mythique du religieux.

</div>

<div class="texte" data-id="9.351">

<span>9.351 &mdash; </span>Le souhait d'émancipation sociale impose un retour au sacré tel un retour à la dynamique ontologique du réel, débarrassant la vie des scléroses hiérarchiques et autoritaires du phénomène religieux.

</div>

<div class="texte" data-id="9.3511">

<span>9.3511 &mdash; </span>Le sacré doit recouvrer sa pureté d'interrogation, sans autorité et sans contrainte, avec la conscience de sa genèse qui s'inscrit dans une dialectique *mystérieuse* de la matière.

</div>

<div class="texte" data-id="9.3512">

<span>9.3512 &mdash; </span>Le sacré ne doit jamais être une domination, mais un moyen de la vie qui cherche son dépassement. Tout phénomène religieux l'en empêche.

</div>

<div class="texte" data-id="9.352">

<span>9.352 &mdash; </span>Ce ne sont pas les réponses qui importent dans un retour révolutionnaire à l'*être*, mais la densité mobile du questionnement sans cesse renouvelé qui le meut.

</div>

<div class="texte" data-id="9.3521">

<span>9.3521 &mdash; </span>Le rapport mythique au monde, qui tolérerait une adaptation du récit émanant de la dialectique du sujet et de son environnement, et particulièrement de son terme, ne pourrait pas être la production du phénomène religieux, car elle se situerait en dehors de la politique, dans une acceptation que rien n'est figé dans le récit de la dialectique.

</div>

<div class="texte" data-id="9.3522">

<span>9.3522 &mdash; </span>L'émancipation au sein de la vie sociale passe par le recouvrement d'une conscience des causalités mythiques organisant la vie sociale. Cette conscience contribue à empêcher la domination de la chose religieuse sur la vie sociale.

</div>

<div class="texte" data-id="9.36">

<span>9.36 &mdash; </span>La chose religieuse fabrique, à partir de son idéologie et dans la fixité de son récit, une typologie biologique qui déforme la biologie dans l'effet de restreindre politiquement la contingence vitale.

</div>

<div class="texte" data-id="9.361">

<span>9.361 &mdash; </span>La distinction par la biologie n'est autre qu'une mise à distance politique de la mutation par une essentialisation de la biologie.

</div>

<div class="texte" data-id="9.3611">

<span>9.3611 &mdash; </span>L'essentialisation de la biologie procède politiquement par l'établissement de la domination des formes du vivant entre elles et par celui de caractéristiques innées et immuables de celles-ci.

</div>

<div class="texte" data-id="9.3612">

<span>9.3612 &mdash; </span>La considération de l'inné et de l'immuable dans la biologie consolide la structure hiérarchique d'une vision politique de la biologie, qui se construit sur l'idée de lutte entre les formes du vivant.

</div>

<div class="texte" data-id="9.3613">

<span>9.3613 &mdash; </span>La déformation de la biologie a un objectif de distinction politique. Elle trouve une visibilité dans l'histoire humaine, au travers du pouvoir politique qu'une caste d'humains, caractérisés par certains traits biologiques, comme la clarté de leur peau, tente d'imposer sur le reste de la société par la distinction que ces traits biologiques leur accordent.

</div>

<div class="texte" data-id="9.3614">

<span>9.3614 &mdash; </span>L'idée de lutte, entre les formes du vivant et au sein même des formes du vivant, est un travestissement politique de la biologie. Elle sert à établir une domination biologique de l'humain sur son environnement, mais aussi à permettre de justifier, directement ou indirectement, la discrimination sociale et l'impossibilité d'une égalité au sein de l'espèce humaine.

</div>

<div class="texte" data-id="9.3615">

<span>9.3615 &mdash; </span>La corporéité de la vie humaine, bornée à une compréhension fixe de celle-ci, sert l'hégémonie de la classe sociale produisant le *régime de savoir* pouvant dire que la corporéité *est* telle qu'elle veut se la représenter.

</div>

<div class="texte" data-id="9.36151">

<span>9.36151 &mdash; </span>La représentation du corps est toujours une représentation politique, et la représentation du corps qui ne tolère pas sa puissance de mutation est toujours la représentation du corps d'une classe sociale voulant soit consolider la verticalité de son hégémonie, soit parvenir à une domination sociale de son discours.

</div>

<div class="texte" data-id="9.3616">

<span>9.3616 &mdash; </span>La structure patriarcale de l'économie humaine, motrice de l'économie libérale contemporaine, compartimente la société par un réductionnisme de la corporéité en adéquation avec les intérêts de la classe sociale dominant la production du discours. Le sort du féminin ou celui des minorités ethniques, tout au long de l'histoire humaine, illustre ce cloisonnement politique par la biologie, soit en restreignant le corps à certaines de ses fonctions, soit en adjoignant à ses caractéristiques une symbolique de la distinction sociale.

</div>

<div class="texte" data-id="9.3617">

<span>9.3617 &mdash; </span>Le discours politique qui dit ce qu'*est* la biologie, alors que la biologie ne peut pas *être* en dehors de sa mutation permanente, empêche, à des fins hégémoniques, une diversité du *devenir* pouvant menacer sa domination sociale.

</div>

<div class="texte" data-id="9.362">

<span>9.362 &mdash; </span>Une femme *est* une femme : une femme *est* une réduction à une pétrification de son *être*. Une femme *est* une réclusion en sa fonction corporelle.

</div>

<div class="texte" data-id="9.3621">

<span>9.3621 &mdash; </span>La femme n'*est* pas femme, elle *devient* femme selon les critères que lui impose le pouvoir mâle dominant. Le *devenir-femme* de la femme n'a pour consistance que la direction culturelle dans laquelle souhaite la limiter la classe mâle dominante.

</div>

<div class="texte" data-id="9.3622">

<span>9.3622 &mdash; </span>Le *devenir* du *devenir-femme* demeure la faille ontologique dans l'ordre établi par le système patriarcal, puisque celui-ci *est* dans un refus ontologique du *devenir*, notamment par la manière dont la mort religieuse fige l'*être*. Il représente une façon de se délier de l'hégémonie culturelle, d'y échapper par la déviance, puisque le *devenir* est toujours une contingence ouverte à l'inattendu de la mutation.

</div>

<div class="texte" data-id="9.3623">

<span>9.3623 &mdash; </span>C'est dans le *devenir-femme*, en une transsubstantiation du vivant qui n'accepte pas la fixité de l'*être*, mais qui s'avance comme une fluidité conduisant son identité à la suite de l'ubiquité des mouvements de la physique, que se situe un potentiel révolutionnaire de destitution de la répartition tripartite du pouvoir politique et du rétablissement d'une contingence libérée dont disposent le corps et la matière.

</div>

<div class="texte" data-id="9.3624">

<span>9.3624 &mdash; </span>La construction du genre doit refuser toute fixité de sa construction établie, et doit devenir une étape vers une situation acceptant l'harmonie mutante du vivant au-delà de la forme humaine. La fluidité doit être rapportée à la vie même, au-delà de la simple forme humaine, vers toute la potentialité des croissances du vivant.

</div>

<div class="texte" data-id="9.3625">

<span>9.3625 &mdash; </span>La domination mâle sur la femme est une *économie*, un ensemble de normes domestiques qui cherche à capitaliser l'avenir par le corps de l'autre.

</div>

<div class="texte" data-id="9.3626">

<span>9.3626 &mdash; </span>Dire que la femme *est*, quelle que soit la situation sociale de cette énonciation, revient toujours à affirmer les intérêts de la classe dominant le social. Cette classe use du discours, en empêchant une fluidité de l'*être*, et ainsi en cristallisant le vivant en son idéologie, pour figer une structuration sociale de la domination.

</div>

<div class="texte" data-id="9.3627">

<span>9.3627 &mdash; </span>Par la domination de la femme, ce n'est pas la femme seule qui est dominée dans le social, mais c'est tout le social qui subit une mécanique de domination.

</div>

<div class="texte" data-id="9.363">

<span>9.363 &mdash; </span>En reprenant un schéma tripartite des fonctions sociales, s'envisage, dans cette soumission de la femme à sa fonction *re-productrice*, une nouvelle illustration du pouvoir que veut fonder les fonctions guerrière et religieuse sur celle de production.

</div>

<div class="texte" data-id="9.4">

<span>9.4 &mdash; </span>Le sacré représente un liant du phénomène social, tandis que le religieux représente la rigidité de ce liant tentant d'empêcher le *devenir* du phénomène social.

</div>

<div class="texte" data-id="9.41">

<span>9.41 &mdash; </span>L'idéologie religieuse structure un champ fini et opaque qui chapeaute le vivant par une essentialisation de ses caractéristiques, dans le seul but de l'assujettir.

</div>

<div class="texte" data-id="9.411">

<span>9.411 &mdash; </span>Il n'est pas envisageable de considérer une essence du vivant, sans passer par l'étude de l'idéologie dont s'arme la chose religieuse pour créer le leurre de l'essence du vivant.

</div>

<div class="texte" data-id="9.4111">

<span>9.4111 &mdash; </span>L'étude d'une essence religieuse relative à une essence humaine, qu'elle soit individuelle ou collective, résulte d'une déformation causée par la propension à la domination idéologique de la chose religieuse, en tant que production d'asservissement d'une classe par une autre.

</div>

<div class="texte" data-id="9.4112">

<span>9.4112 &mdash; </span>Dans le cadre du discours religieux, le vivant est lié au diktat d'une essence prédéfinie par les producteurs idéologiques de celle-ci, et la longueur de ses liens détermine la circonférence stricte du champ de son imagination.

</div>

<div class="texte" data-id="9.412">

<span>9.412 &mdash; </span>Une déconstruction de l'idéologie religieuse permet un retour émancipateur à la notion de sacré par l'extension constante de son champ d'imagination.

</div>

<div class="texte" data-id="9.413">

<span>9.413 &mdash; </span>La négation du religieux permet un retour au sacré, et ainsi à la confrontation du sujet à son *devenir*.

</div>

<div class="texte" data-id="9.42">

<span>9.42 &mdash; </span>Le sacré possède une substance mouvante, dont la conscience doit entraîner la multitude vers son *devenir* émancipateur.

</div>

<div class="texte" data-id="9.421">

<span>9.421 &mdash; </span>Le retour au sacré doit se faire dans la perspective anarchiste que présuppose le mouvement non hiérarchique des questions qu'il soulève.

</div>

<div class="texte" data-id="9.422">

<span>9.422 &mdash; </span>Pour que le retour au sacré soit purgé de la chose religieuse, il doit se faire avec la conscience que le sacré demeure lui-même une construction sociale, mais qui contient une puissance de destitution du pouvoir politique caractérisé par les rigueurs de ses verticalités.

</div>

<div class="texte" data-id="9.4221">

<span>9.4221 &mdash; </span>Pour rejoindre la dynamique du sacré, il s'avère nécessaire de procéder à un mouvement de *retour*, de *renversement*, de *révolution* afin de revenir collectivement aux questionnements ontologiques qui vont de la vie à la vie sans aucun intermédiaire, en débarrassant le social des scléroses provoquées par le phénomène religieux.

</div>

<div class="texte" data-id="9.423">

<span>9.423 &mdash; </span>Attiser les questions qui sous-tendent un retour au sacré est une *praxis* destituante qui ouvre un espace nouveau d'exploration à une multitude rassemblée.

</div>

<div class="texte" data-id="9.4231">

<span>9.4231 &mdash; </span>La *praxis* du sacré s'entend comme une dynamique interrogatrice de la vie par la vie, accessible à tous en toutes choses et par toutes choses.

</div>

<div class="texte" data-id="9.4232">

<span>9.4232 &mdash; </span>La *praxis* du sacré rétablit un champ cosmique du *devenir*, au-delà de la notion de verticalité, voire de celle d'horizontalité qui suppose indirectement celle de verticalité.

</div>

<div class="texte" data-id="9.4233">

<span>9.4233 &mdash; </span>Le *cosmisme* de la *praxis* du sacré dispose une politique *apolitique*, non orthogonale, qui évide les structures sociales des nécessités de sa hiérarchisation.

</div>

<div class="texte" data-id="9.4234">

<span>9.4234 &mdash; </span>Le *cosmisme* rend au vivant sa puissance de transsubstantiation, par une *praxis* du sacré qui rappelle à la biologie qu'avant d'être une déclinaison politique, elle demeure une expression contingente du cosmos.

</div>

<div class="texte" data-id="9.4235">

<span>9.4235 &mdash; </span>Le *cosmisme* offre au *sujet réticulaire* de n'être assujetti qu'au cosmos.

</div>

<div class="texte" data-id="9.4236">

<span>9.4236 &mdash; </span>Le *cosmisme* prélude l'établissement d'un *communisme cosmique*.

</div>

<div class="texte" data-id="10.">

<span>10. &mdash; </span>Le réel est un espace du mystère où tout ne cesse de se transsubstantier, et la *praxis* du mystère signifie cette permanence de la transsubstantiation jusque dans le social.

</div>

<div class="texte" data-id="10.1">

<span>10.1 &mdash; </span>La substance du réel existe en tant que transsubstantiation du réel, qui ne tolère aucune des fixités idéelles de l'essence, établies pour limiter les capacités de l'esprit à actualiser un pouvoir *autre* de l'esprit, en d'autres termes un pouvoir débordant les normes sociales qui conviennent de ce que l'esprit peut *être*.

</div>

<div class="texte" data-id="10.11">

<span>10.11 &mdash; </span>Il n'existe pas une *essence* des choses, il existe une *existence* des choses, qui se module par ses interactions avec d'autres *existences* agitant le réel.

</div>

<div class="texte" data-id="10.111">

<span>10.111 &mdash; </span>Toute essence est une construction sociale de l'*étant* qui refuse l'inconnu de son *devenir*.

</div>

<div class="texte" data-id="10.1111">

<span>10.1111 &mdash; </span>Puisque l'*étant* de l'*être* est une perpétuelle mutation de l'*être* en tant que *devenir*, dans le constant outrepassement de l'immobilité de ce que souhaite une construction sociale, il nous faut nier l'essence.

</div>

<div class="texte" data-id="10.1112">

<span>10.1112 &mdash; </span>Une construction sociale se transforme malgré elle, en subissant le *devenir* irrémédiable de son environnement. Toutefois, elle continue, malgré cette transformation, de se projeter dans la fixité de l'essence autour de laquelle elle façonne sa représentation du réel.

</div>

<div class="texte" data-id="10.112">

<span>10.112 &mdash; </span>La *chose*, en tant que particularité du réel, n'a pas d'essence. Elle est mue par des propriétés qui conditionnent la confrontation de son *devenir* à celui de toutes les autres choses.

</div>

<div class="texte" data-id="10.1121">

<span>10.1121 &mdash; </span>L'essence d'une chose, étymologiquement *l'étant de l'être* d'une chose, doit ainsi être ramenée à la *substance* d'une chose, étymologiquement ce qui *sous*-tient une chose. Cette *substance* saisit l'agencement des conditions matérielles de son existence et purge une quelconque dimension *idéelle* de sa réalité.

</div>

<div class="texte" data-id="10.1122">

<span>10.1122 &mdash; </span>L'essence doit être ramenée à son sens d'*étant*, tout en délaissant les perspectives idéelles sur son *être*. Ces perspectives s'énonceraient uniquement pour éluder l'ignorance de ce qui sous-tend la dynamique d'existence.

</div>

<div class="texte" data-id="10.113">

<span>10.113 &mdash; </span>L'affirmation de l'essence est une pusillanimité de l'esprit qui ne sait que faire des mystères de sa substance organique. Il s'injecte l'anesthésiant de *l'idée* de l'idée, cet éther qui nie *la matérialité* de l'idée, pour tenter d'*être* par-delà l'effroi que lui cause sa fin.

</div>

<div class="texte" data-id="10.1131">

<span>10.1131 &mdash; </span>L'esprit se refuse à sentir qu'il ne décline pas, mais se transforme dans le *devenir* de la matière, et par la matière qui le compose.

</div>

<div class="texte" data-id="10.1132">

<span>10.1132 &mdash; </span>L'esprit existe, et, par son existence, l'esprit peut sentir, puisque l'esprit existe par sa matière qui se dialectise dans la mémoire de cette dialectique.

</div>

<div class="texte" data-id="10.1133">

<span>10.1133 &mdash; </span>L'esprit a le pouvoir de sentir que l'esprit des choses existe sans la nécessité idéelle de leur essence, et il a le pouvoir de sentir, par là même, que l'essence des choses est une construction sociale qui entrave le *devenir* de leur existence.

</div>

<div class="texte" data-id="10.1134">

<span>10.1134 &mdash; </span>L'esprit tente d'établir constamment autour de lui le rassérénement d'une stabilité de l'*être* confondu au réel, mais le réel n'a pas de stabilité dans l'*être*, il *est* l'*être* en tant que *devenir* de la confrontation.

</div>

<div class="texte" data-id="10.12">

<span>10.12 &mdash; </span>Le réel croît par une perpétuelle confrontation à lui-même, et de cette dialectique substantielle apparaît pour l'humain une concrétion temporaire du savoir, qui peut donner l'illusion d'une essence, mais ne peut pourtant se représenter que dans l'image d'une destruction conduisant à la concrétion suivante du savoir.

</div>

<div class="texte" data-id="10.121">

<span>10.121 &mdash; </span>La recherche d'une essence est la négation de la *praxis*, tandis que la recherche d'une substance est *la négation de la négation* de la *praxis*.

</div>

<div class="texte" data-id="10.1211">

<span>10.1211 &mdash; </span>*La négation de la négation* de la *praxis* sonde la matière dans le but de dévoiler sa puissance vectorielle et d'entendre ce que l'existence a de dialectique.

</div>

<div class="texte" data-id="10.122">

<span>10.122 &mdash; </span>La matière s'anime, et c'est dans la motricité de cette animation que peut être creusé le sens des rapports dialectiques de la matière, de leur moteur qui se situe dans l'invisible du réel.

</div>

<div class="texte" data-id="10.1221">

<span>10.1221 &mdash; </span>Une essence de la vie humaine serait une déformation de la rigueur évolutive du *devenir* humain, détaché des rapports dialectiques de la matière dans lesquels il s'inscrit.

</div>

<div class="texte" data-id="10.1222">

<span>10.1222 &mdash; </span>L'ontologie ne peut être qu'une pratique du *devenir*, une raison qui va à la dynamique de l'*étant*. Elle ne se situera jamais dans la fixité d'un *étant de l'être*. Elle ne connaîtra jamais aucune essence.

</div>

<div class="texte" data-id="10.13">

<span>10.13 &mdash; </span>La substance du réel est la nécessité de l'accident.

</div>

<div class="texte" data-id="10.131">

<span>10.131 &mdash; </span>L'accident est une actualisation de la puissance du *devenir*.

</div>

<div class="texte" data-id="10.1311">

<span>10.1311 &mdash; </span>L'accident *est* en chaque situation de l'espace.

</div>

<div class="texte" data-id="10.1312">

<span>10.1312 &mdash; </span>Il n'y a pas à dissocier la substance de l'accident, puisque l'un ne peut exister sans l'autre, la substance étant un *devenir* par l'accident qui la conduit dans le *devenir*.

</div>

<div class="texte" data-id="10.1313">

<span>10.1313 &mdash; </span>Une substance sans accident est la création idéelle d'un réel sans *devenir*. Elle est une construction de l'esprit qui se rassérène en croyant à son propre absolu, en disant, hors de lui, l'absolu.

</div>

<div class="texte" data-id="10.1314">

<span>10.1314 &mdash; </span>Le réel est une puissance de l'espace, et la puissance n'existe que par la possibilité de son actualisation.

</div>

<div class="texte" data-id="10.1315">

<span>10.1315 &mdash; </span>Sans accident, la substance n'*est* pas, puisqu'elle ne peut pas *devenir*. Sans substance, l'accident n'*est* pas, puisqu'il ne peut pas *advenir*. Sans indivision de l'accident et de la substance, le réel n'*est* pas, puisqu'il ne peut pas *survenir*.

</div>

<div class="texte" data-id="10.1316">

<span>10.1316 &mdash; </span>Le réel *survient*, et *survient* en permanence dans la transsubstantiation de ce qu'il *est*.

</div>

<div class="texte" data-id="10.1317">

<span>10.1317 &mdash; </span>Par la fusion de l'accident et de la substance, le réel *est*, parce qu'il peut *pouvoir être*, dans chaque situation de sa croissance.

</div>

<div class="texte" data-id="10.132">

<span>10.132 &mdash; </span>Dans l'établissement restrictif du réel, par l'entendement de la substance telle une abstraction du réel, l'accident est relégué à une particularité qui ne peut être rapportée nécessairement à l'*étant* d'une chose.

</div>

<div class="texte" data-id="10.1321">

<span>10.1321 &mdash; </span>La relégation de l'accident à une ontologie de la non-nécessité permet à l'esprit d'évacuer la nécessité de sa transformation, pouvant aller jusqu'à la disparition de l'état à partir duquel il peut sentir sa dialectique au réel, c'est-à-dire son *mode d'être*.

</div>

<div class="texte" data-id="10.1322">

<span>10.1322 &mdash; </span>Une conceptualisation abstraite de la substance ferait de l'accident une non-nécessité accessoire à son *devenir*, mais c'est cette abstraction qui doit être chassée afin de percevoir le principe nécessaire de l'accident.

</div>

<div class="texte" data-id="10.133">

<span>10.133 &mdash; </span>La *nécessité* de la nécessité est la contingence.

</div>

<div class="texte" data-id="10.1331">

<span>10.1331 &mdash; </span>La substance d'une chose n'existe que par l'agrégat de ses accidents, qui s'associent au *devenir* de l'espace où ils adviennent.

</div>

<div class="texte" data-id="10.1332">

<span>10.1332 &mdash; </span>La substance humaine correspond à l'histoire des accidents d'une certaine forme biologique de la vie.

</div>

<div class="texte" data-id="10.1333">

<span>10.1333 &mdash; </span>L'histoire humaine ne s'entend qu'au travers de la construction sociale faite autour des couches successives d'accidents, qui ont vu la matérialité de la forme humaine dialectiser tant avec la matérialité de son environnement qu'avec sa propre matérialité.

</div>

<div class="texte" data-id="10.13331">

<span>10.13331 &mdash; </span>L'histoire humaine est un agencement des conditions matérielles de la vie en tant qu'espace d'avènement de l'accident.

</div>

<div class="texte" data-id="10.1334">

<span>10.1334 &mdash; </span>La confrontation de la vie humaine au réel constitue le noyau évolutif de sa substance, qui se comprend dans l'impermanence d'un état social de la dialectique.

</div>

<div class="texte" data-id="10.1335">

<span>10.1335 &mdash; </span>La vie humaine n'est que dialectique, et toute dialectique humaine n'est toujours qu'un dialogue avec l'inconnu de l'accident à venir.

</div>

<div class="texte" data-id="10.1336">

<span>10.1336 &mdash; </span>La nécessité humaine est la mutation de sa forme à la forme de l'accident.

</div>

<div class="texte" data-id="10.1337">

<span>10.1337 &mdash; </span>La vie humaine a la forme de l'accident.

</div>

<div class="texte" data-id="10.14">

<span>10.14 &mdash; </span>Les choses *sont* non par leur substance, mais par leur puissance de transsubstantiation.

</div>

<div class="texte" data-id="10.141">

<span>10.141 &mdash; </span>La transsubstantiation est le mouvement de *la dialectique de la dialectique*. Elle est le point de bascule où la synthèse de la dialectique de la matière à sa réalité immédiate annonce la thèse de ce que *sera* la matière dans la dialectique à venir.

</div>

<div class="texte" data-id="10.1411">

<span>10.1411 &mdash; </span>*La dialectique de la dialectique* est une mise en abyme d'un *devenir* qui *est* dans l'insatisfaction permanente d'*être*.

</div>

<div class="texte" data-id="10.1412">

<span>10.1412 &mdash; </span>*La dialectique de la dialectique* n'est pas double, elle n'est pas la conflagration de dialectique, elle est déjà triple dans l'association qui pourra surgir de sa progression dans l'espace de son *devenir*.

</div>

<div class="texte" data-id="10.1413">

<span>10.1413 &mdash; </span>Toute dialectique annonce une dialectique suivante, par l'impossibilité d'une fixité de sa synthèse.

</div>

<div class="texte" data-id="10.142">

<span>10.142 &mdash; </span>Dans la transsubstantiation doit être sondé non ce qui *est* dans la transformation, mais ce qui *soutient*, indissociablement, la forme nouvelle de la substance.

</div>

<div class="texte" data-id="10.1421">

<span>10.1421 &mdash; </span>La transsubstantiation doit être comprise comme la nécessité de la transformation qui anime la substance.

</div>

<div class="texte" data-id="10.1422">

<span>10.1422 &mdash; </span>La transsubstantiation conduit la substance à se transformer perpétuellement par la confrontation dialectique qui va de la matière à la matière, confrontation résultant des principes de la physique.

</div>

<div class="texte" data-id="10.1423">

<span>10.1423 &mdash; </span>La forme n'existe pas sans la substance, et inversement. Leur inextricabilité conditionne le mouvement du réel.

</div>

<div class="texte" data-id="10.143">

<span>10.143 &mdash; </span>La déconstruction de l'essence, ou plutôt l'acceptation de la transsubstantiation permanente du réel, déplace la dynamique ontologique du monde vers une idée matérielle de *substance* humaine inscrite dans un réel *devenant* avec elle.

</div>

<div class="texte" data-id="10.1431">

<span>10.1431 &mdash; </span>Le réel ne *devient* pas indépendamment du *devenir* humain, mais avec lui, malgré lui, malgré sa possible disparition.

</div>

<div class="texte" data-id="10.1432">

<span>10.1432 &mdash; </span>L'avènement de la disparition du *devenir* humain dans le *devenir* du réel n'est pas la marque de l'indépendance du *devenir* du réel par rapport au *devenir* humain.

</div>

<div class="texte" data-id="10.14321">

<span>10.14321 &mdash; </span>Dans un *devenir* du réel qui *serait* sans le *devenir* humain demeureraient les traces de ce que *fut* en tant que *devenir* l'humain.

</div>

<div class="texte" data-id="10.1433">

<span>10.1433 &mdash; </span>Le réel *devient avec* tout ce qui le constitue et tout ce qui peut le constituer.

</div>

<div class="texte" data-id="10.144">

<span>10.144 &mdash; </span>La nécessité de l'accident fait de la substance de la vie humaine une continuelle transsubstantiation des conditions de son établissement.

</div>

<div class="texte" data-id="10.1441">

<span>10.1441 &mdash; </span>L'impermanence d'un état social de la vie humaine peut être opposée, et ainsi indirectement définie, à la permanence d'une contingence transformatrice de l'accident qui fait advenir cet état social.

</div>

<div class="texte" data-id="10.1442">

<span>10.1442 &mdash; </span>Le groupe humain qui souhaiterait se saisir consciemment de la transsubstantiation qui anime le réel, telle une *praxis* lui offrant une émancipation dans son *devenir*, commettrait, pour les *régimes de savoir* passés et présents qui ont modelé l'humanité, une faute morale, puisque ce groupe remettrait en question l'ontologie finaliste sur laquelle s'appuie jusqu'à ce jour le pouvoir politique de l'humanité.

</div>

<div class="texte" data-id="10.2">

<span>10.2 &mdash; </span>Le mystère est une mise en abyme de l'obscurité du réel, dont la découverte a la puissance de propulser la raison en sa traversée.

</div>

<div class="texte" data-id="10.21">

<span>10.21 &mdash; </span>Le mystère s'oppose au mystère, comme la raison s'oppose à l'irraison.

</div>

<div class="texte" data-id="10.211">

<span>10.211 &mdash; </span>Dans l'opposition entre mystère et mystère, c'est à la raison qu'il incombe de constituer la distinction, en disposant la différence entre un mystère comme finalité et un mystère comme moyen.

</div>

<div class="texte" data-id="10.2111">

<span>10.2111 &mdash; </span>L'opposition entre mystère et mystère se place dans la langue telle une faille qui laisse entrevoir un champ de la *praxis*.

</div>

<div class="texte" data-id="10.2112">

<span>10.2112 &mdash; </span>L'étymon de mystère souligne sa dialectique interne : *muéô* dérive de *múô*, l'*initiation* découle de la *fermeture*. Le grec ancien entend l'apprentissage à partir de ce qui se ferme, du sens qui se refuse.

</div>

<div class="texte" data-id="10.212">

<span>10.212 &mdash; </span>La raison et sa dynamique de croissance se retrouvent entièrement dans la dialectique de la langue, qui fait du mystère une raison se dépassant elle-même au travers d'une initiation à ce qui est fermé.

</div>

<div class="texte" data-id="10.2121">

<span>10.2121 &mdash; </span>Ce qui est fermé peut s'ouvrir, et c'est à cette *praxis* que le mystère doit conduire la raison.

</div>

<div class="texte" data-id="10.2122">

<span>10.2122 &mdash; </span>La fermeture ne représente pas le réel au travers de l'idée de frontière, mais de celle de traversée.

</div>

<div class="texte" data-id="10.2123">

<span>10.2123 &mdash; </span>Le mystère réside dans la physique, comme la *fermeture* dans ce qui *devient*.

</div>

<div class="texte" data-id="10.2124">

<span>10.2124 &mdash; </span>À partir de la considération qu'une chose *est*, malgré le fait qu'elle semble *ne pas être* pour notre raison, en d'autres termes, qu'elle se *ferme* à nous, il faut entendre la puissance d'une ouverture de cette fermeture de l'*être*.

</div>

<div class="texte" data-id="10.21241">

<span>10.21241 &mdash; </span>L'ouverture de la fermeture ne se contente pas d'être une puissance de la révélation. Puisque le réel *devient avec* ce qui le constitue, l'accroissement de la connaissance du sujet méconnaissant fait évoluer le *devenir* du réel, et la révélation se présente ainsi comme une stimulation des potentialités du *devenir*, et ainsi une potentialité des transformations de l'*être* de la chose méconnue.

</div>

<div class="texte" data-id="10.21242">

<span>10.21242 &mdash; </span>Il ne sera jamais possible de connaître l'*être* d'une chose fermée, dans l'instant de sa fermeture. L'ouverture à la raison d'une chose jusqu'alors *fermée* à la raison ira à la transformation tant de l'*être* de la chose, que de l'*être* du sujet qui découvre la chose après son ouverture.

</div>

<div class="texte" data-id="10.21243">

<span>10.21243 &mdash; </span>La substance d'une chose fermée n'est pas la même que la substance d'une chose ouverte qui aura connu l'accident de son ouverture.

</div>

<div class="texte" data-id="10.2125">

<span>10.2125 &mdash; </span>Le mystère se présente toujours comme une irrésolution motrice. Son dévoilement ne correspond pas à son extinction, mais au dévoilement du voilement suivant.

</div>

<div class="texte" data-id="10.213">

<span>10.213 &mdash; </span>L'initiation permet d'*aller dans* le mystère, au cœur de ce qui est fermé, et ainsi, par cette présence en la fermeture, elle permet de transformer la chose, par le passage de sa fermeture à son ouverture. L'initiation va *dans* le mystère, le traverse, le transforme par sa traversée, sans jamais le faire disparaître.

</div>

<div class="texte" data-id="10.2131">

<span>10.2131 &mdash; </span>La considération d'un *devenir* du mystère par une dialectique entre le sujet méconnaissant et la chose méconnue est le point clef d'une initiation au mystère qui attise tant le *devenir* du sujet que celui de la chose mystérieuse.

</div>

<div class="texte" data-id="10.2132">

<span>10.2132 &mdash; </span>Le mystère ne se résout pas, il est une tension irrésolue entre le sujet méconnaissant et la chose méconnue, préfigurant le mystère à venir.

</div>

<div class="texte" data-id="10.2133">

<span>10.2133 &mdash; </span>Le mystère n'est jamais une violence entre la chose méconnue et le sujet méconnaissant, dans l'empêchement de leur *devenir* commun.

</div>

<div class="texte" data-id="10.2134">

<span>10.2134 &mdash; </span>L'ennemi du mystère apparaît comme le mystère de la fermeture fermée, à savoir comme la manière dont il est appréhendé socialement par un *régime de savoir* refusant à ce qui est fermé sa capacité à transformer ce qui est ouvert, et inversement.

</div>

<div class="texte" data-id="10.2135">

<span>10.2135 &mdash; </span>L'initiation à la fermeture ne peut pas être une satisfaction de la cloison, comme un serrurier ne se contenterait pas d'une porte close pour pratiquer son art.

</div>

<div class="texte" data-id="10.2136">

<span>10.2136 &mdash; </span>Il nous faut combattre l'impossibilité de s'initier à ce que la chose fermée *est* et ce qu'elle peut *pouvoir être* par l'initiation, en nous et pour elle-même.

</div>

<div class="texte" data-id="10.22">

<span>10.22 &mdash; </span>La vision horizontale et mécaniste de l'initiation, qui s'entend comme une traversée transformatrice du mystère, s'oppose à une vision verticale et finaliste de l'initiation, que nous qualifions de mysticisme, et qui dispose l'accession à ce que dissimule le mystère tel un achèvement moral à partir duquel peut s'établir une hiérarchie sociale.

</div>

<div class="texte" data-id="10.221">

<span>10.221 &mdash; </span>Une verticalité de l'accession au mystère sous-entend une élévation vers sa résolution telle une fin.

</div>

<div class="texte" data-id="10.2211">

<span>10.2211 &mdash; </span>La verticalité de l'initiation dresse une opacité du monde du sujet comme une frontière séparant les êtres. Le mystère s'y échafaude comme une finalité de la discrimination, une discrimination par la sensation paisible de la transcendance du sujet individué.

</div>

<div class="texte" data-id="10.222">

<span>10.222 &mdash; </span>Dans une vision verticale du mystère, il faut s'y hisser par des pratiques du corps, convenues socialement, afin de sentir ce qui se cache dans le réel. Cette accession serait la finalité paisible de l'esprit individuel qui atteindrait un état transcendant de communion.

</div>

<div class="texte" data-id="10.2221">

<span>10.2221 &mdash; </span>Il revient à chaque individu de produire les efforts sociaux nécessaires pour goûter à l'achèvement moral d'une vision verticale de l'initiation par son propre corps. Une telle vision promeut ainsi indirectement une vision individuante de la confrontation du sujet à son environnement, en restreignant le sujet à l'individuation de sa corporéité.

</div>

<div class="texte" data-id="10.2222">

<span>10.2222 &mdash; </span>La discrimination produite par une vision verticale de l'initiation se sert de l'esprit comme d'un leurre pour établir une impassibilité face à l'aléa de la sensation que peut subir le corps. Cet établissement produit une nouvelle discrimination sociale par le corps, dans la capacité pour certains sujets individués de se situer au-delà de ce qu'*est* le corps, ce qui fait du corps une valeur morale négative discriminant les sujets entre eux.

</div>

<div class="texte" data-id="10.223">

<span>10.223 &mdash; </span>La transcendance est l'outil mystique dont use socialement le contempteur du *devenir* mystérieux du réel.

</div>

<div class="texte" data-id="10.2231">

<span>10.2231 &mdash; </span>Le mysticisme fait du mystère sa propre fin où résiderait le secret d'une possible transcendance du corps accessible par le seul corps individué du sujet.

</div>

<div class="texte" data-id="10.2232">

<span>10.2232 &mdash; </span>Le mysticisme fonde un égoïsme, qui s'occupe du contentement d'une caste d'individus, par la sensation subjective d'une soi-disant transcendance du réel.

</div>

<div class="texte" data-id="10.2233">

<span>10.2233 &mdash; </span>Les pratiques mystiques sont socialement hiérarchisées, le plus souvent au sein d'un culte mystique, mais également par une considération comparative des individus au sein du groupe, comparaison établissant une distinction hiérarchique entre l'initié et le non-initié.

</div>

<div class="texte" data-id="10.2234">

<span>10.2234 &mdash; </span>Le mysticisme est une aristocratie, où certains, les *meilleurs*, savent sentir *au-delà* de l'immédiateté du réel, par leur seul mérite d'ascension de l'essence mystérieuse du réel. L'outil qu'y représente la transcendance aide à cette discrimination des *meilleurs*.

</div>

<div class="texte" data-id="10.2235">

<span>10.2235 &mdash; </span>La transcendance n'est jamais un partage permettant au sujet de *devenir* nombre dans le *devenir* de sa traversée du réel, *au-delà* de son immédiateté.

</div>

<div class="texte" data-id="10.2236">

<span>10.2236 &mdash; </span>Le mysticisme se suffit d'une promesse à la transcendance, maintenue néanmoins à bonne distance d'une *praxis* entendue à son échelle collective et partageuse.

</div>

<div class="texte" data-id="10.2237">

<span>10.2237 &mdash; </span>La transcendance est un outil social privant le sujet de son pouvoir révolutionnaire, que le sujet subisse ou non la discrimination de ne pouvoir y accéder, parce que la transcendance se place comme un chemin qui va au-delà de la matière, et conséquemment de la matérialité du social.

</div>

<div class="texte" data-id="10.2238">

<span>10.2238 &mdash; </span>La transcendance discrimine en repoussant tout ce qui ne répond pas aux intérêts de la classe qui en use.

</div>

<div class="texte" data-id="10.23">

<span>10.23 &mdash; </span>Le mysticisme, qui qualifie la stagnation transie du sujet individué au sein de l'opacité de son monde, doit être combattu, puisqu'il est un établissement du mystère pour le mystère.

</div>

<div class="texte" data-id="10.231">

<span>10.231 &mdash; </span>Le mystère forme un danger lorsqu'il est un contentement de lui-même, lorsqu'il refuse intrinsèquement toute idée de son dépassement et de la perpétuation du dépassement. Cette pratique aristocratique du réel rejette toute transsubstantiation du réel.

</div>

<div class="texte" data-id="10.232">

<span>10.232 &mdash; </span>Le mystère s'oppose au mystère, lorsque ses pratiques rationnelles refusent à l'irraison sa résolution du mystère dans une fixation opaque et discriminatoire du réel.

</div>

<div class="texte" data-id="10.2321">

<span>10.2321 &mdash; </span>Le mysticisme, qui prend le plus souvent les apparences d'un spiritualisme dualiste, mais qui peut aussi se décliner en un matérialisme satisfait de l'espace auquel le borne l'immédiateté du réel, se présente comme une irraison de l'immobilité, en cela qu'elle présuppose un dépassement *essentiel*, sans mettre en œuvre une dialectique cherchant l'extension du domaine de sa contingence.

</div>

<div class="texte" data-id="10.2322">

<span>10.2322 &mdash; </span>Les pratiques irrationnelles du mystère constituent le plus souvent, par leur agglomérat, un culte social plus ou moins organisé, le mystère demeurant toujours une pratique sociale malgré certaines de ses appréhensions qui prétendent à la transcendance singulière, comme l'ascétisme ou l'érémitisme. Ces pratiques prolongent néanmoins un héritage culturel par la confrontation d'une pratique sociale du corps et d'un questionnement ontologique.

</div>

<div class="texte" data-id="10.233">

<span>10.233 &mdash; </span>Le mysticisme peut être déconstruit par une raison établissant la fermeture du réel comme un lieu de passage, comme une extension de l'espace ontologique d'un sujet pluriel, sujet lui-même entendu en tant qu'espace d'agglomération accessible librement à chaque vie.

</div>

<div class="texte" data-id="10.2331">

<span>10.2331 &mdash; </span>Le réel ne peut pas être transcendé sans être nié, il ne peut être que transsubstantié, il ne peut être qu'une extension de lui-même : un espace *devenant* du *devenir*.

</div>

<div class="texte" data-id="10.234">

<span>10.234 &mdash; </span>Doivent être distinguées les nuances qui ornent le réel se refermant sur lui-même, s'éloignant de notre œil animal si peu enclin à observer sous les apparences. Doivent être distinguées ces nuances non par les caractéristiques de leur substance cachée, mais par la manière dont elles sont appréhendées socialement.

</div>

<div class="texte" data-id="10.3">

<span>10.3 &mdash; </span>Le mystère doit se saisir comme une *praxis* rationnelle et non hiérarchisée de l'extension des possibles s'offrant au *sujet réticulaire*, à la socialité qui le compose.

</div>

<div class="texte" data-id="10.31">

<span>10.31 &mdash; </span>La *praxis* du mystère en tant qu'initiation doit chercher sa propre distribution rationnelle, accessible à tous et par toutes choses.

</div>

<div class="texte" data-id="10.311">

<span>10.311 &mdash; </span>L'initiation au mystère doit être un partage, un mouvement social qui tente de sentir l'opacité et la traversée qui peut y avoir lieu.

</div>

<div class="texte" data-id="10.3111">

<span>10.3111 &mdash; </span>Si le mystère est une fermeture, son initiation doit être un mouvement commun vers cette fermeture, une initiation en tant que partage de la traversée.

</div>

<div class="texte" data-id="10.312">

<span>10.312 &mdash; </span>L'initiation au mystère prend, dans une perspective contemporaine, la forme de la science qui sonde l'obscurité qui se dresse face à elle.

</div>

<div class="texte" data-id="10.3121">

<span>10.3121 &mdash; </span>La science tente de dissiper l'obscurité immédiate du réel pour envisager la dialectique d'une obscurité médiate à venir.

</div>

<div class="texte" data-id="10.3122">

<span>10.3122 &mdash; </span>La science tente de dissiper l'obscurité pour faire de cette dissipation l'événement signifiant d'une amplification du mystère : la science ne *retient* pas le sujet dans ce qu'elle révèle, elle le propulse en l'inconnu.

</div>

<div class="texte" data-id="10.3123">

<span>10.3123 &mdash; </span>La science est la voie vers une initiation collective au mystère, au travers de la confrontation à ce que le cosmos détient d'inexploré.

</div>

<div class="texte" data-id="10.313">

<span>10.313 &mdash; </span>C'est à la science et à son exactitude de cultiver les questionnements autour du sacré par une confrontation précise et rationnelle à ce que le mystère *retient*.

</div>

<div class="texte" data-id="10.3131">

<span>10.3131 &mdash; </span>La science s'entend tel l'effritement de la *rétention* du mystère. Elle préfigure l'idée qui se situe après la certitude de l'idée, auprès d'une matière tout entière à sa fuite en avant.

</div>

<div class="texte" data-id="10.3132">

<span>10.3132 &mdash; </span>La science ne doit pas être saisie par une approche finaliste, mais comme un moyen, celui d'une traversée de l'instant du *régime de savoir* qui la fonde.

</div>

<div class="texte" data-id="10.3133">

<span>10.3133 &mdash; </span>La science ne produit pas un savoir absolu, mais possède les précarités de l'entendement qui le façonne.

</div>

<div class="texte" data-id="10.3134">

<span>10.3134 &mdash; </span>La rationalité du mystère, au travers d'une science sondant le réel, ne doit pas être réduite à un simple mécanisme causal, qui tenterait d'établir strictement des catégories de causes et des catégories d'effets. Elle propose au savoir sa propre transsubstantiation.

</div>

<div class="texte" data-id="10.314">

<span>10.314 &mdash; </span>L'initiation au mystère, qui ne détourne pas d'une théorie rationnelle de l'extension ontologique du sujet pluriel, dessine une voie émancipatrice, à laquelle chaque singularité qui compose la pluralité subjective peut contribuer librement.

</div>

<div class="texte" data-id="10.3141">

<span>10.3141 &mdash; </span>L'extension de la contingence du mystère à venir, par le savoir scientifique, dote l'humanité d'une sensation motrice du mystère : la sensation de se traverser soi-même, de découvrir la métamorphose à venir de la substance, au-delà des finitudes corporelles de la singularité individuelle, la sensation de participer collectivement au changement de substance du réel, et conséquemment du social.

</div>

<div class="texte" data-id="10.3142">

<span>10.3142 &mdash; </span>L'objectivité de la sensation de la transsubstantiation, et non de la transcendance, doit être creusée et distribuée horizontalement pour que s'envisage une extension de la contingence du mystère.

</div>

<div class="texte" data-id="10.3143">

<span>10.3143 &mdash; </span>L'extension de la contingence du mystère est une extension de l'espace ontologique du *devenir*.

</div>

<div class="texte" data-id="10.32">

<span>10.32 &mdash; </span>La *praxis* du mystère permet de traverser ontologiquement le réel en l'étendant par sa seule traversée.

</div>

<div class="texte" data-id="10.321">

<span>10.321 &mdash; </span>Ce qui compte n'est pas de traverser pour atteindre un achèvement de la traversée, mais de traverser pour perpétuer la traversée.

</div>

<div class="texte" data-id="10.322">

<span>10.322 &mdash; </span>La mécanique du *devenir* dans le mystère est un continuum de la dialectique entre l'*être* et l'*être* lui-même.

</div>

<div class="texte" data-id="10.3221">

<span>10.3221 &mdash; </span>La mécanique du *devenir* dans le mystère est celle d'une critique de la causalité comprise dans une stricte séparation temporelle de la cause et de l'effet. Cette mécanique pourrait être qualifiée de quantique, voire, de manière contre-intuitive, d'anticausale.

</div>

<div class="texte" data-id="10.32211">

<span>10.32211 &mdash; </span>La cause et l'effet n'existent pas, puisque tout est à la fois cause et effet, simultanément.

</div>

<div class="texte" data-id="10.323">

<span>10.323 &mdash; </span>Par la *praxis* du mystère, la raison cherche à s'accroître et à accroître ainsi les possibilités d'une extension de la spatialité ontologique où à la fois le sujet évolue et se confond.

</div>

<div class="texte" data-id="10.3231">

<span>10.3231 &mdash; </span>L'intellect humain balbutie un agencement technique de la raison, qui reste un apprentissage de sa transsubstantiation. Il recherche un *toujours plus* de ses possibles.

</div>

<div class="texte" data-id="10.3232">

<span>10.3232 &mdash; </span>Toute restriction de la voie vers le mystère reviendrait à *retenir* l'humain en une stagnation de son miroitement : *rétention* dans une immobilité niant la puissance de son mouvement de dépassement.

</div>

<div class="texte" data-id="10.3233">

<span>10.3233 &mdash; </span>Le mystère attise une fuite en avant, tel un dépassement de ses représentations présentes.

</div>

<div class="texte" data-id="10.33">

<span>10.33 &mdash; </span>Le réel se heurte au réel et ne cesse de s'engendrer. La *praxis* du mystère vise le dévoilement de cet engendrement et de la contingence qui en découle, jusque dans le social.

</div>

<div class="texte" data-id="10.331">

<span>10.331 &mdash; </span>Le champ des possibles s'étend par un mystère qui se *pratique* rationnellement en vue de sa résolution et de la révélation du mystère le prolongeant. Dès lors la rationalité du mystère apparaît par la rationalité de la *praxis*.

</div>

<div class="texte" data-id="10.332">

<span>10.332 &mdash; </span>La *praxis* du mystère est mue par une volonté *à* la transsubstantiation.

</div>

<div class="texte" data-id="10.3321">

<span>10.3321 &mdash; </span>La volonté *à* la transsubstantiation, la volonté qui va *à* la transsubstantiation davantage que la volonté qui veut détenir la transsubstantiation, se fait mouvement rationnel d'*aller dans* ce qui se *ferme* et d'y révéler l'espace *devenant* qui s'offre à l'*être*.

</div>

<div class="texte" data-id="10.333">

<span>10.333 &mdash; </span>En évitant de constituer sa propre fin, la *praxis* du mystère cherche la dialectique qui ne se contente pas de la facilité de la causalité, mais cherche au contraire un dépassement de la croyance *dualiste* de la cause et de l'effet, par l'établissement d'une permanence de la révolution de la cause et de l'effet, révolution qui se comprend comme une permanence de la confrontation transformatrice du réel à lui-même.

</div>

<div class="texte" data-id="11.">

<span>11. &mdash; </span>La *praxis critique ontologique* du *sujet réticulaire* l'entraîne vers un retour à l'*être*, dans un soulèvement révolutionnaire de l'*être*.

</div>

<div class="texte" data-id="11.1">

<span>11.1 &mdash; </span>Le monde n'est plus ni à interpréter ni à transformer, il est, dans un seul et même mouvement de révolution, à réinterpréter et à retransformer.

</div>

<div class="texte" data-id="11.11">

<span>11.11 &mdash; </span>L'interprétation et la transformation se suivent et se poursuivent dans une corrélation qui fait événement : l'événement d'une conscience révolutionnaire.

</div>

<div class="texte" data-id="11.111">

<span>11.111 &mdash; </span>En renvoyant précisément l'interprétation à l'interprétation *en soi* se révèle une intermédiation de l'immédiateté environnementale à laquelle manque un pouvoir de sondage du dissimulé, tandis que le renvoi de la transformation à la transformation *en soi* expose une fluctuation des surfaces où n'arrive pas à se déployer en profondeur la raison afin de prédisposer un changement substantiel, et ce contre tout contentement superficiel.

</div>

<div class="texte" data-id="11.1111">

<span>11.1111 &mdash; </span>L'évidence de l'interprétation se situe dans une passivité de la confrontation au réel, alors que l'interprétation se saisit aussi du réel pour le modeler en un espace d'existence pour la raison.

</div>

<div class="texte" data-id="11.1112">

<span>11.1112 &mdash; </span>L'évidence de la transformation se situe dans une activité de la confrontation au réel, alors que la transformation change la forme apparente du réel, selon une chaîne déterminée de causalité, sans toujours opérer une modification substantielle de celui-ci.

</div>

<div class="texte" data-id="11.112">

<span>11.112 &mdash; </span>L'interprétation se présente comme une glose d'une transformation précédente et menant à la transformation transformatrice du cadre depuis lequel sa capacité interprétative s'emploie. La causalité façonne son illusion à partir d'une boucle qui quête pourtant son allant.

</div>

<div class="texte" data-id="11.12">

<span>11.12 &mdash; </span>C'est l'entremise de l'interprétation et de l'inintelligence de la transformation qu'il s'avère nécessaire de *relever*, par l'entrechoc de l'interprétation interprétative et de la transformation transformatrice.

</div>

<div class="texte" data-id="11.121">

<span>11.121 &mdash; </span>La raison, par un mouvement autoréférant des signes qu'elle emploie, peut créer la permanence d'un mouvement du *devenir*, où les séquences d'interprétation et de transformation n'existent plus en leur unicité causale, mais existent, tel un état quantique, en une superposition où l'interprétation est à la fois transformation, retransformation et réinterprétation découlant de celle-ci, et la transformation est à la fois interprétation, réinterprétation et retransformation.

</div>

<div class="texte" data-id="11.122">

<span>11.122 &mdash; </span>Le fait de délaisser une séquence d'interprétation et de transformation pour se saisir des notions de réinterprétation et de retransformation offre à la raison de se placer en dehors des évidences de la causalité.

</div>

<div class="texte" data-id="11.1221">

<span>11.1221 &mdash; </span>La causalité s'efface dans la disparition de la séquence interprétation-transformation.

</div>

<div class="texte" data-id="11.13">

<span>11.13 &mdash; </span>La *réinterprétation-retransformation* est l'*action herméneutique* qui s'inscrit dans un retour métamorphique sur elle-même, et conséquemment sur l'objet avec lequel elle dialectise.

</div>

<div class="texte" data-id="11.131">

<span>11.131 &mdash; </span>L'*action herméneutique*, en d'autres termes la *réinterprétation-retransformation* qui se saisit de sa réalité immédiate, est une récursivité agissante de la raison qui poursuit la métamorphose permanente de l'*être*.

</div>

<div class="texte" data-id="11.1311">

<span>11.1311 &mdash; </span>L'articulation de l'*action herméneutique* provoque une identité mouvante et cyclique de l'interprétation et de la transformation.

</div>

<div class="texte" data-id="11.1312">

<span>11.1312 &mdash; </span>La situation de l'*action herméneutique* se trouve en même temps dans ce qui la devance et dans ce qui la précède.

</div>

<div class="texte" data-id="11.1313">

<span>11.1313 &mdash; </span>Le mouvement de l'*action herméneutique* est un retour continuel sur *soi*, mais pris dans l'extension de l'espace qui compose le *soi*.

</div>

<div class="texte" data-id="11.1314">

<span>11.1314 &mdash; </span>La réinterprétation et la retransformation de l'*action herméneutique* signalent leur identité dans l'émiettement des simplicités causales de l'appréhension du réel pour signifier une superposition des états du réel où à la fois tout *était*, *est* et *sera*.

</div>

<div class="texte" data-id="11.1315">

<span>11.1315 &mdash; </span>En l'*action herméneutique*, la construction causale s'efface. L'effet y est cause, et inversement, en l'événement identique de leur transsubstantiation.

</div>

<div class="texte" data-id="11.1316">

<span>11.1316 &mdash; </span>Pour souligner l'identité de la réinterprétation et de la retransformation, il importe de rapprocher cette jonction des pratiques sociales qui agitent une *praxis du soulèvement*, tout en écartant l'évidence sémantique de la passivité interprétative et de l'activité transformatrice.

</div>

<div class="texte" data-id="11.1317">

<span>11.1317 &mdash; </span>L'évidence sémantique apparente de la passivité interprétative et de l'activité transformatrice demeure l'outil de la contre-révolution dont use le *régime de savoir* dominant, et que l'*action herméneutique* cherche à annihiler.

</div>

<div class="texte" data-id="11.132">

<span>11.132 &mdash; </span>La synthèse additive des caractéristiques propres à l'*action herméneutique* explique la mécanique vitale du *devenir*, se comprenant par la croissance de sa spatialité et non par l'avancement de sa temporalité.

</div>

<div class="texte" data-id="11.1321">

<span>11.1321 &mdash; </span>Ce qui devance *devient* ce qui précède et réciproquement, dans un mouvement de révolution.

</div>

<div class="texte" data-id="11.1322">

<span>11.1322 &mdash; </span>L'événement de la conscience révolutionnaire apparaît comme une fatalité de l'accident dans la révolution infinie du *devenir*.

</div>

<div class="texte" data-id="11.133">

<span>11.133 &mdash; </span>L'*action herméneutique* révèle à la conscience sa puissance révolutionnaire.

</div>

<div class="texte" data-id="11.1331">

<span>11.1331 &mdash; </span>L'*action herméneutique* s'opère en l'événement de la *praxis* que nous nommons révolution.

</div>

<div class="texte" data-id="11.1332">

<span>11.1332 &mdash; </span>La dialectique entre le sujet et le réel est toujours une *dialectique de la dialectique*, retournant en son endroit initial, qui est à la fois l'achèvement de son cycle, l'avènement de son cycle prochain, l'événement du cycle révolutionnaire lui-même.

</div>

<div class="texte" data-id="11.2">

<span>11.2 &mdash; </span>La révolution ontologique, qui se propage dans la conscience du sujet par le mouvement cyclique de l'*action herméneutique*, se transcrit dans le social par une *praxis du soulèvement*.

</div>

<div class="texte" data-id="11.21">

<span>11.21 &mdash; </span>La révolution ontologique est un *soulèvement* du réel qui va à sa transsubstantiation. Une *praxis du soulèvement* correspond à la transcription politique de la révolution ontologique dans le phénomène social.

</div>

<div class="texte" data-id="11.211">

<span>11.211 &mdash; </span>Le retour *du* même n'est pas un retour *au* même, il marque sa progression dans l'*être* par l'inscription de la révolution dans une extension continuelle de l'*être*.

</div>

<div class="texte" data-id="11.2111">

<span>11.2111 &mdash; </span>L'*être* en tant que devenir est une révolution permanente de l'*être*.

</div>

<div class="texte" data-id="11.2112">

<span>11.2112 &mdash; </span>La spirale est une torsion, son tournoiement une tension vers un *toujours plus* de son *être*.

</div>

<div class="texte" data-id="11.2113">

<span>11.2113 &mdash; </span>La révolution par un mouvement de retour continuel de l'*être* à l'*être* provoque une transsubstantiation de l'*être*.

</div>

<div class="texte" data-id="11.2114">

<span>11.2114 &mdash; </span>L'étape synthétique de la dialectique ne se contente pas d'*être*, mais établit son *toujours plus* dans un retour révolutionnaire de son mouvement de dépassement, et ce mouvement de dépassement se saisit de sa propre évolution pour faire révolution.

</div>

<div class="texte" data-id="11.2115">

<span>11.2115 &mdash; </span>Le phénomène de dépassement ne s'entend pas comme le simple franchissement d'une étape, mais comme la traversée prise pour elle-même, là où se *relève* sa dynamique *en soi*.

</div>

<div class="texte" data-id="11.212">

<span>11.212 &mdash; </span>La notion d'*Aufhebung* totalise ce que constitue le phénomène de dépassement dialectique par une conjonction de la négativité et de la positivité de son signifié, à la fois *suppression* et *conservation*, en exprimant ce qui se maintient dans ce qui s'abolit, mais en laissant entendre sa transformation.

</div>

<div class="texte" data-id="11.2121">

<span>11.2121 &mdash; </span>La *négativité positive* de l'*Aufhebung*, notion traduite tantôt par *dépassement*, tantôt par *sursomption*, s'entend le mieux dans ce que d'aucuns ont traduit par le terme de *relève*. Celui-ci continue malgré tout de contenir un manque par rapport à l'*Aufhebung*. La signification de changement du terme *relève* n'a pas à la fois la force négative d'abrogation et celle positive de maintien, voire de dépassement. Il faudrait coupler au terme de *relève* le sens de *relèvement*, tout en accentuant cette opposition de leur négativité et de leur positivité, ce qui peut s'entendre en revenant simplement aux multiples sens du verbe *relever*. Malgré une traduction exacte quasi impossible, rapprocher le signifié du verbe *aufheben* de celui du verbe *relever* exprime la tension synthétique de l'*Aufhebung* dialectique, en contenant des sens contradictoires qui fabriquent ensemble une synthèse.

</div>

<div class="texte" data-id="11.2122">

<span>11.2122 &mdash; </span>Le verbe *relever* peut décrire par une variation autour de sa racine l'ensemble du processus dialectique. *Relever* marque ainsi les mouvements de la dialectique, avec la thèse ou *la levée*, l'antithèse ou *l'enlèvement*, la synthèse ou *la relève*.

</div>

<div class="texte" data-id="11.2123">

<span>11.2123 &mdash; </span>L'*Aufhebung* de la substance va au soulèvement du mystère du réel.

</div>

<div class="texte" data-id="11.213">

<span>11.213 &mdash; </span>*La dialectique de la dialectique* fait de *la relève* un *soulèvement* de ce qui s'y dissimule.

</div>

<div class="texte" data-id="11.2131">

<span>11.2131 &mdash; </span>La révolution ontologique se *relève* dans le social par une *praxis* de son soulèvement.

</div>

<div class="texte" data-id="11.22">

<span>11.22 &mdash; </span>La *praxis du soulèvement* fait de la *praxis* une critique ontologique agissante en tant que révolution ontologique du social.

</div>

<div class="texte" data-id="11.221">

<span>11.221 &mdash; </span>La *praxis du soulèvement* articule la jonction révolutionnaire entre création et théorie par une *action herméneutique*, appliquée par le vivant à ce qu'il considère *être*, afin de déterminer ce que l'*être* *serait* au-delà de sa considération.

</div>

<div class="texte" data-id="11.2211">

<span>11.2211 &mdash; </span>La *praxis du soulèvement* tente de relever, pour la raison qui la met en œuvre, la potentialité actionnelle de ce qui se dissimule derrière les évidences de l'espace du *devenir* où se développe la vie.

</div>

<div class="texte" data-id="11.2212">

<span>11.2212 &mdash; </span>Qu'est-ce que la révolution si ce n'est le perpétuel soulèvement du *même* l'entraînant vers sa variation, le carré de son *Aufhebung*, brisant son identité et réordonnant le désordre de ses brisures ?

</div>

<div class="texte" data-id="11.222">

<span>11.222 &mdash; </span>Dans l'image torsive de la spirale, le mouvement de révolution s'entend comme une récursivité de la dialectique, comme *la négation de la négation* qui se réapplique au résultat de *la négation de la négation*.

</div>

<div class="texte" data-id="11.2221">

<span>11.2221 &mdash; </span>*La négation de la négation* se multiplie par elle-même, elle est la synthèse au carré, le recommencement du recommencement de son mouvement critique : voilà la perpétuité du *soulèvement*.

</div>

<div class="texte" data-id="11.2222">

<span>11.2222 &mdash; </span>Le soulèvement quête le mouvement du *toujours plus* dans et par le tourbillon qui l'entraîne vers l'inconnu de son *devenir*.

</div>

<div class="texte" data-id="11.2223">

<span>11.2223 &mdash; </span>La *praxis du soulèvement* a la capacité politique de briser la cristallisation de ce qui *est*, causée par ce qui *paraît*, par la volonté d'établir pour le social une accélération de son *devenir*.

</div>

<div class="texte" data-id="11.23">

<span>11.23 &mdash; </span>L'*action herméneutique* est une critique ontologique, dans le sens qu'elle exerce ses capacités de discernement en deçà de ce qui *paraît*, à la recherche des dynamiques de ce qui peut se soulever en son *être*.

</div>

<div class="texte" data-id="11.231">

<span>11.231 &mdash; </span>La critique qui se déploie par l'*action herméneutique* permet à la raison de discerner le *devenir* de son *être*.

</div>

<div class="texte" data-id="11.232">

<span>11.232 &mdash; </span>La critique se place entre la passivité de l'interprétation et l'activité de la transformation, comprises en leur acception classique, pour les fusionner en une *praxis* substantiellement révolutionnaire.

</div>

<div class="texte" data-id="11.2321">

<span>11.2321 &mdash; </span>L'*action herméneutique* est une modalité de la *praxis du soulèvement*.

</div>

<div class="texte" data-id="11.2322">

<span>11.2322 &mdash; </span>L'interprétation et la transformation du réel se trouvent *relevées* par l'*action herméneutique* qui *réinterprète-retransforme* sa réalité immédiate par le discernement de la critique appliquée au réel.

</div>

<div class="texte" data-id="11.2323">

<span>11.2323 &mdash; </span>La critique issue de l'*action herméneutique* alimente un discernement de la *praxis* jusqu'à une conscience mettant en cause le *régime de savoir* à partir duquel le sens de la critique s'entend.

</div>

<div class="texte" data-id="11.23231">

<span>11.23231 &mdash; </span>La critique devient critique de la signification de la critique, pour *relever* le *régime de savoir* à partir duquel la critique apparaît, et ainsi opérer sa propre mutation.

</div>

<div class="texte" data-id="11.2324">

<span>11.2324 &mdash; </span>L'*action herméneutique* provoquera dans le social l'apparition de nouvelles formes de sa substance par le renouvellement de la signification du social, et conséquemment provoquera une mutation de la substance du social elle-même du fait de ce lien inextricable entre forme et substance du réel.

</div>

<div class="texte" data-id="11.233">

<span>11.233 &mdash; </span>Ne succèdent plus à des perspectives théoriques des perspectives politiques dans une décomposition distinctive du processus historique, mais s'opère une jonction de l'action, de la théorie et de la création dans le mouvement d'un soulèvement se soulevant lui-même, dans le cycle exponentiel d'une extension de l'*être*.

</div>

<div class="texte" data-id="11.3">

<span>11.3 &mdash; </span>Pour que la permanence de la révolution de l'*être* trouve son extension jusque dans le social, la *praxis* du *sujet réticulaire* doit être une permanence de la critique de sa participation à l'*être*.

</div>

<div class="texte" data-id="11.31">

<span>11.31 &mdash; </span>Toute *praxis* a fondamentalement la puissance de se déployer dans le réel par sa nature critique et sa nature ontologique, puisqu'elle peut déployer un champ actionnel créateur d'une continuité spatiale de l'*être*, à partir d'une compréhension réflexive et extensive qui va de l'*être* à l'*être*.

</div>

<div class="texte" data-id="11.311">

<span>11.311 &mdash; </span>Puisque le temps est une dimension de l'espace en tant que connaissance de celui-ci, dimension figée de l'espace pour le sujet qui se confronte à son immédiateté, il importe de ne pas s'arrêter de *révolutionner* le temps pour que l'extension de l'*être* devienne l'exponentielle des contingences de son événement.

</div>

<div class="texte" data-id="11.3111">

<span>11.3111 &mdash; </span>L'*être* advient dans l'*être*, et son événement est la permanence de sa métamorphose.

</div>

<div class="texte" data-id="11.3112">

<span>11.3112 &mdash; </span>L'*être* est métamorphique, puisqu'il *devient*, et même lorsqu'il *n'est pas*, il demeure une puissance du surgissement de sa métamorphose.

</div>

<div class="texte" data-id="11.312">

<span>11.312 &mdash; </span>La révolution doit suivre le mouvement métamorphique de l'*être* dans l'*être*.

</div>

<div class="texte" data-id="11.3121">

<span>11.3121 &mdash; </span>La révolution doit *être*, par conséquent, elle doit être l'extension sociale qui se rapproche de la *physique* au plus près de son allant, dans la suite continuelle de ce qui croît.

</div>

<div class="texte" data-id="11.313">

<span>11.313 &mdash; </span>Le *cosmisme* du *sujet réticulaire* fait de sa *praxis* une pratique métamorphique de la réalité avec laquelle le *sujet réticulaire* dialectise.

</div>

<div class="texte" data-id="11.32">

<span>11.32 &mdash; </span>La *praxis critique ontologique* dispose une raison pratique dans le social, qui y dispose à son tour une puissance de l'émancipation.

</div>

<div class="texte" data-id="11.321">

<span>11.321 &mdash; </span>La *praxis critique ontologique*, dans sa perspective émancipatrice, se transcrit dans le social telle une *praxis du soulèvement* qui se diffuse sans hiérarchie dans la multitude agissante.

</div>

<div class="texte" data-id="11.3211">

<span>11.3211 &mdash; </span>Le social peut se transsubstantier et rejoindre l'ontologie du *devenir* par la dynamique de soulèvement qu'offre la *praxis critique ontologique*.

</div>

<div class="texte" data-id="11.322">

<span>11.322 &mdash; </span>L'événement de la révolution ontologique qui s'opère par la *praxis du soulèvement* est l'accession à la communion et la communion avec ce qui est commun.

</div>

<div class="texte" data-id="11.3221">

<span>11.3221 &mdash; </span>Le processus de soulèvement qui apparaît par l'application au social d'une *praxis critique ontologique* y expose ce qu'il y a de commun dans le cosmos avec le social, et les interconnexions possibles qui peuvent s'y établir.

</div>

<div class="texte" data-id="11.3222">

<span>11.3222 &mdash; </span>Débarrassé des pesanteurs d'une idéologie qui compartimente le social aux seules fins de servir les propensions d'*avoir* d'une classe dominante parce que possédante, le social peut s'unir non en ce qu'il *a* d'identique, mais en ce qu'il *est* matériellement identique au cosmos.

</div>

<div class="texte" data-id="11.3223">

<span>11.3223 &mdash; </span>Dans une continuelle et commune transsubstantiation du vivant qui étend ses possibles à la poursuite d'une *physique*, qui ne cesse elle-même d'accroître la contingence du domaine où ces possibles propres au vivant peuvent advenir, la *praxis critique ontologique* offre au *sujet réticulaire* un *mode d'être* par le soulèvement qui s'opère dans le social pour rejoindre ce que le social a de cosmique, c'est-à-dire ce par quoi il *est du* cosmos.

</div>

<div class="texte" data-id="11.3224">

<span>11.3224 &mdash; </span>Ce qu'il y a de cosmique dans le social est toujours ce qu'il y a d'harmonieux en son *devenir*.

</div>

<div class="texte" data-id="11.323">

<span>11.323 &mdash; </span>Le retour à l'*être* passe par une *praxis critique ontologique* qui purge la modernité, mue par son capitalisme libéral, de son ontologie de l'*avoir*.

</div>

<div class="texte" data-id="11.3231">

<span>11.3231 &mdash; </span>La *praxis critique ontologique* dépouille la modernité de son simulacre de l'*être* qui masque toute possibilité d'une traversée de la réalité immédiate.

</div>

<div class="texte" data-id="11.3232">

<span>11.3232 &mdash; </span>La *praxis critique ontologique* désœuvre la raison marchande, elle la défait de la mystification qui consiste en un matérialisme de l'échange ne sachant se situer que dans les apparences et dans l'accaparement de ces apparences.

</div>

<div class="texte" data-id="11.3233">

<span>11.3233 &mdash; </span>La *praxis critique ontologique* permet de rejoindre les croissances et les excroissances d'un réel ne finissant pas de *devenir* par-delà le *régime de savoir* moderne.

</div>

<div class="texte" data-id="11.33">

<span>11.33 &mdash; </span>L'*être* révolutionné de l'*être* se révèle à lui-même par la *praxis critique ontologique*.

</div>

<div class="texte" data-id="11.331">

<span>11.331 &mdash; </span>Tout est possible pour le social libéré de l'*avoir* qui fomente son retour à l'*être*, puisque tout est possible en tout ce qui *devient*.

</div>

<div class="texte" data-id="11.3311">

<span>11.3311 &mdash; </span>Libéré des emprises de l'*avoir*, le social libère la vie de la politique.

</div>

<div class="texte" data-id="11.3312">

<span>11.3312 &mdash; </span>Le social émancipé de ses fixités se rassemble autour de ce qui est commun dans l'*être*.

</div>

<div class="texte" data-id="11.3313">

<span>11.3313 &mdash; </span>Le social entame l'infinie traversée de ce qui ne finit pas de *devenir* en lui.

</div>

<div class="texte" data-id="11.332">

<span>11.332 &mdash; </span>La révolution de l'*être* est une promesse de révolution de la politique.

</div>

<div class="texte" data-id="11.3321">

<span>11.3321 &mdash; </span>Le renversement de notre temps commence par le soulèvement de notre espace, afin qu'il *soit* dans l'*être* autrement.

</div>

<div class="texte" data-id="11.3322">

<span>11.3322 &mdash; </span>La seule destination de l'*être* s'entraperçoit dans un soulèvement qui ne se contente jamais de lui-même. La destination de l'*être* apparaît sans discontinuer dans sa permanente disparition. Tout dans l'*être* va au *non-être*, en conséquence, tout dans l'*être* va à sa métamorphose.

</div>

<div class="texte" data-id="11.3323">

<span>11.3323 &mdash; </span>La révolution de l'*être* s'offre à ce qui se transsubstantie en attendant déjà l'inconnu de sa transsubstantiation suivante.

</div>

<div class="texte" data-id="11.333">

<span>11.333 &mdash; </span>L'*être* révolutionné de l'*être* est l'*être* qui ne cesse de se traverser lui-même.

</div>

<div class="texte" data-id="11.3331">

<span>11.3331 &mdash; </span>Tout *est* parce que tout *devient*, et tout *devient* parce que tout se *transsubstantie*.

</div>

<div class="texte" data-id="11.3332">

<span>11.3332 &mdash; </span>L'extension de l'*être* est l'infini de l'extension suivante, et son *devenir* d'infini se meut et se mue en la transsubstantiation de toutes choses.

</div>

<div class="texte" data-id="11.3333">

<span>11.3333 &mdash; </span>La traversée n'a pas de fin puisqu'elle est sa propre fin.

</div>

