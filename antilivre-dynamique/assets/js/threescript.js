const container = document.querySelector('.container');
const textes = [...document.querySelectorAll('.texte')];
const texteContainer = document.querySelector('.textes');
let textesOpen = false;

let camPos;

let data = { nodes: [], links: [] };

// Color test
// let colors = [
//   [255, 255, 255],
//   [255, 0, 0],
//   [0, 255, 0],
//   [0, 0, 255],
//   [255, 255, 0],
//   [0, 255, 255],
//   [255, 0, 255],
//   [100, 100, 50],
//   [20, 100, 50],
//   [20, 10, 100],
//   [20, 20, 100],
//   [100, 10, 100],
// ]

// Color mix
let colors = [
  [255, 255, 255],
  [255, 195, 0],
  [195, 244, 102],
  [11, 80, 60],
  [100, 0, 1],
  [90, 212, 145],
  [124, 225, 252],
  [66, 45, 196],
  [154, 164, 238],
  [223, 81, 108],
  [254, 172, 178],
  [245, 214, 160],
]

// // Color red
// let colors = [
//   [255, 255, 255],
//   [240, 193, 123],
//   [201, 147, 51],
//   [176, 116, 62],
//   [186, 163, 109],
//   [130, 0, 1],
//   [253, 154, 159],
//   [242, 105, 80],
//   [174, 2, 2],
//   [225, 66, 11],
//   [232, 152, 89],
//   [255, 175, 0],
// ]

// // Color green-yellow
// let colors = [
//   [255, 255, 255],
//   [16,105,85],
//   [103,155,40],
//   [179, 220, 206],
//   [202, 166, 56],
//   [113, 152, 7],
//   [162, 198, 92],
//   [207, 241, 33],
//   [114, 119, 3],
//   [199, 144, 79],
//   [74,70,6],
//   [248, 217, 13],
// ]

// Color green-blue
// let colors = [
//   [255, 255, 255],
//   [5, 75, 21],
//   [103, 155, 40],
//   [179, 220, 206],
//   [103, 127, 29],
//   [233, 198, 98],
//   [103, 150, 114],
//   [83, 122, 103],
//   [113, 185, 181],
//   [234, 205, 145],
//   [96, 152, 149],
//   [10, 136, 73],
// ]

//
// Make a list of links for data vizualisation
//
function tractatus() {
  textes.forEach(el => {
    const id = el.dataset.id;
    const group = el.dataset.id.substring(0, el.dataset.id.indexOf('.'));
    const lastEl = el.dataset.id.slice(-1);
    let hierarch;
    let level;
    if (lastEl == '.') {
      hierarch = el.dataset.id.substring(0, el.dataset.id.indexOf('.'));
      level = 1;
    } else {
      hierarch = el.dataset.id.slice(-1);
      level = el.dataset.id.substring(el.dataset.id.indexOf('.')).length;
    }
    const node = {
      "id": id,
      "group": group,
      "level": level,
      "hierarch": hierarch
    };
    data.nodes.push(node);
  });
  const entries = [];
  let max = 0;
  const order = [];

  data.nodes.forEach((el) => {
    if (el.id.length - 1 > max) max = el.id.length - 1;
    entries.push(el.id);
  });

  for (let i = 1; i <= max; i++) {
    const subarray = [];
    entries.forEach((el) => {
      if (i == 1 && el.slice(-1) == '.') {
        let toPush = el.substring(0, el.indexOf('.'));
        subarray.push(toPush);
      }
      if (i != 1 && el.slice(-1) != '.') {
        let subnum = el.substring(el.indexOf('.') + 1);
        if (subnum.length + 1 == i) {
          subarray.push(el);
        }
      }
    });
    order.push(subarray);
  }

  order[0] = order[0].map(el => el + '.');

  for (let i = 0; i < max - 1; i++) {
    if (i == 0) {
      order[i].forEach((el) => {
        const link = {
          "source": "0.",
          "target": el,
          "type": "first"
        }
        data.links.push(link);
      });
    }
    order[i].forEach((el) => {
      order[i + 1].forEach((search) => {
        const partSearch = search.substring(0, el.length);
        if (el === partSearch) {
          const link = {
            "source": el,
            "target": search,
            "type": "other"
          }
          data.links.push(link);
        }
      });
    });
  }
  const finish = document.querySelector(".loading");
  finish.classList.add("loading--all-is-loaded");
}
tractatus();

// console.log(data);
const graph = ForceGraph3D({
  controlType: 'trackball',
})(container)
  .graphData(data)
  // .jsonUrl('./js/praxis.json')
  .backgroundColor('#000000')
  .showNavInfo(false)
  .linkOpacity(0.62)
  // .linkColor((link) => '#ffffff')
  .linkColor((link) => link.type == "first" ? '#ffff00' : '#ffffff')
  .linkColor((link) => {
    let group = parseInt(link.target);
    let r = colors[group][0];
    let g = colors[group][1];
    let b = colors[group][2];

    if (r > 255) r = 255;
    if (g > 255) g = 255;
    if (b > 255) b = 255;

    return `rgb(${r},${g},${b})`;

  })
  .linkResolution(20)
  .nodeResolution(20)
  .nodeColor((el) => {
    // colors = [
    //   [255,255,255],
    //   [255,255,255],
    //   [255,255,255],
    //   [255,255,255],
    //   [255,255,255],
    //   [255,255,255],
    //   [255,255,255],
    //   [255,255,255],
    //   [255,255,255],
    //   [255,255,255],
    //   [255,255,255],
    //   [255,255,255],
    // ]
    let group = parseInt(el.group);

    let modifyColor = 0;

    // if (el.level > 1) {
    //   modifyColor = (el.level * 25);
    //   if (el.hierarch > 1) {
    //     modifyColor += (el.hierarch * 10);
    //   }
    // }

    let r = colors[group][0] + modifyColor;
    let g = colors[group][1] + modifyColor;
    let b = colors[group][2] + modifyColor;

    if (r > 255) r = 255;
    if (g > 255) g = 255;
    if (b > 255) b = 255;

    // console.log(`id: ${el.id} ; level: ${el.level}; hierarch: ${el.hierarch}; rgb: ${r},${g},${b}`);

    return `rgb(${r},${g},${b})`;
  })
  // .nodeAutoColorBy('group')
  .nodeOpacity(1)
  .nodeVal(20)
  .nodeThreeObject((el) => {
    let group = parseInt(el.group);

    let r = colors[group][0];
    let g = colors[group][1];
    let b = colors[group][2];

    if (r > 255) r = 255;
    if (g > 255) g = 255;
    if (b > 255) b = 255;

    // return `rgb(${r},${g},${b})`;
    const color = new THREE.Color(`rgb(${r},${g},${b})`);
    return new THREE.Mesh(
      // new THREE.BoxGeometry(20, 20, 20),
      new THREE.SphereGeometry(7),
      new THREE.MeshPhongMaterial({
        color: color,
        shininess: 50,
        // transparent: true,
        // opacity: 0.75
      })
    )
  })
  .onNodeDragEnd(node => {
    node.fx = node.x;
    node.fy = node.y;
    node.fz = node.z;
  })
  .nodeLabel(node => `<span class="label-hover">${node.id}</span>`)
  .onNodeClick(node => {
    const texteChoisi = document.querySelector('[data-id="' + node.id + '"]')
    graph.enableNavigationControls(false);
    camPos = graph.cameraPosition();
    document.querySelector('.scene-tooltip').innerHTML = '';
    if (textesOpen) {
      textes.forEach((el) => {
        el.classList.remove('show');
      });
      texteChoisi.classList.add('show');
      document.querySelector('.texte.show').scrollIntoView();
    } else {
      texteContainer.classList.add('show');
      texteChoisi.classList.add('show');
      textesOpen = !textesOpen;
    }
  });

// graph.d3Force('link').distance((link) => {
//   if (link.type === "first") {
//     return 1000;
//   }
//   return 3;
// });

// graph.d3Force('link')
//      .distance((link) => link.type == "first" ? '5000' : '1000');

graph.controls().maxDistance = 7000;
graph.controls().minDistance = 0;

// const bloomPass = new THREE.UnrealBloomPass();
// bloomPass.strength = 0.5;
// bloomPass.radius = 1;
// bloomPass.threshold = 0.1;
// graph.postProcessingComposer().addPass(bloomPass);

function resizeGraph() {
  if (graph) {
    let height = container.clientHeight;
    let width = container.clientWidth;

    graph.width(width);
    graph.height(height);
    let controls = graph.controls();
    controls.handleResize();
  }
}
window.addEventListener('resize', resizeGraph);

const closeBtn = document.querySelector('.textes__close');
closeBtn.addEventListener('click', (e) => {
  e.preventDefault();
  graph.enableNavigationControls(true);
  document.querySelector('.texte.show').scrollIntoView();
  texteContainer.classList.remove('show');
  textes.forEach((el) => {
    el.classList.remove('show');
  });
  textesOpen = !textesOpen;
  closeBtn.blur();
});

////
//// From JSON make a list of links for data vizualisation
////
//let entriesbis = [];
//const max = 4;
//let links = '';
//const order = [];
//function tractatusLink() {
//  const allLinks = [];
//  for (let i = 1; i <= max; i++) {
//    const subarray = [];
//    entriesbis.forEach((el) => {
//      if (el.length == i) {
//        subarray.push(el);
//      }
//    });
//    order.push(subarray);
//  }
//  for (let i = 0; i < max - 1; i++) {
//    if (i == 0) {
//      order[i].forEach((el) => {
//        const link = {
//          "source": "0",
//          "target": el
//        }
//        links = links + `{"source": "0", "target": "${el}"},`;
//      });
//    }
//    order[i].forEach((el) => {
//      order[i+1].forEach((search) => {
//        const partSearch = search.substring(0, el.length);
//        if (el === partSearch) {
//          const link = {
//            "source": el,
//            "target": search
//          }
//          links = links + `{"source": "${el}", "target": "${search}"},`;
//        }
//      });
//    });
//  }
//}
// tractatusLink;
// function makeList() {
//     const url = "./js/praxis.json";
//     async function fetchAsync () {
//       // await response of fetch call
//       let response = await fetch(url);
//       // only proceed once promise is resolved
//       let data = await response.json();
//       // only proceed once second promise is resolved
//       return data;
//     }
//     // trigger async function
//     // log response or catch error of fetch promise
//     fetchAsync()
//         .then(data => {
//             data.nodes.forEach((el) => {
//                 entriesbis.push(el.id);
//             });
//         })
//         .then(tractatus)
//         .catch(reason => console.log(reason.message))
// }
// makeList();

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
// var contentBounce = document.querySelector('.container');
// contentBounce.addEventListener('touchstart', function (event) {
//   this.allowUp = this.scrollTop > 0;
//   this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
//   this.slideBeginY = event.pageY;
// });

// contentBounce.addEventListener('touchmove', function (event) {
//   var up = event.pageY > this.slideBeginY;
//   var down = event.pageY < this.slideBeginY;
//   this.slideBeginY = event.pageY;
//   if ((up && this.allowUp) || (down && this.allowDown)) {
//     event.stopPropagation();
//   } else {
//     event.preventDefault();
//   }
// });

//// Menu
const menuBtn = document.querySelector('.btn--menu');
const menu = document.querySelector('.menu');
let menuOpen = false;

menuBtn.addEventListener('click', (e) => {
  e.preventDefault();
  menuOpen = !menuOpen;
  // if (menuOpen) {
  //   if (!isTouchDevice()) controls.activeLook = false;
  // } else {
  //   if (!isTouchDevice()) controls.activeLook = true;
  // }
  menu.classList.toggle('show--menu');
  menuBtn.blur();
});

//
// Scrollbar with simplebar.js
//
let simpleBarContent = new SimpleBar(document.querySelector('.textes'));
let simpleBarMenu = new SimpleBar(document.querySelector('.menu'));
// Disable scroll simplebar for print
window.addEventListener('beforeprint', (event) => {
  simpleBarContent.unMount();
  simpleBarMenu.unMount();
});
window.addEventListener('afterprint', (event) => {
  simpleBarContent = new SimpleBar(document.querySelector('.textes'));
  simpleBarMenu = new SimpleBar(document.querySelector('.menu'));
});

// // Anciennes versions
// let messageIOS = false;
//
// const platform = window.navigator.platform;
// const iosPlatforms = ['iPhone', 'iPad', 'iPod'];
//
// if (!messageIOS && iosPlatforms.indexOf(platform) !== -1) {
//   const banner = document.createElement('div');
//   banner.innerHTML = `<div class="devicemotion--ios" style="z-index: 900; position: absolute; bottom: 0; left: 0; width: 100%; background-color:#000; color: #fff;"><p style="font-style: 1em; padding: 2em 1em; text-align: center;">Cet antilivre risque de ne pas fonctionner avec des versions d’iOS inférieures à la version 13.<br>(Cliquer sur cette bannière pour la faire disparaître).</p></div>`;
//   banner.onclick = () => banner.remove(); // You NEED to bind the function into a onClick event. An artificial 'onClick' will NOT work.
//   document.querySelector('body').appendChild(banner);
//   messageIOS = true;
// }
