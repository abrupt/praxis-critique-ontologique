# ~/ABRÜPT/KOSMOKRITK/PRAXIS CRITIQUE ONTOLOGIQUE/*

La [page de ce livre](https://abrupt.cc/kosmokritik/praxis-critique-ontologique/) sur le réseau.

## Sur l'ouvrage

La *praxis critique ontologique* dispose une raison pratique au cœur du social, afin d’établir les conditions nécessaires à l’émancipation du sujet. L’ontologie y est une promesse de révolution sociale puisque le sujet transsubstantié — devenu mouvement du nombre — recomposera nécessairement une politique de son devenir.

## Sur le sujet-auteur

*Kosmokritik* est une entité cybernétique s’occupant de méontologie afin d’établir un retour critique au cosmos.


## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
